/* 
 * File:   Cutting_planes.h
 * Author: black
 *
 * Created on October 15, 2014, 12:51 PM
 */



#ifndef CUTTING_PLANES_H_
#define	CUTTING_PLANES_H_

#include <armadillo>
#include "Datacontainer.h"
#define INF (-log(0.0))
#define MU 0.1

using namespace arma;

class Cutting_planes{
public:
    Cutting_planes(int dim, int num_data);
    void gen_vec(rowvec w, Data_container *dc);
    rowvec gen_vec_beta(rowvec w, rowvec oldw, Data_container *dc, double C, bool &change);
    void gen_vec_hyb(rowvec w, Data_container *dc);
    virtual ~Cutting_planes();
    double get_max(rowvec w);
    double at(int i, int j);
    double replace(int i, int j, double val);
    mat *a_ij;
    rowvec *b_i;
    int cont, dim;
    bool replace_row;
    double old_sqnorm_w;
    
    void get_data_beta(rowvec *new_cut, double &xi, unsigned long &length_cut, unsigned long &t_error);
    void write_cp(rowvec w);
private:
    int num_data, length_cut, length_cut_l, np, cp_pos, training_err, training_err_l;
    double error, *hpd, *hpf, xi, xi_l;
    rowvec *output, *old_output, *new_cut, *new_cut_l;
    int sort_data(double* value, double* data, uint32_t size);
    void swap(double* a, double* b);
};

#endif	/* CUTTING_PLANES_H */

