/*
 * Lineextractor.h
 *
 *  Created on: Dec 12, 2014
 *      Author: black
 */

#ifndef LINEEXTRACTOR_NORM_H_
#define LINEEXTRACTOR_NORM_H_

#include <armadillo>
#include "Datacontainer.h"

using namespace arma;

class Lineextractor_norm : public Data_container{
public:
    Lineextractor_norm(int dim, string path, string datafile, int num_data=0);
	virtual ~Lineextractor_norm();
    mat *extract_xij();
    vec *extract_yi();
    void run();
    void run(unsigned int numline);
	bool end_extraction();
};

#endif /* LINEEXTRACTOR_NORM_H_ */
