/*
 * File:   Bee_Col.cpp
 * Author: black
 *
 * Created on July 1, 2015, 1:10 PM
 */

#include "Bee_Col_ada.h"
#include "QObject"
extern double time_alg;

Bee_Col_ada::Bee_Col_ada(int num_sources, int final_iter, double F,
                         Data_container *dc, double C, int num_alphas, double limit_trials) {
    try {
        bec = new BeeCol_data;
        bec->dc = dc;
        bec->final_iter = final_iter;
        bec->C = C;
        bec->num_sources = num_sources;
        bec->num_alphas = num_alphas;
        bec->F = F;
        bec->sum_fit=0;
        bec->b=0;
        bec->limit_trials=limit_trials;

        bec->condition_var1 = PTHREAD_COND_INITIALIZER;
        bec->condition_var2 = PTHREAD_COND_INITIALIZER;
        bec->condition_var3 = PTHREAD_COND_INITIALIZER;
        bec->count_mutex = PTHREAD_MUTEX_INITIALIZER;

        bec->array_mutex=new pthread_mutex_t[num_sources];

        for(int i=0; i<num_sources; i++)
            bec->array_mutex[i]= PTHREAD_MUTEX_INITIALIZER;

        bec->Best_Pos = new rowvec(num_alphas);
        bec->w = new rowvec(dc->dim);
        bec->xixj = new LinKer(num_alphas, dc->dim, false);
        bec->Si = new rowvec*[num_sources];

        for(int i=0; i<num_sources; i++)
            bec->Si[i]=new rowvec(num_alphas);

        bec->fit = new rowvec(num_sources);
        bec->unsuc_trials = new rowvec(num_sources);
        bec->nectar_Si = new rowvec(num_sources);
        bec->bias_Si = new rowvec(num_sources);
        bec->pm = new rowvec(num_sources);
        bec->flag_update=true;

        bec->flag_release=false;
        bec->unsuc_trials->zeros();

    } catch (std::bad_alloc& ba) {
        std::cerr << "bad_alloc caught in PSO constructor: " << ba.what() << '\n';
        exit(EXIT_FAILURE);
    }
}

void *Bee_Col_ada::updateSources(void *threadarg) {

    try {
        BeeCol_data * bec = (BeeCol_data *) threadarg;
        double fi, nectar_vi, bias, tmp = 0, v_ij=0, ran_num, prob;
        int r = 0, i, j, phase, cont, num_source;
        bool flag_change;
        Adatron objfunc(bec->num_alphas);
        rowvec *ran_num_vec;

        ran_num_vec = new rowvec(bec->num_alphas);

        pthread_mutex_lock(&bec->count_mutex);

            num_source = bec->cont_thread;
            bec->cont_thread++;

            if (bec->cont_thread < bec->num_sources)
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
            else {
                bec->cont_thread = 0;
                pthread_cond_broadcast(&bec->condition_var1);
            }

        pthread_mutex_unlock(&bec->count_mutex);

        arma_rng::set_seed(time(NULL)+num_source);//Agregado para arma
        srand(time(NULL)+num_source);


        /*for (j = 0; j < bec->xixj->num_alphas; j++) {
            ran_num = bec->C * ((float) rand() / RAND_MAX);
            bec->Si[num_source]->at(j) = ran_num;
        }*/

        ran_num_vec->randu();
        *ran_num_vec=bec->C * (*ran_num_vec);
        *bec->Si[num_source] = normalise(*ran_num_vec,1);

        //*bec->Si[num_source] = normalise(*bec->Si[num_source],1);

        //bec->Si->row(num_source).print("Alpha");
        bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
        bec->bias_Si->at(num_source) = bias;
        //pthread_mutex_unlock(&bec->count_mutex);//<----------

        if(bec->nectar_Si->at(num_source)>=0)
            bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
        else
            bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));

        pthread_mutex_lock(&bec->count_mutex);

            bec->cont_thread++;

            if (bec->cont_thread >= bec->num_sources){
                bec->fval_bpos=bec->nectar_Si->at(num_source);
                bec->Best_Pos=bec->Si[num_source];
                bec->b=bec->bias_Si->at(num_source);
                pthread_cond_signal(&bec->condition_var2);
            }

            pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        pthread_mutex_unlock(&bec->count_mutex);

            bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;
        pthread_mutex_lock(&bec->count_mutex);
            bec->cont_thread++;
            if (bec->cont_thread >= bec->num_sources){
                bec->cont_thread=0;
                bec->flag_update=false;
                pthread_cond_broadcast(&bec->condition_var1);
            }else
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        pthread_mutex_unlock(&bec->count_mutex);


        //srand(time(NULL)+num_source);<<<------

        while (bec->block) {

            //pthread_mutex_lock(&bec->count_mutex);//<-----
            for (phase = 0; phase < 2; phase++) {

                flag_change = false;

                if (phase == 0) {//Employed Bees Phase

                    i = num_source;

                } else {//Onlooker Bees Phase

                    ran_num = ((float) rand() / RAND_MAX);

                    //bec->count_mutex.lock();
                    pthread_mutex_lock(&bec->count_mutex);

                    bec->cont_thread++;

                    if (bec->cont_thread >= bec->num_sources)
                        //bec->condition_var2.wakeOne();
                        pthread_cond_signal(&bec->condition_var2);

                    //bec->condition_var1.wait(&bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

                    //bec->count_mutex.unlock();
                    pthread_mutex_unlock(&bec->count_mutex);

                    if (bec->flag_update == true) {

                        bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;
                    }


                    //bec->count_mutex.lock();
                    pthread_mutex_lock(&bec->count_mutex);

                    bec->cont_thread++;

                    if (bec->cont_thread >= bec->num_sources) {
                        bec->cont_thread = 0;
                        bec->flag_update = false;
                        //bec->condition_var1.wakeAll();
                        pthread_cond_broadcast(&bec->condition_var1);

                    } else
                        //bec->condition_var1.wait(&bec->count_mutex);
                        pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

                    //bec->count_mutex.unlock();
                    pthread_mutex_unlock(&bec->count_mutex);

                    for (cont = 0, prob = 0; cont < bec->num_sources && ran_num > prob; cont++) {
                        prob += bec->pm->at(cont);
                        if (ran_num <= prob)
                            i = cont;
                    }
                }

                do {

                    j = round((bec->num_alphas - 1)* ((double) rand() / RAND_MAX));
                    fi = -bec->F + (2 * bec->F * ((double) rand() / RAND_MAX));


                    do {
                        r = round((bec->num_sources - 1) * ((double) rand() / RAND_MAX));
                    } while (r == i);

                    v_ij = bec->Si[i]->at(j) + fi * (bec->Si[i]->at(j) - bec->Si[r]->at(j));



                    if (v_ij < 0)
                        v_ij = 0;
                    else
                        if (v_ij > bec->C)
                            v_ij = bec->C;

                    //cout<<bec->Si->at(i, j)<<endl;
                    if (v_ij != bec->Si[i]->at(j))
                        flag_change = true;



                } while (!flag_change);


//pthread_mutex_lock(&bec->count_mutex);
                nectar_vi = objfunc.eval_cpos(bec->Si[i], j, v_ij, bec->xixj, bias);
                //pthread_mutex_unlock(&bec->count_mutex);


                //bec->array_mutex[i].lock();
                pthread_mutex_lock(&bec->array_mutex[i]);
                    if (nectar_vi < bec->nectar_Si->at(i)) {

                        bec->Si[i]->at(j) = v_ij;
                        bec->nectar_Si->at(i) = nectar_vi;
                        bec->bias_Si->at(i) = bias;

                        if (bec->nectar_Si->at(i) >= 0)
                            bec->fit->at(i) = 1 / (1 + bec->nectar_Si->at(i));
                        else
                            bec->fit->at(i) = (1 + abs(bec->nectar_Si->at(i)));

                        bec->flag_update = true;
                        bec->unsuc_trials->at(i) = 0;

                    } else {

                        bec->unsuc_trials->at(i)++;
                    }
                //bec->array_mutex[i].unlock();
                pthread_mutex_unlock(&bec->array_mutex[i]);

            }


            //bec->count_mutex.lock();
            pthread_mutex_lock(&bec->count_mutex);

                bec->cont_thread++;

                if (bec->cont_thread >= bec->num_sources) {
                    bec->cont_thread = 0;
                    //bec->condition_var2.wakeAll();
                    pthread_cond_broadcast(&bec->condition_var2);
                    //bec->condition_var1.wait(&bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
                    bec->flag_update = false;
                } else
                    //bec->condition_var1.wait(&bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

            //bec->count_mutex.unlock();
            pthread_mutex_unlock(&bec->count_mutex);


            if (bec->unsuc_trials->at(num_source) >= bec->limit_trials) {//Scout Bee Phase

                /*for (j = 0; j < bec->xixj->num_alphas; j++) {
                    ran_num = bec->C * ((float) rand() / RAND_MAX);
                    bec->Si[num_source]->at(j) = ran_num;
                }

                 *bec->Si[num_source] = normalise(*bec->Si[num_source],1);*/

                ran_num_vec->randu();
                *ran_num_vec = bec->C * (*ran_num_vec);
                *bec->Si[num_source] = normalise(*ran_num_vec, 1);

                bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
                bec->bias_Si->at(num_source) = bias;

                if (bec->nectar_Si->at(num_source) >= 0)
                    bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
                else
                    bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));

                bec->flag_update = true;
                bec->unsuc_trials->at(num_source) = 0;
            }

            pthread_mutex_lock(&bec->count_mutex);

                bec->cont_thread++;

                if (bec->cont_thread >= bec->num_sources) {

                    bec->flag_release=true;
                    //bec->condition_var2.wakeOne();
                    pthread_cond_signal(&bec->condition_var2);
                    //bec->condition_var1.wait(&bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
                    bec->flag_update = false;
                }else
                    //bec->condition_var1.wait(&bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

            //bec->count_mutex.unlock();
            pthread_mutex_unlock(&bec->count_mutex);

        }

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }


    pthread_exit(NULL);
    return threadarg;
}

void *Bee_Col_ada::evalSources(void *threadarg) {

    try {
        int cont;
        double fval_CBest;
        uword ind_CBest=0;
        BeeCol_data * bec = (BeeCol_data *) threadarg;
        //Adatron objfunc;

        pthread_mutex_lock(&bec->count_mutex);

        pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);

        for (int iter = 0; iter <= bec->final_iter; iter++) {
            do {
                if (bec->flag_update) {
                    bec->sum_fit = bec->fit->at(0);

                    for (cont = 1; cont < bec->num_sources; cont++)
                        bec->sum_fit += bec->fit->at(cont);

                    fval_CBest=bec->nectar_Si->min(ind_CBest);

                    if(fval_CBest<bec->fval_bpos){
                        //cout<<"Nectar: "<<fval_CBest<<endl;
                        bec->Best_Pos=bec->Si[ind_CBest];
                        bec->fval_bpos=fval_CBest;
                        bec->b=bec->bias_Si->at(ind_CBest);
                    }
                }

                bec->cont_thread = 0;

                pthread_cond_broadcast(&bec->condition_var1);
                pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);
            } while (!bec->flag_release);

            bec->flag_release=false;

            if (bec->fval_bpos <= 0.001)
                break;
        }

        if (bec->flag_update) {

            fval_CBest = bec->nectar_Si->min(ind_CBest);

            if (fval_CBest < bec->fval_bpos) {
                //cout << "Nectar: " << fval_CBest << endl;
                bec->Best_Pos = bec->Si[ind_CBest];
                bec->fval_bpos = fval_CBest;
                bec->b = bec->bias_Si->at(ind_CBest);
            }
        }

        //bec->nectar_Si->max(ind_GBest);

        //cout<<"GBest: "<<bec->fval_bpos<<endl;

        bec->block = false;

        pthread_cond_broadcast(&bec->condition_var1);

        pthread_mutex_unlock(&bec->count_mutex);
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
    return threadarg;
}

void Bee_Col_ada::write_Colval(BeeCol_data *bec) {

    remove("/home/black/workspace/ff/pts.txt");
    /*if (remove("/home/black/workspace/ff/pts.txt") != 0)
        perror("Error deleting file:pts");
    else
        puts("File successfully deleted:pts");*/

    ofstream *pts;
    pts = new ofstream("/home/black/workspace/ff/pts.txt", ios::out | ios::app);

    for (int i = 0; i < bec->num_sources; i++) {
        for (int j = 0; j < bec->xixj->num_alphas; j++)
            *pts << bec->Si[i]->at(j) << ' ';
        *pts << endl;
    }

    pts->close();
}

void Bee_Col_ada::run() {

    try {
        cout<<"run normal"<<endl;
        int rc, id, i, c_times = 0;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);
        if (!bec->dc->end_extraction()) {
            auto duration = 0;

            srand(time(NULL));

            bec->dc->run();
            bec->xixj->fill(bec->dc);

            bec->cont_thread = 0;
            high_resolution_clock::time_point t1, t2;
            t1 = high_resolution_clock::now();

            pthread_t threadEvalVecs, threadsUpdateVecs[bec->num_sources];
            bec->block = true;

            rc = pthread_create(&threadEvalVecs, NULL, evalSources, (void*) &(*bec));
            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }


            for (id = 0; id < bec->num_sources; id++) {
                rc = pthread_create(&threadsUpdateVecs[id], NULL, updateSources, (void*) &(*bec));
                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm," << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalVecs, NULL);
            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < bec->num_sources; id++) {
                rc = pthread_join(threadsUpdateVecs[id], NULL);
                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }


            c_times++;
            t2 = high_resolution_clock::now();
            //cout << "Iter: " << c_times << endl;

            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000 << endl;
            time_alg += (float) duration / 1000000;
            cout <<"GBest: "<<bec->fval_bpos<<endl;

            form_w();
            for (i = 0; i < bec->num_sources; i++) delete bec->Si[i];
        }

        //results.close();
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void Bee_Col_ada::run(LinKer *lk) {

    try {
        erase_lk = false;
        int rc, id, i, c_times = 0;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);

        auto duration = 0;
        high_resolution_clock::time_point t1, t2;

        srand(time(NULL));

        //bec->dc->run();
        //bec->xixj->fill_mod(bec->dc);
        bec->xixj = lk;

        bec->cont_thread = 0;

        t1 = high_resolution_clock::now();
        cout << "Run" << endl;

        pthread_t threadEvalVecs, threadsUpdateVecs[bec->num_sources];

        bec->block = true;


        rc = pthread_create(&threadEvalVecs, NULL, evalSources, (void*) &(*bec));

        if (rc) {
            cout << "Error:unable to create thread threadEvalSwarm," << rc
                    << endl;
            exit(EXIT_FAILURE);
        }

        for (id = 0; id < bec->num_sources; id++) {
            rc = pthread_create(&threadsUpdateVecs[id], NULL, updateSources,
                    (void*) &(*bec));

            if (rc) {
                cout << "Error:unable to create thread threadsUpdateSwarm,"
                        << rc << endl;
                exit(EXIT_FAILURE);
            }
        }

        rc = pthread_join(threadEvalVecs, NULL);

        if (rc) {
            cout << "Error:unable to join threadEvalSwarm," << rc << endl;
            exit(EXIT_FAILURE);
        }

        for (id = 0; id < bec->num_sources; id++) {
            rc = pthread_join(threadsUpdateVecs[id], NULL);

            if (rc) {
                cout << "Error:unable to join threadsUpdateSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }
        }

        c_times++;
        t2 = high_resolution_clock::now();
        //cout << "Iter: " << c_times << endl;

        duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        cout.unsetf(std::ios::floatfield);
        cout.precision(6);
        cout << "Time: " << (float) duration / 1000000 << endl;
        time_alg += (float) duration / 1000000;
        cout << "GBest: " << bec->fval_bpos << endl;

        form_w();
        for (i = 0; i < bec->num_sources; i++)
            delete bec->Si[i];


        //results.close();
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void Bee_Col_ada::form_w(){
    int i, dim_alph=bec->num_alphas;

    bec->w->zeros();

    for(i=0; i<dim_alph; i++)
        *bec->w+=bec->xixj->y_i->at(i) * bec->xixj->x_ij->row(i) * bec->Best_Pos->at(i); //Salida deseada * dot(xixj) * alpha
}

rowvec *Bee_Col_ada::get(double &b) {
    b = bec->b;
    return bec->w;
}

Bee_Col_ada::~Bee_Col_ada() {
    delete [] bec->Si;
    delete bec->fit;
    delete bec->unsuc_trials;
    delete bec->pm;
    delete bec->bias_Si;
    delete bec->w;

    if(erase_lk)
        delete bec->xixj;

    bec->Best_Pos=NULL;
    delete bec->nectar_Si;
    delete bec;
}
