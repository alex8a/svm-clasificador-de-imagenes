/*
 * Fullextractor.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#include "Fullextractor.h"

Fullextractor::Fullextractor(int dim, int num_data, string path, string datafile, bool sec, bool mult_y)
:Data_container(dim, num_data, path, datafile){
	try{
		x_ij=new mat(num_data,dim);
		x_ij->zeros();
		y_i=new vec(num_data);
		y_i->zeros();
                
	}catch(std::bad_alloc& ba){
		std::cerr << "bad_alloc caught Fullextractor: " << ba.what() << '\n';
		exit(EXIT_FAILURE);
	}
        
        this->sec=sec;
        this->mult_y=mult_y;
}

mat *Fullextractor::extract_xij(){
	return x_ij;
}

vec *Fullextractor::extract_yi(){
	return y_i;
}

bool Fullextractor::end_extraction(){
	return eofflag;
}

void Fullextractor::run(){

	unsigned long pos;
	int val;
        rowvec tmp(dim);
        
	if(!eofflag){
		srand(time(NULL));

		for(unsigned int i=0; i<num_data; i++){
                    
                    if(sec)
                        pos=i;
                    else
			pos=rand()%(num_total_data+1);
                    
                    if(!mult_y){
                        x_ij->row(i)=get_line(pos, val);                     
                    }else{
                        tmp=get_line(pos, val);
                        x_ij->row(i)=val*tmp;
                    }
                        
                        y_i->at(i)=val;
                        
                    if(i%10000==0)
                        cout<<"Num. lines: "<<i<<endl;
		}
                

		eofflag=true;
	}
}


Fullextractor::~Fullextractor() {
    delete x_ij;
    x_ij=NULL;
    delete y_i;
    y_i=NULL;
}

