/*
 * PSO.h
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#ifndef PSO_H_
#define PSO_H_

#include <pthread.h>
#include <cstdlib>
#include <chrono>
#include <armadillo>

#include "PSOdata.h"
#include "ff.h"

using namespace arma;
using namespace std;

class PSO{
public:
	PSO(int dim_dset, float c1, float c2, float Vmax, float w_start, float w_end,
	        int swarm_size, int final_iter, vec *boundary, string name, 
                string path, string datafile);
	virtual ~PSO();
	void run();
        void run_mabc();
        void run_pso();
	rowvec *get();

private:

	swarm_data *swarm;
	rowvec *data;

};

#endif /* PSO_H_ */
