/*
 * Fullextractor.h
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#ifndef FULLEXTRACTOR_H_
#define FULLEXTRACTOR_H_

#include <cstdlib>
#include <armadillo>

#include "Datacontainer.h"

using namespace arma;

class Fullextractor: public Data_container {
public:
    Fullextractor(int dim, int num_data, string path, string datafile, bool sec=false, bool mult_y=false);
	virtual ~Fullextractor();
    mat *extract_xij();
    vec *extract_yi();
    bool end_extraction();
    void run();
    
private:
       bool sec, mult_y; 

};

#endif /* FULLEXTRACTOR_H_ */
