/* 
 * File:   Foldextractor_norm.cpp
 * Author: black
 * 
 * Created on 23 de octubre de 2015, 05:43 PM
 */

#include "Foldextractor_norm.h"

Foldextractor_norm::Foldextractor_norm(int dim, int num_data_pf, int num_folds,
        string path, vector<string> dirlist_data, int omit_fold)
:Data_container(dim,num_data_pf,path,dirlist_data.at(0)){

    this->num_folds=num_folds;
    this->path=path;
    this->num_data_pf=num_data_pf;
    this->num_data=num_data_pf*(num_folds-1);
    this->dirlist_data=dirlist_data;
    this->omit_fold=omit_fold;
}

void Foldextractor_norm::run(){

    //bool flag_join=false;
    int cont_lin=0;
    x_ij=new mat(num_data, dim);
    y_i=new vec(num_data);
    
    /*dc=new Fullextractor_norm(dim, num_data_pf, path, dirlist_data.at(0), true, false);
    *x_ij=*dc->extract_xij();
    *y_i=*dc->extract_yi();
    delete dc;*/

    for (int fold = 0; fold < num_folds; fold++) {
        if (omit_fold != fold) {
            dc = new Lineextractor_norm(dim, path, dirlist_data.at(fold), num_data_pf);
            do {
                dc->run();
                x_ij->row(cont_lin) = dc->extract_xij()->row(0);
                y_i->at(cont_lin) = dc->extract_yi()->at(0);
                cont_lin++;
            } while (!dc->end_extraction()&&cont_lin%num_data_pf!=0);
            delete dc;
        }
        /*if (omit_fold != fold&&flag_join) {
            dc = new Fullextractor_norm(dim, num_data_pf, path, dirlist_index.at(fold), dirlist_data.at(fold), true, false);
            dc->run();
            *x_ij = join_cols(*x_ij, *dc->extract_xij());
            *y_i = join_cols(*y_i, *dc->extract_yi());
            delete dc;
        }else 
        if (omit_fold != fold){
            dc = new Fullextractor_norm(dim, num_data_pf, path, dirlist_index.at(fold), dirlist_data.at(fold), true, false);
            dc->run();
            *x_ij = *dc->extract_xij();
            *y_i =  *dc->extract_yi();
            delete dc;
            flag_join=true;
        }*/
    }
    
    eofflag=true;
}

mat *Foldextractor_norm::extract_xij(){
	return x_ij;
}

vec *Foldextractor_norm::extract_yi(){
	return y_i;
}

bool Foldextractor_norm::end_extraction(){
	return eofflag;
}

Foldextractor_norm::~Foldextractor_norm() {

    delete x_ij;
    x_ij=NULL;
    delete y_i;
    y_i=NULL;
}

