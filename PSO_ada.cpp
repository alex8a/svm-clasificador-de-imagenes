/*
 * PSO.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#include "PSO_ada.h"
#include <typeinfo>
extern double time_alg;

PSO_ada::PSO_ada(double c1, double c2, double C, double w_start, double w_end, double Vmax,
        int swarm_size, int final_iter, Data_container *dc,  int num_alphas) {

    try {
        
        swarmdt = new swarm_data_ada;
        swarmdt->w_vec = new rowvec(dc->dim);
        swarmdt->cont_thread = 0;
        swarmdt->final_iter = final_iter;
        swarmdt->swarm_size = swarm_size;
        swarmdt->C = C;
        swarmdt->c1 = c1;
        swarmdt->c2 = c2;
        swarmdt->w = w_start;
        swarmdt->w_start = w_start;
        swarmdt->w_end = w_end;
        swarmdt->fval_PBest = new vec(swarm_size);
        swarmdt->num_alphas=num_alphas;
        swarmdt->Vmax=Vmax;
        
        swarmdt->P_best = new rowvec*[swarmdt->swarm_size];

        swarmdt->dc = dc;     
        swarmdt->cont_thread = 0;
        
        swarmdt->bias_pb = new vec(swarm_size);
        swarmdt->ind_GBest = 0;
        swarmdt->condition_var1 = PTHREAD_COND_INITIALIZER;
        swarmdt->condition_var2 = PTHREAD_COND_INITIALIZER;
        swarmdt->count_mutex = PTHREAD_MUTEX_INITIALIZER;
        swarmdt->xixj = new LinKer(num_alphas, dc->dim, false);
        
    } catch (std::bad_alloc& ba) {
        std::cerr << "bad_alloc caught in PSO constructor: " << ba.what() << '\n';
        exit(EXIT_FAILURE);
    }

}

void *PSO_ada::updateSwarm(void *threadarg) {
    try {
        swarm_data_ada * swarm = (swarm_data_ada *) threadarg;
        int numpart, i;
        double r1, r2, bias, fval_Swarm;
        rowvec  V(swarm->num_alphas), *Swarm_pos;
        Adatron objfunc(swarm->num_alphas);
        
        Swarm_pos=new rowvec(swarm->num_alphas);
        Swarm_pos->zeros();
        V.zeros();
        
        pthread_mutex_lock(&swarm->count_mutex);

        numpart = swarm->cont_thread;
        swarm->cont_thread++;

        if (swarm->cont_thread < swarm->swarm_size)
            pthread_cond_wait(&swarm->condition_var1, &swarm->count_mutex);
        else {
            swarm->cont_thread = 0;
            pthread_cond_broadcast(&swarm->condition_var1);
        }
        
        pthread_mutex_unlock(&swarm->count_mutex);
        
        swarm->fval_PBest->at(numpart) = objfunc.eval(swarm->P_best[numpart], swarm->xixj, bias);
        swarm->bias_pb->at(numpart)=bias;
        


       
        srand(time(NULL)+numpart);
        
        while (swarm->block) {

            
            r1 = ((double) rand() / RAND_MAX);
            r2 = ((double) rand() / RAND_MAX);

                V= V * swarm->w
                   + swarm->c1 * r1 * (*swarm->P_best[numpart] - *Swarm_pos)
                   + swarm->c2 * r2 * (*swarm->P_best[swarm->ind_GBest] - *Swarm_pos);

            for (i = 0; i < swarm->num_alphas; i++)
                if (V.at(i) > swarm->Vmax)
                    V.at(i) = swarm->Vmax;
                else if (V.at(i) < -swarm->Vmax)
                    V.at(i) = -swarm->Vmax;

            *Swarm_pos = *Swarm_pos + V;

            for (i = 0; i < swarm->num_alphas; i++)
                if (Swarm_pos->at(i) > swarm->C)
                    Swarm_pos->at(i) = swarm->C;
                else if (Swarm_pos->at(i) < 0)
                    Swarm_pos->at(i) = 0;
            
            fval_Swarm = objfunc.eval(Swarm_pos, swarm->xixj, bias);

            if (fval_Swarm < swarm->fval_PBest->at(numpart)) {
                swarm->fval_PBest->at(numpart) = fval_Swarm;
                *swarm->P_best[numpart] = *Swarm_pos;
                swarm->bias_pb->at(numpart) = bias;
            }
            
            pthread_mutex_lock(&swarm->count_mutex);
            swarm->cont_thread++;

            if (swarm->cont_thread >= swarm->swarm_size)
                pthread_cond_signal(&swarm->condition_var2);

            pthread_cond_wait(&swarm->condition_var1, &swarm->count_mutex);
            pthread_mutex_unlock(&swarm->count_mutex);
        }
        
        delete Swarm_pos;
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
    pthread_exit(NULL);


    return threadarg;
}


void *PSO_ada::evalSwarm(void *threadarg) {

    try {
        swarm_data_ada * swarm = (swarm_data_ada *) threadarg;
        uword ind_BestP;
        double dec;
        
        pthread_mutex_lock(&swarm->count_mutex);

        dec = (swarm->w_start - swarm->w_end) / swarm->final_iter;

        pthread_cond_wait(&swarm->condition_var2, &swarm->count_mutex);


        for (int iter = 0; iter <= swarm->final_iter; iter++) {
            swarm->w = swarm->w_start - dec * iter;

            swarm->fval_PBest->min(ind_BestP);

            if (swarm->fval_PBest->at(swarm->ind_GBest) > swarm->fval_PBest->at(ind_BestP)) {

                swarm->ind_GBest=ind_BestP;
                swarm->b=swarm->bias_pb->at(ind_BestP);
                if (swarm->fval_PBest->at(swarm->ind_GBest) <= 0.001)
                    break;
            }

            swarm->cont_thread = 0;

            pthread_cond_broadcast(&swarm->condition_var1);
            pthread_cond_wait(&swarm->condition_var2, &swarm->count_mutex);
        }

        //cout << "GBest " << as_scalar(swarm->fval_PBest->row(swarm->ind_GBest)) << endl;
        swarm->block = false;
        pthread_cond_broadcast(&swarm->condition_var1);


        pthread_mutex_unlock(&swarm->count_mutex);

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
    pthread_exit(NULL);

    return threadarg;
}

void PSO_ada::write_partval(swarm_data_ada *swarm){
        
    if (remove("/home/black/workspace/ff/pts.txt") != 0)
        perror("Error deleting file:pts");
    else
        puts("File successfully deleted:pts");
    
    ofstream pts;
    pts.open("/home/black/workspace/ff/pts.txt",ios::out|ios::app);
    
    for(int i=0; i<swarm->swarm_size; i++){
        for(int j=0; j<swarm->dc->dim; j++)
            pts<<swarm->P_best[i]->at(i)<<" ";
        pts<<endl;
    }
    
    pts.close();
}

void PSO_ada::run() {

    try {
        int rc, id, c_times = 0;
        double ran_num/*, perc*/;
        //Adatron objfunc(swarmdt->swarm_size);
        ofstream results;
        results.open("./results_clasif",ios::out|ios::app);
        
        if (!swarmdt->dc->end_extraction()) {

            auto duration = 0;
            high_resolution_clock::time_point t1, t2;
            
            srand(time(NULL));

            swarmdt->dc->run();
            
            for (unsigned int i = 0; i < swarmdt->swarm_size; i++) {
                swarmdt->P_best[i] = new rowvec(swarmdt->num_alphas);
                for (unsigned int j = 0; j < swarmdt->num_alphas; j++) {
                    ran_num = swarmdt->C * ((double) rand() / RAND_MAX);
                    swarmdt->P_best[i]->at(j) = ran_num;
                }
                *swarmdt->P_best[i] = normalise(*swarmdt->P_best[i], 1);
            }
            
            swarmdt->xixj->fill_mod(swarmdt->dc);

            t1 = high_resolution_clock::now();
            cout << "Run" << endl;
            
            pthread_t threadEvalSwarm, threadsUpdateSwarm[swarmdt->swarm_size];

            swarmdt->block = true;

            rc = pthread_create(&threadEvalSwarm, NULL, evalSwarm,
                    (void*) &(*swarmdt));

            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < swarmdt->swarm_size; id++) {
                rc = pthread_create(&threadsUpdateSwarm[id], NULL, updateSwarm,
                        (void*) &(*swarmdt));

                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm,"
                            << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalSwarm, NULL);

            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < swarmdt->swarm_size; id++) {
                rc = pthread_join(threadsUpdateSwarm[id], NULL);

                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc
                            << endl;
                    exit(EXIT_FAILURE);
                }
            }

            c_times++;
            //cout << "Iter: " << c_times << endl;

            t2 = high_resolution_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000 << endl;
            time_alg += (float) duration / 1000000;
            cout <<"GBest: "<<swarmdt->fval_PBest->at(swarmdt->ind_GBest)<<endl;
            
            form_w();

            //perc = objfunc.classify(swarmdt->w_vec, swarmdt->bias_pb->at(swarmdt->ind_GBest),swarmdt->dc, false);
            //results << perc << endl;
        }
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void PSO_ada::run(LinKer *lk) {

    try {
        erase_lk=false;
                
        int rc, id, c_times = 0;
        double ran_num/*, perc*/;
        //Adatron objfunc(swarmdt->swarm_size);
        ofstream results;
        results.open("./results_clasif",ios::out|ios::app);
        

            auto duration = 0;
            high_resolution_clock::time_point t1, t2;
            
            srand(time(NULL));

            //swarmdt->dc->run();
            //swarmdt->xixj->fill_mod(swarmdt->dc);
            swarmdt->xixj=lk;
             
            for (unsigned int i = 0; i < swarmdt->swarm_size; i++) {
                swarmdt->P_best[i] = new rowvec(swarmdt->num_alphas);
                for (unsigned int j = 0; j < swarmdt->num_alphas; j++) {
                    ran_num = swarmdt->C * ((double) rand() / RAND_MAX);
                    swarmdt->P_best[i]->at(j) = ran_num;
                }
                *swarmdt->P_best[i] = normalise(*swarmdt->P_best[i], 1);
            }
            
            

            t1 = high_resolution_clock::now();
            cout << "Run" << endl;
            
            pthread_t threadEvalSwarm, threadsUpdateSwarm[swarmdt->swarm_size];

            swarmdt->block = true;

            rc = pthread_create(&threadEvalSwarm, NULL, evalSwarm,
                    (void*) &(*swarmdt));

            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < swarmdt->swarm_size; id++) {
                rc = pthread_create(&threadsUpdateSwarm[id], NULL, updateSwarm,
                        (void*) &(*swarmdt));

                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm,"
                            << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalSwarm, NULL);

            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < swarmdt->swarm_size; id++) {
                rc = pthread_join(threadsUpdateSwarm[id], NULL);

                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc
                            << endl;
                    exit(EXIT_FAILURE);
                }
            }

            c_times++;
            //cout << "Iter: " << c_times << endl;


            t2 = high_resolution_clock::now();
            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000 << endl;
            time_alg += (float) duration / 1000000;
            cout <<"GBest: "<<swarmdt->fval_PBest->at(swarmdt->ind_GBest)<<endl;
            
            form_w();

            //perc = objfunc.classify(swarmdt->w_vec, swarmdt->bias_pb->at(swarmdt->ind_GBest),swarmdt->dc, false);
            //results << perc << endl;
        
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void PSO_ada::form_w() {
    int i, dim_alph = swarmdt->num_alphas;

    swarmdt->w_vec->zeros();

    for (i = 0; i < dim_alph; i++)
        *swarmdt->w_vec += swarmdt->xixj->y_i->at(i) * swarmdt->xixj->x_ij->row(i) * swarmdt->P_best[swarmdt->ind_GBest]->at(i);
}

rowvec *PSO_ada::get(double &b) {
    b = swarmdt->b;
    return swarmdt->w_vec;
}

PSO_ada::~PSO_ada() {

    delete swarmdt->fval_PBest;
    delete []swarmdt->P_best;
    delete swarmdt->w_vec;
     
    if(erase_lk)
        delete swarmdt->xixj;
    
    delete swarmdt->bias_pb;
    delete swarmdt;
}

