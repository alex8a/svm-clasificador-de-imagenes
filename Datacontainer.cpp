/* 
 * File:   data_container.cpp
 * Author: black
 * 
 * Created on August 19, 2014, 6:22 PM
 */

#include "Datacontainer.h"

/*
Data_container::Data_container(int dim,  unsigned int num_data, string path, string indexfile, string datafile) {
    string path_index=path+indexfile, path_data=path+datafile;
    unsigned long length;
    cont_line=0;

    streamdata.open(path_data.c_str(), ios::in | ios::binary);
    streamindex.open(path_index.c_str(), ios::in | ios::binary);
    if(!streamdata.is_open()||!streamindex.is_open()) {
        cout<<"Unable to open file"<<endl;
        exit(EXIT_FAILURE);
    }
    this->dim=dim;
    eofflag=false;

    streamindex.seekg(0, streamindex.end);
    length=streamindex.tellg();
	cout<<"Tamaño del archivo de indices: "<<length<<endl;
    streamindex.seekg(0, streamindex.beg);

    size_ind_line=sizeof(unsigned long)+sizeof(unsigned int);
	cout<<size_ind_line<<endl;
    num_total_data=length/size_ind_line;
	cout<<"Total de datos: "<<num_total_data<<", numdata: "<<num_data<<endl;

    if(num_total_data>num_data)
      this->num_data=num_data;
    else
      this->num_data=num_total_data;
}*/

Data_container::Data_container(int dim,  unsigned int num_data, string path, string datafile) { //ALex
    string path_data=datafile;
    int length;
    cont_line=0;
    cout<<path_data<<endl;

    streamdata.open(path_data.c_str(), ios::in | ios::binary);
    if(!streamdata.is_open()) {
        cout<<"Unable to open file"<<endl;
        exit(EXIT_FAILURE);
    }
    this->dim=dim;
    eofflag=false;

    streamdata.seekg(0, streamindex.end);
    length=streamdata.tellg();
    cout<<"Tamaño del archivo de datos: "<<length<<"  "<<length/((dim+1)*4)<<endl;
    streamdata.seekg(0, streamindex.beg);
    num_total_data=length/((dim+1)*4);
	cout<<"Total de datos: "<<num_total_data<<", numdata: "<<num_data<<endl;
    if(num_total_data>num_data)
      this->num_data=num_data;
    else
      this->num_data=num_total_data;
}

/*rowvec Data_container::get_line(int &y){

    rowvec tmpvec(dim);
    tmpvec.zeros();

    if(!eofflag){
        unsigned long pos_file;
        unsigned int size, pos, offset;
        double val;

        streamindex.read((char *)&pos_file,sizeof(unsigned long));
        streamindex.read((char *)&size,sizeof(unsigned int));
        byte *buffer=new byte[size];
        cout<<"Indice: "<<pos_file<<" - Tamaño: "<<size<<" bytes, "<<(size-4)/12<<" datos"<<endl;

        streamdata.read((char *)buffer,size);

        //cout<< "Buffer: "<<buffer<<endl;

        memcpy(&y,buffer,sizeof(int));

        cout<< "y: "<<y<<endl;

        offset=sizeof(int);
        while(offset<size){

            memcpy(&pos,buffer+offset,sizeof(unsigned int));
            offset+=sizeof(int);
            memcpy(&val,buffer+offset,sizeof(double));
            offset+=sizeof(double);
            tmpvec.at(pos-1)=val;
            //cout<<"Posicion "<<pos-1<<" se le asigna: "<<val<<endl;
        }

        cont_line++;
        if(cont_line==num_total_data)
            eofflag=true;
        delete []buffer;
        buffer=NULL;
    }
    //cout<<"tmpvec"<<endl;
    //tmpvec.print();
    return tmpvec;
}*/

rowvec Data_container::get_line(int &y){ //ALex
    rowvec tmpvec(dim);
    tmpvec.zeros();
    if(!eofflag){
        int offset;
        float val;
        byte *buffer=new byte[((dim+1)*sizeof(float))];
        streamdata.read((char *)buffer,(dim+1)*sizeof(float));
        //Ponemos la salida deseada en y
        memcpy(&y,buffer,sizeof(int));
        //cout<< "getLine y: "<<y<<endl;
        offset=sizeof(int);
        int posVector=0;
        while(offset<dim){
            memcpy(&val,buffer+offset,sizeof(float));
            offset+=sizeof(float);
            tmpvec.at(posVector)=val;
            posVector++;
            //cout<<"Posicion "<<posVector<<" se le asigna: "<<val<<endl;
        }
        cont_line++;
        if(cont_line==num_total_data)
            eofflag=true;
        delete []buffer;
        buffer=NULL;
    }
    //cout<<"tmpvec"<<endl;
    //tmpvec.print();
    return tmpvec;
}

/*rowvec Data_container::get_line(unsigned int numline, int &y){

	rowvec tmpvec(dim);
	tmpvec.zeros();


	if(numline<num_total_data){
		unsigned long pos_file;
		unsigned int size, pos, offset;
		double val;

		streamindex.seekg(numline*size_ind_line,streamindex.beg);
		streamindex.read((char *)&pos_file,sizeof(unsigned long));
		streamindex.read((char *)&size,sizeof(unsigned int));
		byte *buffer=new byte[size];

		streamdata.seekg(pos_file,streamindex.beg);
		streamdata.read((char *)buffer,size);

		memcpy(&y,buffer,sizeof(int));

		offset=sizeof(int);
		while(offset<size){

			memcpy(&pos,buffer+offset,sizeof(unsigned int));
			offset+=sizeof(int);
			memcpy(&val,buffer+offset,sizeof(double));
			offset+=+sizeof(double);
			tmpvec.at(pos-1)=val;
		}
                delete []buffer;
                buffer=NULL;
		//tmpvec.print("tmpvec");
	}
	return tmpvec;
}*/

rowvec Data_container::get_line(unsigned int numline, int &y){
    rowvec tmpvec(dim);
    tmpvec.zeros();
    if(numline<num_total_data){
        int offset;
        float val;

        byte *buffer=new byte[(dim+1)*sizeof(int)];
        streamdata.seekg(numline*((dim+1)*sizeof(int)),streamdata.beg);
        streamdata.read((char *)buffer,(dim+1)*sizeof(int));

        memcpy(&y,buffer,sizeof(int));

        //cout<<"y: "<<y<<endl;

        offset=sizeof(int);
        int posVector=0;
        while(offset<dim){
            memcpy(&val,buffer+offset,sizeof(float));
            offset+=sizeof(float);
            tmpvec.at(posVector)=val;
            posVector++;
        }
        delete []buffer;
        buffer=NULL;
        //tmpvec.print("tmpvec");
    }
    return tmpvec;
}


Data_container::~Data_container() {

    streamdata.close();
    streamindex.close();
}

