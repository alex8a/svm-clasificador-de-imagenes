#ifndef PREPROCESSOR_H
#define PREPROCESSOR_H
#include "QObject"
#include "fstream"
#include "vector"
#include "QPixmap"
#include <opencv2/opencv.hpp>

#include "ui_mainwindow.h"

#include <string>
using namespace std;

class Preprocessor: public QObject
{
    Q_OBJECT
public:
    Preprocessor(QString path, QString fileNameRead, QString fileNameWrite);
    QString fileNameRead;
    QString fileNameWrite;
    QString path;
public slots:
    void preprocesar(Ui::MainWindow *ui);
signals:
    void progress(int);
};

#endif // PREPROCESSOR_H
