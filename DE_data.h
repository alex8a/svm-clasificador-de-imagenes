/* 
 * File:   DE_data.h
 * Author: black
 *
 * Created on February 23, 2015, 2:57 PM
 */

#ifndef DE_DATA_H
#define	DE_DATA_H

typedef struct {
    pthread_mutex_t count_mutex, count_mutex2;
    pthread_cond_t condition_var1, condition_var2, condition_var3;

    int cont_thread, cont_thread2, final_iter, num_vecs, num_alphas;
    double  fval_leader, size_of_batch, b;
    float F, crsv, C, std;

    bool block, reval;
    rowvec  *leader, *fval_VecsBest, *bias_xi, *w, *cond_val, *pos_replace, **xi;
    uword ind_GBest;
    Data_container *dc;
    LinKer *xixj;
}DE_data;

#endif	/* DE_DATA_H */

