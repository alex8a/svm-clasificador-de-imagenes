/* 
 * File:   svm.h
 * Author: black
 *
 * Created on 25 de agosto de 2014, 03:53 PM
 */


#ifndef SVM_H_
#define	SVM_H_

#include <cstdlib>
#include <ctime>
#include <armadillo>
#include <cmath>
#include "LinKer.h"
#include "Datacontainer.h"
#include "DE_data.h"
#include "resultsvm.h"

using namespace arma;

class SVM {
public:
    SVM();
    double evaluate_prim(rowvec alpha, LinKer *K, float C, rowvec *w, double &b);
    void form_w(rowvec alpha, LinKer *K, float C, rowvec *w, double &b);
    double evaluate_dual(rowvec alpha, LinKer *K, double &cond_val);
    double evaluate_dual(rowvec alpha, int pos_mod, double ai_mod, LinKer *K, double &cond_val);
    ResultSVM* classify(rowvec *w, double b, Data_container *dc, bool line_per_line=true);
    double classify(rowvec *w, Data_container *dc, bool line_per_line=true, bool mult_y=false);
    double check_condition(LinKer *K, rowvec alpha);
    ~SVM();
    
private:


};

#endif	/* SVM_H */

