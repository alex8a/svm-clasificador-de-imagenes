/* 
 * File:   DE_data.h
 * Author: black
 *
 * Created on February 23, 2015, 2:57 PM
 */

#ifndef BEECOL_DATA_H
#define BEECOL_DATA_H
#include "QMutex"
#include "QWaitCondition"
#include "QThread"
#include "Datacontainer.h"
#include "LinKer.h"

typedef struct {
    pthread_mutex_t count_mutex, *array_mutex;
    pthread_cond_t condition_var1, condition_var2, condition_var3;

    int cont_thread, final_iter, num_sources, num_alphas, limit_trials, trial_cont, suc_trials, *rank_pos;
    double  size_of_batch, b, C, F, sum_fit, fval_bpos, FCR;

    bool block, flag_update, flag_release;
    rowvec   *nectar_Si, *w, *cond_val, *bias_Si, *fit, *pm, *unsuc_trials, *Best_Pos, **Si;
    Data_container *dc;
    LinKer *xixj;
    
}BeeCol_data;

#endif	/* BEECOL_DATA_H */

