#include "entrenador.h"
#include "SVM.h"
#include "Fullextractor_norm.h"
#include "Lineextractor_norm.h"
#include "Evolalg.h"
#include "Bee_Col_ada.h"
#include "mBee_Col_ada.h"
#include "DE_ada.h"
#include "PSO_ada.h"
#include <mainwindow.h>

Entrenador::Entrenador()
{

}

void Entrenador::entrenar(Ui::MainWindow *ui){
    //for(int i=0;i < 15;i++) emit progress(i);
    //Se obtiene la dimension de los datos de la interfaz
    this->dim = ui->txtDim->text().toInt();
    //Se genera el vector de pesos de la SVM
    w = new rowvec(dim);
    //Se obtiene el nombre el archivo de dataset de la interfaz
    string path = ui->txtDataSet->text().toStdString();
    string datafile = path;
    //Se declara el objeto para recoger los datos del algoritmo evolutivo
    ResultSVM *resultTrain;
    switch (ui->comboAlgo->currentIndex()+1) {
        case 1:
        //Se invoca el algoritmo de colonia de abeja artificiales
            resultTrain=bee_col_ada(dim, path, datafile);
            break;
        case 2:
        //Se invoca el algoritmo de colonia de abeja artificiales
            resultTrain=mbee_col_ada(dim, path, datafile);
            break;
        case 3:
        //Se invoca el algoritmo de evolución diferencial
            resultTrain=de_ada(dim, path, datafile);
            break;
        default:
            break;
        }
    emit progress(100);
    if(resultTrain){
        //Se utilizan los datos arrojados por el algoritmo evolutivo
        cout<<"TAMAÑO DE W "<<w->size()<<endl;
        cout<<"Total data: "<<resultTrain->totalNumData<<endl;
        cout<<"Total good: "<<resultTrain->totalGood<<endl;
        cout<<"Total goodpos: "<<resultTrain->totalGoodPos<<endl;
        cout<<"Total goodneg: "<<resultTrain->totalGoodNeg<<endl;
        ui->lblNumData->setText(QString::number(resultTrain->totalNumData));
        ui->lblGood->setText(QString::number(resultTrain->totalGood));
        ui->lblWrong->setText(QString::number(resultTrain->totalNumData-resultTrain->totalGood));
        ui->lblTruePos->setText(QString::number(resultTrain->totalGoodPos));
        ui->lblTrueNeg->setText(QString::number(resultTrain->totalGoodNeg));
        ui->lblPercent->setText(QString::number((double) resultTrain->totalGood / resultTrain->totalNumData * 100)+"%");
        ui->tabTest->setEnabled(true);
        ui->lblTestInfo->setText("SVM entrenada para datos de "+QString::number(dim)+" dimensiones.");
        cout<<w->size()<<endl;
    }
}

int Entrenador::countZerosInW(){
    int counter0=0;
    int wSize = (int)w->size();
    for(int i=0;i<wSize;i++)
        if(w->at(i)==0)counter0++;
    return counter0;
}

ResultSVM* Entrenador::bee_col_ada(int dim, string path, string datafile){
    int num_data = 914, num_sources = 30, final_iter = 500, num_alphas=400;
    double F=1,C=0.09, limit_trials=50;
    cout << "Bee_Col_ADA" << endl;
    //Se actualiza el progreso en interfaz (5%)
    emit progress(5);
    //Fullextractor_norm extrae todos el dataset de golpe para el entrenamiento
    edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
    emit progress(25);
    //El objeto Bee_Col_ada es el encargado de entrenar la red por medio de algoritmos de enjambre de abejas
    Evol_alg *abca = new Bee_Col_ada(num_sources, final_iter, F, edata, C, num_alphas, limit_trials);
    emit progress(30);
    //corre el algoritmo de colonia de abejas
    abca->run();
    //W son los parametros de la red ya entrenada
    rowvec *temp = abca->get(b);
    for(int i=0;i<temp->size();i++){
        w->at(i)=temp->at(i);
    }
    emit progress(50);
    //Lineextractor_norm solo posee un unico dato del dataset
    clfdata = new Lineextractor_norm(dim, path, datafile);

    cout<<"w mide: "<<w->size()<<", "<<countZerosInW()<<" valores son iguales a 0"<<endl;
    cout<<"b: "<<b<<endl;
    //Se pone a prueba la SVM con los datos de entrenamiento
    result = svm.classify(w, b, clfdata);
    cout<<"0000-----000000"<<endl;
    emit progress(75);
    result = svm.classify(w, b, edata,false);

    delete clfdata;
    delete abca;
    delete edata;

    //delete r;

    cout << "Finish" << endl;
    return result;
}

ResultSVM* Entrenador::mbee_col_ada(int dim, string path, string datafile){

    int num_data = 914, final_iter = 1000, num_alphas=400;
    double F=1.8, C=0.1, limit_trials=100, FCR=0.5;

    cout << "mBee_Col_ADA" << endl;

    edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
    Evol_alg *abca = new mBee_Col_ada(final_iter, FCR, F, edata, C, num_alphas, limit_trials);

    abca->run();
    rowvec *temp = abca->get(b);
    for(int i=0;i<temp->size();i++){
        w->at(i)=temp->at(i);
    }

    clfdata = new Lineextractor_norm(dim, path, datafile);

    //w->print("w");
    cout<<"b: "<<b<<endl;
    result = svm.classify(w, b, clfdata);
    cout<<"0000-----000000"<<endl;
    result = svm.classify(w, b, edata,false);

    delete clfdata;
    delete abca;
    delete edata;
    //delete r;

    cout << "Finish" << endl;
    return result;
}

ResultSVM* Entrenador::de_ada(int dim, string path, string datafile){

    int num_data = 914, num_vecs = 30, final_iter = 1000, num_alphas=400;
    double F=0.8, crsv=0.5, C=0.1;

    cout << "DE_ADA" << endl;

    edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
    Evol_alg *de = new DE_ada(F, crsv, num_vecs, final_iter, edata, C, num_alphas);

    de->run();
    rowvec *temp = de->get(b);
    for(int i=0;i<temp->size();i++){
        w->at(i)=temp->at(i);
    }

    clfdata = new Lineextractor_norm(dim, path, datafile);

    //w->print("w");
    cout<<"b: "<<b<<endl;
    result = svm.classify(w, b, clfdata);
    cout<<"0000-----000000"<<endl;
    result = svm.classify(w, b, edata,false);

    delete clfdata;
    delete de;
    delete edata;
    //delete r;

    cout << "Finish" << endl;
    return result;
}

ResultSVM* Entrenador::pso_ada(int dim, string path, string datafile){
    double c1=2, c2=2, C=10, w_start=1.5, w_end=0.0001, Vmax=1.5;
    int swarm_size=5, num_data = 400, final_iter = 1000, num_alphas=400;

    cout << "PSO_ADA" << endl;

    edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
    Evol_alg *pso = new PSO_ada(c1, c2, C, w_start, w_end, Vmax, swarm_size, final_iter, edata, num_alphas);

    pso->run();
    rowvec *temp = pso->get(b);
    for(int i=0;i<temp->size();i++){
        w->at(i)=temp->at(i);
    }

    clfdata = new Lineextractor_norm(dim, path, datafile);

    //w->print("w");
    cout<<"b: "<<b<<endl;
    result = svm.classify(w, b, clfdata);
    cout<<"0000-----000000"<<endl;
    result = svm.classify(w, b, edata,false);

    delete clfdata;
    delete pso;
    delete edata;
    //delete r;

    cout << "Finish" << endl;
    return result;
}
