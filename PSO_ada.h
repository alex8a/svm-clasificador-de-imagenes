/*
 * PSO.h
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#ifndef PSO_ADA_H_
#define PSO_ADA_H_

#include <pthread.h>
#include <cstdlib>
#include <chrono>

#include "LinKer.h"
#include "Evolalg.h"
#include "PSOdata_ada.h"
#include "Datacontainer.h"
#include "Adatron.h"

class PSO_ada : public Evol_alg {
public:
	PSO_ada(double c1, double c2, double C, double w_start, double w_end, double Vmax,
	        int swarm_size, int final_iter, Data_container *edata, int num_alphas);
	virtual ~PSO_ada();
	void run();
        void run(LinKer *lk);
	rowvec *get(double &b);

private:
    void form_w();
    static void *updateSwarm(void *threadarg);
    static void *evalSwarm(void *threadarg);
    static void write_partval(swarm_data_ada *swarm);
    swarm_data_ada *swarmdt;

};

#endif /* PSO_ADA_H_ */
