/* 
 * File:   Roc.h
 * Author: black
 *
 * Created on March 24, 2015, 7:59 PM
 */

#ifndef ROC_H
#define	ROC_H
#include <armadillo>
#include "Lineextractor.h"

using namespace std;
using namespace arma;

class Roc {
public:
    Roc(int dim, string path, string datafile, rowvec w, double b, int div_range);
    void write_curve_vals(string destination, bool append);
    virtual ~Roc();
    
private:
    void get_extremes();
    void obtain_vals();
    Lineextractor *le, *letmp;
    rowvec w, *c1, *c2, *acum_c1, *acum_c2;
    double pos_ext, neg_ext, b;
    int div_range;
    string destination;
    ofstream write_file;
};

#endif	/* ROC_H */

