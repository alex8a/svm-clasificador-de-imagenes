/* 
 * File:   ff.cpp
 * Author: black
 * 
 * Created on October 19, 2015, 4:20 PM
 */

#include "ff.h"

extern double time_alg;
ff::ff(string name_alg, int num_alphas, string path, int dim) {
    
    evd=new ev_data;
    evd->num_folds=10;
    evd->condition_var1 = PTHREAD_COND_INITIALIZER;
    evd->count_mutex = PTHREAD_MUTEX_INITIALIZER;
    evd->cont_thread=0;
    evd->acum_gen=0;
    evd->num_alphas=num_alphas;
    evd->path=path;
    evd->name_alg=name_alg;
    evd->dim=dim;
    
    vector<string> dirlist_data, dirlist_index;
    
    dirlist_data.resize(evd->num_folds);
    dirlist_index.resize(evd->num_folds);
    
    get_file_names(path, dirlist_data, dirlist_index);
    
    evd->dirlist_data=dirlist_data;
    evd->dirlist_index=dirlist_index;
    
    evd->clfdata=new Data_container*[evd->num_folds];
    evd->lk=new LinKer*[evd->num_folds];
    
    for(int i=0; i< evd->num_folds; i++){
        cout<<"LK-"<<i<<endl;
        evd->clfdata[i]=new Fullextractor_norm(evd->dim, num_alphas, path, evd->dirlist_data.at(i), true, false);
        evd->clfdata[i]->run();
        evd->lk[i]=new LinKer(num_alphas, evd->clfdata[i]->dim, false);
        evd->lk[i]->fill_mod(evd->clfdata[i]); 
    }
}

void *ff::fasteval(void *threadarg) {

    ev_data * ev = (ev_data *) threadarg;

    
    pthread_mutex_lock(&ev->count_mutex);

    int numfold;
    Evol_alg *ea;
    
    double genres, b;
    
    rowvec *w;
    
    numfold = ev->cont_thread;
    ev->cont_thread++;

    if (ev->cont_thread < ev->num_folds){
        //cout<<"IN: "<<numfold<<endl;
        pthread_cond_wait(&ev->condition_var1, &ev->count_mutex);
        //cout<<"OUT: "<<numfold<<endl;
    }else {
        //cout<<"IN: "<<numfold<<endl;
        ev->cont_thread = 0;
        pthread_cond_broadcast(&ev->condition_var1);
        //cout<<"OUT: "<<numfold<<endl;
    }

    //pthread_mutex_unlock(&ev->count_mutex);
    
    
    
    //pthread_mutex_lock(&ev->count_mutex);
    if (strcmp(ev->name_alg.c_str(), "DE") == 0)
        ea = new DE_ada(ev->F, ev->crsv, ev->num_ind, ev->final_iter, ev->clfdata[numfold], ev->C, ev->num_alphas);
    else 
    if (strcmp(ev->name_alg.c_str(), "ABC") == 0)
        ea = new Bee_Col_ada(ev->num_ind, ev->final_iter, ev->F, ev->clfdata[numfold], ev->C, ev->num_alphas, ev->limit_trials);
    else
    if (strcmp(ev->name_alg.c_str(), "mABC") == 0)  
        ea = new mBee_Col_ada(ev->final_iter, ev->FCR, ev->F, ev->clfdata[numfold], ev->C, ev->num_alphas, ev->limit_trials);
    else
        ea = new PSO_ada(ev->c1, ev->c2, ev->C, ev->w_start, ev->w_end, ev->Vmax, ev->num_ind, ev->final_iter, ev->clfdata[numfold], ev->num_alphas);
    
    //pthread_mutex_unlock(&ev->count_mutex);
    
    //pthread_mutex_lock(&ev->count_mutex);
    //cout<<"---------->RUN: "<<numfold<<endl;
    ea->run(ev->lk[numfold]);
    pthread_mutex_unlock(&ev->count_mutex); 
    
   
    
    w = ea->get(b);

    //cout<<"---------->CLF: "<<numfold<<endl;
    pthread_mutex_lock(&ev->count_mutex);
    SVM svm;
    ResultSVM *result =svm.classify(w, b, ev->clfdata[numfold], false);
    genres = result->percentGood;
    pthread_mutex_unlock(&ev->count_mutex);
    ev->acum_gen += genres;
    
    delete ea;

    //cout<<"---------->END: "<<numfold<<endl;
    //pthread_exit(NULL);
    return NULL;
}

float ff::evaluate(rowvec eval){
    
    float perc;
    evd->acum_gen = 0;
    evd->cont_thread = 0;

    if (strcmp(evd->name_alg.c_str(), "DE") == 0) {
        ff_de(eval);
    } else
        if (strcmp(evd->name_alg.c_str(), "ABC") == 0) {
        evd->name_alg = "ABC";
        ff_abc(eval);
    } else
        if (strcmp(evd->name_alg.c_str(), "mABC") == 0) {
        evd->name_alg = "mABC";
        ff_mabc(eval);
    } else {
        evd->name_alg = "PSO";
        ff_pso(eval);
    }
    
    
    /*int id, rc;
    pthread_t threadsEval[evd->num_folds];

    for (id = 0; id < evd->num_folds; id++) {
        rc = pthread_create(&threadsEval[id], NULL, fasteval,
                (void*) &(*evd));

        if (rc) {
            cout << "Error:unable to create thread threadsUpdateSwarm,"
                    << rc << endl;
            exit(EXIT_FAILURE);
        }
    }

    for (id = 0; id < evd->num_folds; id++) {
        rc = pthread_join(threadsEval[id], NULL);

        if (rc) {
            cout << "Error:unable to join threadsUpdateSwarm," << rc
                    << endl;
            exit(EXIT_FAILURE);
        }
    }*/
    
    int numfold;
    Evol_alg *ea;
    SVM svm;
    double genres, b;
    
    rowvec *w;

    for (numfold = 0; numfold < evd->num_folds; numfold++) {

        if (strcmp(evd->name_alg.c_str(), "DE") == 0)
            ea = new DE_ada(evd->F, evd->crsv, evd->num_ind, evd->final_iter, evd->clfdata[numfold], evd->C, evd->num_alphas);
        else
            if (strcmp(evd->name_alg.c_str(), "ABC") == 0)
            ea = new Bee_Col_ada(evd->num_ind, evd->final_iter, evd->F, evd->clfdata[numfold], evd->C, evd->num_alphas, evd->limit_trials);
        else
            if (strcmp(evd->name_alg.c_str(), "mABC") == 0)
            ea = new mBee_Col_ada(evd->final_iter, evd->FCR, evd->F, evd->clfdata[numfold], evd->C, evd->num_alphas, evd->limit_trials);
        else
            ea = new PSO_ada(evd->c1, evd->c2, evd->C, evd->w_start, evd->w_end, evd->Vmax, evd->num_ind, evd->final_iter, evd->clfdata[numfold], evd->num_alphas);

        ea->run(evd->lk[numfold]);



        w = ea->get(b);

        ResultSVM *result=svm.classify(w, b, evd->clfdata[numfold], false);
        genres = result->percentGood;
        evd->acum_gen += genres;

        delete ea;
    }
    perc=evd->acum_gen/evd->num_folds;
    cout << "Finish ------------ " <<perc<< endl;   
    return perc;
}

void ff::ff_de(rowvec eval){

    evd->num_ind = eval.at(0); 
    evd->final_iter = eval.at(1); 
    evd->F=eval.at(2);
    evd->crsv=eval.at(3);
    evd->C=eval.at(4);
    
}

void ff::ff_abc(rowvec eval){
    
    evd->num_ind = eval.at(0); 
    evd->final_iter = eval.at(1); 
    evd->F=eval.at(2);
    evd->C=eval.at(3);
    evd->limit_trials=eval.at(4);

}

void ff::ff_mabc(rowvec eval){
        
    evd->final_iter = eval.at(0); 
    evd->F=eval.at(1);
    evd->C=eval.at(2);
    evd->limit_trials=eval.at(3);
    evd->FCR=eval.at(4);

     
}

void ff::ff_pso(rowvec eval){
        
    evd->num_ind = eval.at(0); 
    evd->final_iter = eval.at(1); 
    evd->c1 = eval.at(2);
    evd->c2=eval.at(3);
    evd->C=eval.at(4);
    evd->w_start=eval.at(5);
    evd->w_end=eval.at(6);
    evd->Vmax=eval.at(7);

}

void ff::get_file_names(string dir_path, vector<string> &dirlist_data, vector<string> &dirlist_index){
    
    DIR *d=NULL;
    struct dirent *dir;
    string d_name, subs;

    
    d = opendir(dir_path.c_str());


    if (d) {
        while ((dir = readdir(d)) != NULL) {
            d_name=dir->d_name;
            if(d_name.compare(0,13,"saved_indexes")==0){
                subs=d_name.substr(13,2);
                dirlist_index.at(atoi(subs.c_str()))=dir->d_name;
                
            }else
            if(d_name.compare(0,10,"saved_data")==0){
                subs=d_name.substr(10,2);
                dirlist_data.at(atoi(subs.c_str()))=dir->d_name;

            }
            
        }
        
        delete dir;
    }
        closedir(d);
        
        
}

ff::~ff() {
    /*for(int i=0; i< evd->num_folds; i++){
        delete evd->clfdata[i];
        delete evd->lk[i];
    }

    delete []evd->clfdata;
    delete []evd->lk;
    delete evd;*/
}

