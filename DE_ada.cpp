 /* 
 * File:   DE.cpp
 * Author: black
 * 
 * Created on February 20, 2015, 7:29 PM
 */

#include "DE_ada.h"
extern double time_alg;

DE_ada::DE_ada(double F, double crsv, int num_vecs, int final_iter, 
        Data_container *dc, double C, int num_alphas) {

    try {

        ded = new DE_data;
        ded->F = F;
        ded->crsv = crsv;
        ded->dc = dc;
        ded->final_iter = final_iter;
        ded->ind_GBest = 0;
        ded->xi = new rowvec*[num_vecs];
        ded->fval_VecsBest = new rowvec(num_vecs);
        ded->num_vecs = num_vecs;
        ded->w = new rowvec(dc->dim);
        ded->pos_replace = new rowvec(num_alphas);
        ded->num_alphas = num_alphas;

        ded->cont_thread = 0;
        ded->cont_thread2 = 0;
        
        ded->condition_var1 = PTHREAD_COND_INITIALIZER;
        ded->condition_var2 = PTHREAD_COND_INITIALIZER;
        ded->condition_var3 = PTHREAD_COND_INITIALIZER;
        ded->count_mutex = PTHREAD_MUTEX_INITIALIZER;
        ded->count_mutex2 = PTHREAD_MUTEX_INITIALIZER;
        ded->reval = false;
        ded->leader = new rowvec(num_alphas);
        ded->xixj = new LinKer(num_alphas, dc->dim, false);

        ded->bias_xi = new rowvec(num_vecs);
        
        ded->C = C;

    } catch (std::bad_alloc& ba) {
        std::cerr << "bad_alloc caught in PSO constructor: " << ba.what() << '\n';
        exit(EXIT_FAILURE);
    }
}

void *DE_ada::updateVecs(void *threadarg) {

    try {
        DE_data * ded = (DE_data *) threadarg;
        rowvec  *ui, *tmp_vec, *rcj;
        double  fval_Vec, bias, F, maxF = 1.2, minF = 0.1, tmp = 0;
        int r1 = 0, r2 = 0, j, Jr, numvec;
        Adatron objfunc(ded->num_alphas);


        pthread_mutex_lock(&ded->count_mutex);

        numvec = ded->cont_thread;
        
        ded->cont_thread++;

        if (ded->cont_thread < ded->num_vecs)
            pthread_cond_wait(&ded->condition_var1, &ded->count_mutex);
        else {
            ded->cont_thread = 0;
            pthread_cond_broadcast(&ded->condition_var1);
        }

        pthread_mutex_unlock(&ded->count_mutex);

        ded->fval_VecsBest->at(numvec) = objfunc.eval(ded->xi[numvec], ded->xixj, bias);
        ded->bias_xi->at(numvec) = bias;

        ui = new rowvec(ded->xixj->num_alphas);
        rcj = new rowvec(ded->xixj->num_alphas);
        
        arma_rng::set_seed(time(NULL)+numvec);///<-------------
        srand(time(NULL)+numvec);

        while (ded->block) {

            
            r1 = round((ded->num_vecs - 1) * ((double) rand() / RAND_MAX));
            for (r2 = r1; r2 == r1;)
                r2 = round((ded->num_vecs - 1) * ((double) rand() / RAND_MAX));

            Jr = round((ded->xixj->num_alphas - 1) * ((double) rand() / RAND_MAX));

            F = minF + (maxF - minF)*((double) rand() / RAND_MAX);

            rcj->randu();
            
            for (j = 0; j < ded->xixj->num_alphas; j++) {
                //rcj = ((double) rand() / RAND_MAX);
                if (rcj->at(j) < ded->crsv || j == Jr) {

                    tmp = ded->xi[ded->ind_GBest]->at(j) + F * (ded->xi[r1]->at(j) - ded->xi[r2]->at(j));
                    
                    if (tmp >= 0 && tmp <= ded->C)
                        ui->at(j) = tmp;
                    else
                    if (tmp < 0)
                        ui->at(j) = 0;
                    else
                        ui->at(j) = ded->C;
                } else
                    ui->at(j) = ded->xi[numvec]->at(j);

            }


            fval_Vec = objfunc.eval(ui, ded->xixj, bias);

            
            pthread_mutex_lock(&ded->count_mutex2);
                ded->cont_thread2++;;

                if (fval_Vec < ded->fval_VecsBest->at(numvec)) {

                    ded->fval_VecsBest->at(numvec) = fval_Vec;

                    if (ded->cont_thread2 < ded->num_vecs) 
                        pthread_cond_wait(&ded->condition_var3, &ded->count_mutex2);

                    tmp_vec=ded->xi[numvec];
                    ded->xi[numvec] = ui;
                    ui=tmp_vec;
                    //ui = new rowvec(ded->xixj->num_alphas);
                    ded->bias_xi->at(numvec) = bias;
                }

                if (ded->cont_thread2 == ded->num_vecs) {
                    ded->cont_thread2=0;
                    pthread_cond_broadcast(&ded->condition_var3);

                }
            
            pthread_mutex_unlock(&ded->count_mutex2);
            
            
            
            pthread_mutex_lock(&ded->count_mutex);
                ded->cont_thread++;

                if (ded->cont_thread >= ded->num_vecs) 
                    pthread_cond_signal(&ded->condition_var2);


                pthread_cond_wait(&ded->condition_var1, &ded->count_mutex);

            pthread_mutex_unlock(&ded->count_mutex);
            
        }

        delete ui;

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    
    pthread_exit(NULL);
    return threadarg;
}

void *DE_ada::evalVecs(void *threadarg) {

    try {
        
        DE_data * ded = (DE_data *) threadarg;
        uword ind_BestP;
        int iter;
        double maxF = 0.9, minF = 0.000001, tmpfval_leader, tmpfval_cond, tmpb;
        //SVM objfunc;

        pthread_mutex_lock(&ded->count_mutex);

        pthread_cond_wait(&ded->condition_var2, &ded->count_mutex);

        for (iter = 0; iter <= ded->final_iter; iter++) {

            ded->fval_VecsBest->min(ind_BestP);

            ded->reval = false;

            if ((ded->fval_VecsBest->at(ded->ind_GBest) > ded->fval_VecsBest->at(ind_BestP))) {
                //cout << "Ind GBest - " << iter << ": " << ded->fval_VecsBest->at(ind_BestP) << endl;
                ded->ind_GBest = ind_BestP;
                ded->b = ded->bias_xi->at(ind_BestP);

                if (ded->fval_VecsBest->at(ded->ind_GBest) <= 0.001)
                    break;
            }

            ded->cont_thread = 0;

            pthread_cond_broadcast(&ded->condition_var1);
            pthread_cond_wait(&ded->condition_var2, &ded->count_mutex);
            ded->F = maxF - (maxF - minF) / ded->final_iter*iter;
        }

        ded->block = false;

        pthread_cond_broadcast(&ded->condition_var1);

        pthread_mutex_unlock(&ded->count_mutex);
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
    return threadarg;
}

void DE_ada::write_partval(DE_data *ded) {

    remove("/home/black/workspace/ff/pts.txt");
    /*if (remove("/home/black/workspace/ff/pts.txt") != 0)
        perror("Error deleting file:pts");
    else
        puts("File successfully deleted:pts");*/

    ofstream *pts;
    pts = new ofstream("/home/black/workspace/ff/pts.txt", ios::out | ios::app);

    for (int i = 0; i < ded->num_vecs; i++) {
        for (int j = 0; j < ded->xixj->num_alphas; j++)
            *pts << ded->xi[i]->at(j) << ' ';
        *pts << endl;
    }

    pts->close();
}

void DE_ada::run() {

    try {
        int rc, id, c_times = 0, i;
        double ran_num;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);

        if (!ded->dc->end_extraction()) {
            auto duration = 0;
            high_resolution_clock::time_point t1, t2;
            
            srand(time(NULL));

            ded->dc->run();
            
            arma_rng::set_seed(time(NULL));
            
            for (i = 0; i < ded->num_vecs; i++) {
                ded->xi[i]=new rowvec(ded->xixj->num_alphas);
                ded->xi[i]->randu();
                *ded->xi[i]=*ded->xi[i]*ded->C;
                /*for (unsigned int j = 0; j < ded->xixj->num_alphas; j++) {
                    ran_num = ded->C * ((double) rand() / RAND_MAX);
                    ded->xi[i]->at(j) = ran_num;
                }*/
                
                *ded->xi[i] = normalise(*ded->xi[i],1);
            }

            ded->xixj->fill(ded->dc);
            
            t1 = high_resolution_clock::now();
            cout << "Run" << endl;

            pthread_t threadEvalVecs, threadsUpdateVecs[ded->num_vecs];

            ded->block = true;

            rc = pthread_create(&threadEvalVecs, NULL, evalVecs, (void*) &(*ded));

            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < ded->num_vecs; id++) {
                rc = pthread_create(&threadsUpdateVecs[id], NULL, updateVecs,
                        (void*) &(*ded));

                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm,"
                            << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalVecs, NULL);

            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < ded->num_vecs; id++) {
                rc = pthread_join(threadsUpdateVecs[id], NULL);

                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc
                            << endl;
                    exit(EXIT_FAILURE);
                }
            }

            c_times++;
            t2 = high_resolution_clock::now();
            //cout << "Iter: " << c_times << endl;

            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000 << endl;
            time_alg += (float) duration / 1000000;
            cout <<"GBest: "<<ded->fval_VecsBest->at(ded->ind_GBest)<<endl;
            
            //ded->xi->row(ded->ind_GBest).print("alpha");
            form_w();
            
             for (i = 0; i < ded->num_vecs; i++) 
                delete ded->xi[i];
            
            /*Adatron ad(ded->num_alphas, ded->ig_ran_min, ded->ig_ran_max);
            cout << "Class" << endl;
            ad.classify(ded->w, ded->b, ded->dc, false);*/

        }

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void DE_ada::run(LinKer *lk) {

    try {
        erase_lk=false;
        int rc, id, c_times = 0, i;
        double ran_num;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);

            auto duration = 0;
            high_resolution_clock::time_point t1, t2;
            
            srand(time(NULL));

            //ded->dc->run();
            //ded->xixj->fill_mod(ded->dc);
            ded->xixj=lk;
           
            for (i = 0; i < ded->num_vecs; i++) {
                ded->xi[i]=new rowvec(ded->xixj->num_alphas);
                ded->xi[i]->randu();
                *ded->xi[i]=*ded->xi[i]*ded->C;
                /*for (unsigned int j = 0; j < ded->xixj->num_alphas; j++) {
                    ran_num = ded->C * ((double) rand() / RAND_MAX);
                    ded->xi[i]->at(j) = ran_num;
                }*/
                
               *ded->xi[i] = normalise(*ded->xi[i],1);
            }
            
            t1 = high_resolution_clock::now();
            cout << "Run" << endl;

            pthread_t threadEvalVecs, threadsUpdateVecs[ded->num_vecs];

            ded->block = true;

            rc = pthread_create(&threadEvalVecs, NULL, evalVecs, (void*) &(*ded));

            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < ded->num_vecs; id++) {
                rc = pthread_create(&threadsUpdateVecs[id], NULL, updateVecs,
                        (void*) &(*ded));

                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm,"
                            << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalVecs, NULL);

            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < ded->num_vecs; id++) {
                rc = pthread_join(threadsUpdateVecs[id], NULL);

                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc
                            << endl;
                    exit(EXIT_FAILURE);
                }
            }

            c_times++;
            t2 = high_resolution_clock::now();
            //cout << "Iter: " << c_times << endl;

            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000<< endl;
            time_alg += (float) duration / 1000000;
            cout <<"GBest: "<<ded->fval_VecsBest->at(ded->ind_GBest)<<endl;
            
            //ded->xi->row(ded->ind_GBest).print("alpha");
            form_w();
            
             for (i = 0; i < ded->num_vecs; i++) 
                delete ded->xi[i];
            
            /*Adatron ad(ded->num_alphas, ded->ig_ran_min, ded->ig_ran_max);
            cout << "Class" << endl;
            ad.classify(ded->w, ded->b, ded->dc, false);*/

        

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void DE_ada::form_w() {
    int i, dim_alph = ded->num_alphas;

    ded->w->zeros();

   for (i = 0; i < dim_alph; i++)
        *ded->w += ded->xixj->y_i->at(i) * ded->xixj->x_ij->row(i) * ded->xi[ded->ind_GBest]->at(i);
}

rowvec *DE_ada::get(double &b) {
    b = ded->b;
    return ded->w;
}

DE_ada::~DE_ada() {

    delete ded->fval_VecsBest;
    delete [] ded->xi;
    delete ded->w;
    delete ded->pos_replace;
    delete ded->leader;
    
    if(erase_lk)
        delete ded->xixj;
    
    delete ded->bias_xi;
    delete ded;
}

