/* 
 * File:   Bee_Col.cpp
 * Author: black
 * 
 * Created on July 1, 2015, 1:10 PM
 */

#include "mBee_Col_ada.h"
extern double time_alg;

mBee_Col_ada::mBee_Col_ada(int final_iter, double FCR, double F, Data_container *dc,
        double C, int num_alphas, double limit_trials) {

    try {

        bec = new BeeCol_data;
        bec->dc = dc;
        bec->final_iter = final_iter;
        bec->C = C;
        bec->num_sources = 3;
        bec->num_alphas = num_alphas;
        bec->F = F;
        bec->sum_fit = 0;
        bec->b = 0;
        bec->limit_trials = limit_trials;
        bec->FCR = FCR;
        bec->trial_cont=0;
        bec->suc_trials=0;
        
        /*bec->condition_var1 = PTHREAD_COND_INITIALIZER;
        bec->condition_var2 = PTHREAD_COND_INITIALIZER;
        bec->count_mutex = PTHREAD_MUTEX_INITIALIZER;*/

        //bec->array_mutex = new QMutex[bec->num_sources];
        bec->array_mutex = new pthread_mutex_t[bec->num_sources];

        for (int i = 0; i < bec->num_sources; i++)
            bec->array_mutex[i] = PTHREAD_MUTEX_INITIALIZER;

        bec->rank_pos=new int[3];
        bec->Best_Pos = new rowvec(num_alphas);
        bec->bias_Si = new rowvec(bec->num_sources);
        bec->w = new rowvec(dc->dim);
        bec->xixj = new LinKer(num_alphas, dc->dim, false);
        bec->Si = new rowvec*[bec->num_sources];
        
        for(int i=0; i<bec->num_sources; i++)
            bec->Si[i]=new rowvec(num_alphas);
                    
        bec->fit = new rowvec(bec->num_sources);
        bec->nectar_Si = new rowvec(bec->num_sources);
        bec->pm = new rowvec(bec->num_sources);
        bec->flag_update = true;
        
        bec->flag_release = false;

    } catch (std::bad_alloc& ba) {
        std::cerr << "bad_alloc caught in PSO constructor: " << ba.what() << '\n';
        exit(EXIT_FAILURE);
    }
}

void *mBee_Col_ada::updateSources(void *threadarg) {

    try {
        BeeCol_data * bec = (BeeCol_data *) threadarg;
        double fi, nectar_vi, tmp = 0, bias, v_ij = 0, prob, ran_num;
        int r = 0, i, j, phase, cont, num_source, rpos;
        bool flag_change;
        rowvec *v_i, *ran_num_vec;
        Adatron objfunc(bec->num_alphas);
        
        v_i = new rowvec(bec->num_alphas);
        ran_num_vec = new rowvec(bec->num_alphas);
        
        //bec->count_mutex.lock();
        pthread_mutex_lock(&bec->count_mutex);

            num_source = bec->cont_thread;
            bec->cont_thread++;

            if (bec->cont_thread < bec->num_sources)
                //bec->condition_var1.wait(&bec->count_mutex);
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
            else {
                bec->cont_thread = 0;
                //bec->condition_var1.wakeAll();
                pthread_cond_broadcast(&bec->condition_var1);
            }

        //bec->count_mutex.unlock();
        pthread_mutex_unlock(&bec->count_mutex);

        srand(time(NULL)+num_source);//<---Moved here
        arma_rng::set_seed(time(NULL)+num_source);//Agregado para arma
        /*for (j = 0; j < bec->xixj->num_alphas; j++) {
            ran_num = bec->C * ((double) rand() / RAND_MAX);
            bec->Si[num_source]->at(j) = ran_num;
        }*/

        ran_num_vec->randu();
        *ran_num_vec=bec->C * (*ran_num_vec);
        *bec->Si[num_source] = normalise(*ran_num_vec,1);

        bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
        
        bec->bias_Si->at(num_source) = bias;

        if (bec->nectar_Si->at(num_source) >= 0)
            bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
        else
            bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));

        //bec->count_mutex.lock();
        pthread_mutex_lock(&bec->count_mutex);

            bec->cont_thread++;

            if (bec->cont_thread >= bec->num_sources) {
                bec->fval_bpos = bec->nectar_Si->at(num_source);
                bec->Best_Pos = bec->Si[num_source];
                updateSumFit(bec);
                bec->cont_thread=0;
                //bec->condition_var1.wakeAll();
                pthread_cond_broadcast(&bec->condition_var1);
            }else
                //bec->condition_var1.wait(&bec->count_mutex);
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        //bec->count_mutex.unlock();
        pthread_mutex_unlock(&bec->count_mutex);
            
            bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;
            
        //bec->count_mutex.lock();
        pthread_mutex_lock(&bec->count_mutex);
            bec->cont_thread++;
            if (bec->cont_thread >= bec->num_sources) {
                bec->cont_thread = 0;
                bec->flag_update = false;
                //bec->condition_var1.wakeAll();
                pthread_cond_broadcast(&bec->condition_var1);
            } else
                //bec->condition_var1.wait(&bec->count_mutex);
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        //bec->count_mutex.unlock();
        pthread_mutex_unlock(&bec->count_mutex);


        //srand(time(NULL)+num_source);<<<---Moved up

        while (bec->block) {

            //pthread_mutex_lock(&bec->count_mutex);
            for (phase = 0; phase < 2; phase++) {

                flag_change = false;


                fi = -bec->F + (2 * bec->F * ((double) rand() / RAND_MAX));


                if (phase == 0) {//Employed Bees Phase

                    i = num_source;
                    bec->trial_cont++;
                } else {//Onlooker Bees Phase

                    ran_num = ((double) rand() / RAND_MAX);
                    prob = 0;

                    //bec->count_mutex.lock();
                    pthread_mutex_lock(&bec->count_mutex);
 //cout<<"1-i"<<endl;
                        bec->cont_thread++;

                        if (bec->cont_thread >= bec->num_sources){
                            updateSumFit(bec);
                            bec->cont_thread=0;
                            //bec->condition_var1.wakeAll();
                            pthread_cond_broadcast(&bec->condition_var1);
                        }else
                            //bec->condition_var1.wait(&bec->count_mutex);
                            pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
 //cout<<"1-o"<<endl;
                    //bec->count_mutex.unlock();
                    pthread_mutex_unlock(&bec->count_mutex);
                    
                    if(bec->flag_update)
                        bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;

                    //bec->count_mutex.lock();
                    pthread_mutex_lock(&bec->count_mutex);
//cout<<"2-i"<<endl;
                        bec->cont_thread++;

                        if (bec->cont_thread >= bec->num_sources) {
                            bec->cont_thread=0;
                            //bec->condition_var1.wakeAll();
                            pthread_cond_broadcast(&bec->condition_var1);
                        }else
                            //bec->condition_var1.wait(&bec->count_mutex);
                            pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
//cout<<"2-o"<<endl;
                    //bec->count_mutex.unlock();
                    pthread_mutex_unlock(&bec->count_mutex);
                    
                    for (cont = 0; cont <= bec->num_sources && ran_num > prob; cont++) {
                        prob += bec->pm->at(cont);
                        if (ran_num <= prob)
                            i = cont;
                    }
                    
                    bec->trial_cont++;
                }

                do {
                    r = round((bec->num_sources - 1) * ((double) rand() / RAND_MAX));
                } while (r == i);

                for (j = 0; j < bec->num_alphas; j++) {

                    if (((double) rand() / RAND_MAX) <= bec->FCR) {
                        v_ij = bec->Si[i]->at(j) + fi * (bec->Si[i]->at(j) - bec->Si[r]->at(j));

                        if (v_ij < 0)
                            v_ij = 0;
                        else
                            if (v_ij > bec->C)
                            v_ij = bec->C;
                        
                        v_i->at(j)=v_ij;
                    }else
                        v_i->at(j)= bec->Si[i]->at(j);
                }

                nectar_vi = objfunc.eval(v_i, bec->xixj, bias);
                

                //bec->array_mutex[i].lock();
                pthread_mutex_lock(&bec->array_mutex[i]);
//cout<<"3-i"<<endl;  
                    if (nectar_vi < bec->nectar_Si->at(i)) {

                         /*pthread_mutex_lock(&bec->count_mutex);
                        objfunc.calc_n_swap();
                         pthread_mutex_unlock(&bec->count_mutex);*/
                         
                        bec->Si[i] = v_i;
                        bec->nectar_Si->at(i) = nectar_vi;
                        bec->bias_Si->at(i) = bias;

                        if (bec->nectar_Si->at(i) >= 0)
                            bec->fit->at(i) = 1 / (1 + bec->nectar_Si->at(i));
                        else
                            bec->fit->at(i) = (1 + abs(bec->nectar_Si->at(i)));

                        bec->suc_trials++;
                        bec->flag_update=true;
                    } 
//cout<<"3-o"<<endl;               
                //bec->array_mutex[i].unlock();
                pthread_mutex_unlock(&bec->array_mutex[i]);

            }


            //bec->count_mutex.lock();
            pthread_mutex_lock(&bec->count_mutex);

                bec->cont_thread++;
//cout<<"4-i"<<endl;
                if (bec->cont_thread >= bec->num_sources) {
                    bec->cont_thread=0;
                    rank(bec);
                    //bec->condition_var1.wakeAll();
                    pthread_cond_broadcast(&bec->condition_var1);
                }else
                    //bec->condition_var1.wait( &bec->count_mutex);
                    pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
//cout<<"4-o"<<endl;
            //bec->count_mutex.unlock();
            pthread_mutex_unlock(&bec->count_mutex);


            if (bec->rank_pos[1] == num_source) {

                
                //do{<---Causa deadlock
                 //srand(time(NULL)*rand());//<--------
                    rpos = round((bec->num_alphas - 1)* ((double) rand() / RAND_MAX));
                    tmp=bec->C * ((double) rand() / RAND_MAX);
                    /*tmp=bec->Si->at(num_source,rpos)+bec->F*(bec->Si->at(num_source,rpos)-bec->Si->at(bec->rank_pos[0],rpos));
                    
                    if(tmp<0)
                        tmp=0;
                    else
                    if(tmp>bec->C)
                        tmp=bec->C;*/
                    
                //}while(tmp==bec->Si[bec->rank_pos[0]]->at(rpos));
                
                *bec->Si[num_source]=*bec->Si[bec->rank_pos[0]];
                bec->Si[num_source]->at(rpos)=tmp;
                
                /*for (j = 0; j < bec->xixj->num_alphas; j++) {
                    if(j!=rpos)
                        bec->Si[num_source]->at(j)=bec->Si[bec->rank_pos[0]]->at(j);
                }*/

                bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
                bec->bias_Si->at(num_source) = bias;

                if (bec->nectar_Si->at(num_source) >= 0)
                    bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
                else
                    bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));
                
            } else
                if (bec->rank_pos[2] == num_source) {
                /*for (j = 0; j < bec->xixj->num_alphas; j++) {
                ran_num = bec->C * ((double) rand() / RAND_MAX);
                bec->Si[num_source]->at(j) = ran_num;
            }

                 *bec->Si[num_source] = normalise(*bec->Si[num_source],1);*/

                ran_num_vec->randu();
                *ran_num_vec = bec->C * (*ran_num_vec);
                *bec->Si[num_source] = normalise(*ran_num_vec, 1);

                bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
                bec->bias_Si->at(num_source) = bias;

                if (bec->nectar_Si->at(num_source) >= 0)
                    bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
                else
                    bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));
            }
            //////////////////////////////////////////////

            //bec->count_mutex.lock();
            pthread_mutex_lock(&bec->count_mutex);
 //cout<<"5-i"<<endl;
                bec->cont_thread++;

                if (bec->cont_thread >= bec->num_sources) {
                    //bec->condition_var2.wakeOne();
                    pthread_cond_signal(&bec->condition_var2);
                    bec->flag_update = false;
                }

                //bec->condition_var1.wait(&bec->count_mutex);
                pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
 //cout<<"5-o"<<endl;
            //bec->count_mutex.unlock();
            pthread_mutex_unlock(&bec->count_mutex);

        }
        

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
    return threadarg;
}

void mBee_Col_ada::updateSumFit(BeeCol_data *bec) {
    if (bec->flag_update) {
        bec->sum_fit = bec->fit->at(0);

        for (int cont = 1; cont < bec->num_sources; cont++)
            bec->sum_fit += bec->fit->at(cont);
    }
                
}

void mBee_Col_ada::rank(BeeCol_data* bec){
    
    int tmp, i, j;
    bec->rank_pos[0]=0;
    bec->rank_pos[1]=1;
    bec->rank_pos[2]=2;
    
    for(i=1; i<3; i++)
        for(j=i; j>0 ; j--) {
            
            if(bec->nectar_Si->at(bec->rank_pos[j])>bec->nectar_Si->at(bec->rank_pos[j-1])){
                tmp=bec->rank_pos[j-1];
                bec->rank_pos[j-1]=bec->rank_pos[j];
                bec->rank_pos[j]=tmp;
            }
        }
}

void *mBee_Col_ada::evalSources(void *threadarg) {

    try {

        double fval_CBest, prop;
        uword ind_CBest = 0;
        BeeCol_data * bec = (BeeCol_data *) threadarg;
        int iter;
        
        //bec->count_mutex.lock();
        pthread_mutex_lock(&bec->count_mutex);

        //bec->condition_var2.wait(&bec->count_mutex);
        pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);

        for (iter = 0; iter < bec->final_iter; iter++) {

            fval_CBest = bec->nectar_Si->min(ind_CBest);

            if (fval_CBest < bec->fval_bpos) {
                //cout << "Nectar - "<<iter<<": " << fval_CBest << endl;
                bec->Best_Pos = bec->Si[ind_CBest];
                bec->fval_bpos = fval_CBest;
                bec->b=bec->bias_Si->at(ind_CBest);
                
            }

            if(bec->trial_cont>=bec->limit_trials){
                
                prop=(double)bec->suc_trials/bec->trial_cont;
                bec->trial_cont=0;
                bec->suc_trials=0;
                
                if(prop<0.2)
                    bec->F*=0.85;
                else
                    if(prop>0.2)
                        bec->F/=0.85;
            }
            
            bec->cont_thread = 0;
 //cout<<"6-i"<<endl;
            //bec->condition_var1.wakeAll();
            pthread_cond_broadcast(&bec->condition_var1);
            //bec->condition_var2.wait(&bec->count_mutex);
            pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);

        //cout<<"6-o"<<endl;    
            if (bec->fval_bpos <= 0.001)
                break;
        }
        

        fval_CBest = bec->nectar_Si->min(ind_CBest);

        if (fval_CBest < bec->fval_bpos) {
            //cout << "Nectar: " << fval_CBest << endl;
            bec->Best_Pos = bec->Si[ind_CBest];
            bec->fval_bpos = fval_CBest;
            bec->b=bec->bias_Si->at(ind_CBest);
        }

        //bec->nectar_Si->max(ind_GBest);

        //cout << "GBest: " << bec->fval_bpos << endl;
       
        bec->block = false;

        //bec->condition_var1.wakeAll();
        pthread_cond_broadcast(&bec->condition_var1);

        //bec->count_mutex.unlock();
        pthread_mutex_unlock(&bec->count_mutex);
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
    return threadarg;
}

void mBee_Col_ada::write_Colval(BeeCol_data *bec) {

    remove("/home/black/workspace/ff/pts.txt");


    ofstream *pts;
    pts = new ofstream("/home/black/workspace/ff/pts.txt", ios::out | ios::app);

    for (int i = 0; i < bec->num_sources; i++) {
        for (int j = 0; j < bec->xixj->num_alphas; j++)
            *pts << bec->Si[i]->at(j) << ' ';
        *pts << endl;
    }

    pts->close();
}

void mBee_Col_ada::run() {

    try {
        int rc, id, c_times = 0;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);

        if (!bec->dc->end_extraction()) {
            auto duration = 0;
            high_resolution_clock::time_point t1, t2;

            //srand(time(NULL));

            bec->dc->run();
            bec->xixj->fill(bec->dc);

            bec->cont_thread = 0;

            t1 = high_resolution_clock::now();
            cout << "Run" << endl;

            pthread_t threadEvalVecs, threadsUpdateVecs[bec->num_sources];

            bec->block = true;


            rc = pthread_create(&threadEvalVecs, NULL, evalSources, (void*) &(*bec));

            if (rc) {
                cout << "Error:unable to create thread threadEvalSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < bec->num_sources; id++) {
                rc = pthread_create(&threadsUpdateVecs[id], NULL, updateSources,
                        (void*) &(*bec));

                if (rc) {
                    cout << "Error:unable to create thread threadsUpdateSwarm,"
                            << rc << endl;
                    exit(EXIT_FAILURE);
                }
            }

            rc = pthread_join(threadEvalVecs, NULL);

            if (rc) {
                cout << "Error:unable to join threadEvalSwarm," << rc << endl;
                exit(EXIT_FAILURE);
            }

            for (id = 0; id < bec->num_sources; id++) {
                rc = pthread_join(threadsUpdateVecs[id], NULL);

                if (rc) {
                    cout << "Error:unable to join threadsUpdateSwarm," << rc
                            << endl;
                    exit(EXIT_FAILURE);
                }
            }

            c_times++;
            t2 = high_resolution_clock::now();
            //cout << "Iter: " << c_times << endl;

            duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
            cout.unsetf(std::ios::floatfield);
            cout.precision(6);
            cout << "Time: " << (float) duration / 1000000 << endl;
            time_alg += (float) duration / 1000000;
            cout << "GBest: " << bec->fval_bpos << endl;
            
            form_w();
            
            /*for(int i=0; i<bec->num_sources; i++)
                delete bec->Si[i];*/
        }

        //results.close();
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void mBee_Col_ada::run(LinKer *lk) {

    try {

        erase_lk = false;
        int rc, id, c_times = 0;
        //ofstream results;
        //results.open("./results_clasif",ios::out|ios::app);

        auto duration = 0;
        high_resolution_clock::time_point t1, t2;

        //srand(time(NULL));

        //bec->dc->run();
        //bec->xixj->fill_mod(bec->dc);
        bec->xixj = lk;

        bec->cont_thread = 0;

        t1 = high_resolution_clock::now();
        cout << "Run" << endl;

        pthread_t threadEvalVecs, threadsUpdateVecs[bec->num_sources];

        bec->block = true;


        rc = pthread_create(&threadEvalVecs, NULL, evalSources, (void*) &(*bec));

        if (rc) {
            cout << "Error:unable to create thread threadEvalSwarm," << rc
                    << endl;
            exit(EXIT_FAILURE);
        }

        for (id = 0; id < bec->num_sources; id++) {
            rc = pthread_create(&threadsUpdateVecs[id], NULL, updateSources,
                    (void*) &(*bec));

            if (rc) {
                cout << "Error:unable to create thread threadsUpdateSwarm,"
                        << rc << endl;
                exit(EXIT_FAILURE);
            }
        }

        rc = pthread_join(threadEvalVecs, NULL);

        if (rc) {
            cout << "Error:unable to join threadEvalSwarm," << rc << endl;
            exit(EXIT_FAILURE);
        }

        for (id = 0; id < bec->num_sources; id++) {
            rc = pthread_join(threadsUpdateVecs[id], NULL);

            if (rc) {
                cout << "Error:unable to join threadsUpdateSwarm," << rc
                        << endl;
                exit(EXIT_FAILURE);
            }
        }

        c_times++;
        t2 = high_resolution_clock::now();
        //cout << "Iter: " << c_times << endl;

        duration += std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        cout.unsetf(std::ios::floatfield);
        cout.precision(6);
        cout << "Time: " << (float) duration / 1000000 << endl;
        time_alg += (float) duration / 1000000;
        cout << "GBest: " << bec->fval_bpos << endl;

        form_w();

        /*for(int i=0; i<bec->num_sources; i++)
            delete bec->Si[i];*/


        //results.close();
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void mBee_Col_ada::form_w(){
    int i, dim_alph=bec->num_alphas;
    
    bec->w->zeros();

    for(i=0; i<dim_alph; i++)
        *bec->w+=bec->xixj->y_i->at(i)*bec->xixj->x_ij->row(i)*bec->Best_Pos->at(i);
}

rowvec *mBee_Col_ada::get(double &b) {
    b = bec->b;
    return bec->w;
}

mBee_Col_ada::~mBee_Col_ada() {
    delete []bec->Si;
    delete []bec->rank_pos;
    delete bec->nectar_Si;
    delete []bec->array_mutex;
    delete bec->bias_Si;
    delete bec->fit;
    delete bec->pm;
    delete bec->w;
    
    if(erase_lk)
        delete bec->xixj;
     
    delete bec;
}

