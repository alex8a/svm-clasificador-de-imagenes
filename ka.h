/*
 * PSO.h
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#ifndef KA_H_
#define KA_H_

#include <pthread.h>
#include <cstdlib>
#include <chrono>
#include <dirent.h>

#include "LinKer.h"
#include "Evolalg.h"
#include "PSOdata_ada.h"
#include "Datacontainer.h"
#include "Adatron.h"
#include "SVM.h"
#include "Foldextractor_norm.h"

class k_ada{
public:
	k_ada();
	virtual ~k_ada();
	void run();
        

private:
    void form_w();
    void get_file_names(string dir_path, vector<string> &dirlist_data, vector<string> &dirlist_index);

};

#endif /* KA_H_ */
