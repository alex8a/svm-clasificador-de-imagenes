/* 
 * File:   evdata.h
 * Author: black
 *
 * Created on October 22, 2015, 8:15 PM
 */

#ifndef EVDATA_H
#define	EVDATA_H

typedef struct {
    pthread_mutex_t count_mutex;
    pthread_cond_t condition_var1;
    vector<string> dirlist_data, dirlist_index;
    string path, name_alg;
    int num_folds, dim, cont_thread, fold, num_ind, final_iter, num_alphas;
    rowvec *w;
    double b, acum_gen,  F, C, limit_trials, FCR, crsv, c1, c2, w_start, w_end, Vmax;
    Data_container  **clfdata;
    LinKer **lk;
}ev_data;


#endif	/* EVDATA_H */

