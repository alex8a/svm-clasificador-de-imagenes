/* 
 * File:   LinKer.cpp
 * Author: black
 * 
 * Created on June 5, 2015, 6:26 PM
 */

#include "LinKer.h"

LinKer::LinKer(int num_alphas, int dim, bool randnum) {
    this->num_alphas = num_alphas;
    xixj = new mat(num_alphas, num_alphas);
    xixj->zeros();
    x_ij = new mat(num_alphas, dim);
    y_i = new rowvec(num_alphas);
    this->randnum = randnum;
}

void *LinKer::t_run(void *threadarg) {

    try {
        LK_data * lk = (LK_data *) threadarg;
        int num_thread, up_b, lo_b, h, i, j;
        double x_ih, x_jh;
        vec acum(lk->num_alphas);
        
        pthread_mutex_lock(&lk->count_mutex);
        //lk->count_mutex.lock();

        num_thread=lk->cont_thread;
        lk->cont_thread++;
        
        lo_b=ceil(num_thread*lk->inc);
        up_b=ceil((num_thread+1)*lk->inc);
        //cout<<num_thread<<" - lo_b: "<<lo_b<<" - "<<"up_b: "<<up_b<<endl;
        pthread_mutex_unlock(&lk->count_mutex);
        //lk->count_mutex.unlock();


        
        if(up_b>lk->dim)
            up_b=lk->dim;

        /*for (h = lo_b; h < up_b; h++) 
            for (i = 0; i < lk->num_alphas; i++) {
                x_ih=lk->X->at(i,h);
                
                if(x_ih!=0) {
                    acum = 0;
                    for (j = i; j < lk->num_alphas; j++)
                        acum += x_ih * lk->X->at(j, h);

                    pthread_mutex_lock(&lk->count_mutex);
                        lk->xixj->at(i,j)+=acum;
                        
                    pthread_mutex_unlock(&lk->count_mutex);
                //xixj->at(i,j)= xixj->at(j,i)=dot(X->row(pos_elems[i]),X->row(pos_elems[j]));
                }
            }*/

        /*cout<<"Comienza T_run"<<endl;
        cout<<"lo_b; "<<lo_b<<" up_b: "<<up_b<<" num_thread: "<<num_thread<<" lk->inc: "<<lk->inc<<" lk->num_alphas: "<<lk->num_alphas<<endl;

        cout<<"XRows: "<<lk->X-><<" XCols: "<<lk->X->cols<<endl;
        cin.get();*/
        for (i = 0; i < lk->num_alphas; i++){
            acum.zeros();
            for (h = lo_b; h < up_b; h++) {
                x_ih = lk->X->at(i, h);

                if (x_ih != 0) {
                    for (j = i; j < lk->num_alphas; j++){
                        x_jh=lk->X->at(j, h);
                        
                        if(x_jh != 0)
                            acum.at(j) += x_ih * x_jh;
                    }
                }
            }

            pthread_mutex_lock(&lk->count_mutex);
            //lk->count_mutex.lock();
            for (j = i; j < lk->num_alphas; j++){
                lk->xixj->at(i, j) += acum.at(j);
                //cout<<"productopunto"<<lk->xixj->at(i, j) <<endl;
            }

            //lk->xixj->print();


            pthread_mutex_unlock(&lk->count_mutex);
            //lk->count_mutex.unlock();
            /*cout<<"Linket t_run"<<endl;
            cin.get();*/
        }
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
    return threadarg;
}

void LinKer::fill(Data_container *dc) {
    mat *X = dc->extract_xij();
    vec *Y = dc->extract_yi();
    int pos_elems[num_alphas], i, j, num_threads = 20, rc, id;
    LK_data lk;
    
    lk.count_mutex=PTHREAD_MUTEX_INITIALIZER;
    lk.dim=dc->dim;
    lk.dc=dc;
    lk.xixj=xixj;
    lk.cont_thread=0;
    lk.num_alphas=num_alphas;
    lk.X=X;
    
    pthread_t threads[num_threads];
    cout<<"Filling matrix"<<endl;
    

    if (randnum) {
        srand(time(NULL));
        for (i = 0; i < num_alphas; i++)
            pos_elems[i] = round((dc->num_data - 1) * ((float) rand() / RAND_MAX));
    } else {
        for (i = 0; i < num_alphas; i++)
            pos_elems[i] = i;

        pos_data = num_alphas;
    }

    lk.pos_elems=pos_elems;
    /*for(i=0; i<num_alphas; i++){
        x_ij->row(i)=X->row(pos_elems[i]);
        //x_ij->row(i).print("x");

        y_i->at(i)=Y->at(pos_elems[i]);
        for(j=i; j<num_alphas; j++)
            xixj->at(i,j)= xixj->at(j,i)=dot(X->row(pos_elems[i]),X->row(pos_elems[j]));
    }

    ofstream  ofs ("test-a.txt", std::ofstream::out);
    xixj->print(ofs);//*/
    
    lk.inc=(double)lk.dim/num_threads;
    for (id = 0; id < num_threads; id++) {
        rc = pthread_create(&threads[id], NULL, t_run, (void*) &lk);

        if (rc) {
            cout << "Error:unable to create thread threadsUpdateSwarm,"
                    << rc << endl;
            exit(EXIT_FAILURE);
        }
    }


    for (id = 0; id < num_threads; id++) {
        rc = pthread_join(threads[id], NULL);

        if (rc) {
            cout << "Error:unable to join threadsUpdateSwarm," << rc << endl;
            exit(EXIT_FAILURE);
        }
    }
    for(i=0; i<num_alphas; i++){
        x_ij->row(i)=X->row(pos_elems[i]);
        y_i->at(i)=Y->at(pos_elems[i]);
        for(j=i; j<num_alphas; j++){
            xixj->at(j,i)= xixj->at(i,j);
        }


    }
    
    //y_i->print("yi");
    /*ofstream  ofs ("test-b.txt", std::ofstream::out);
    xixj->print(ofs);//*/
}

void LinKer::fill_mod(Data_container *dc) {
    mat *X = dc->extract_xij();
    vec *Y = dc->extract_yi();
    int i;
    
    *x_ij=*X;
    *y_i=Y->t();

    for(i=0; i<num_alphas; i++){
        xixj->at(i,i)= dot(x_ij->row(i),x_ij->row(i));
        cout<<"xixj"<<xixj->at(i,i)<<endl;
    }

    
    
}

double LinKer::replace(Data_container *dc, rowvec alpha, rowvec *pos_replace) {
    int pos_dc, pos_rplz = 0;
    for (int i = 0; i < alpha.n_elem && pos_data < dc->num_data; i++) {
        if (alpha.at(i) == 0) {
            pos_replace->at(pos_rplz) = i;
            pos_rplz++;

            if (randnum) {
                pos_dc = round((dc->num_data - 1) * ((float) rand() / RAND_MAX));
            } else {
                pos_dc = pos_data;
                pos_data++;
            }

            x_ij->row(i) = dc->extract_xij()->row(pos_dc);
            y_i->at(i) = dc->extract_yi()->at(pos_dc);

            for (int j = i; j < num_alphas; j++) {
                xixj->at(i, j) = xixj->at(j, i) = dot(x_ij->row(j), x_ij->row(i));
                cout<<"calculo xixj"<<endl;
            }
            cin.get();
        }
    }

    pos_replace->at(pos_rplz) = -1;
    return 0;
}

LinKer::~LinKer() {
    delete xixj;
    delete x_ij;
    delete y_i;
}

