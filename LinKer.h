/* 
 * File:   LinKer.h
 * Author: black
 *
 * Created on June 5, 2015, 6:26 PM
 */

#ifndef LINKER_H
#define	LINKER_H

#include <armadillo>
#include <typeinfo>

#include "Datacontainer.h"
#include "LKdata.h"
#include <QObject>
using namespace arma;

class LinKer {
public:
    LinKer(int num_alphas, int dim, bool randnum=true);
    void fill(Data_container *dc);
    void fill_mod(Data_container *dc);
    double replace(Data_container *dc, rowvec alpha, rowvec *pos);
    virtual ~LinKer();
    
    mat *x_ij, *xixj;
    rowvec *y_i;
    int num_alphas;
    
private:
    bool randnum;
    double pos_data=0;
    static void *t_run(void *threadarg);
};

#endif	/* LINKER_H */

