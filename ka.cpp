/*
 * PSO.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#include "ka.h"
#include <typeinfo>
extern double time_alg;

k_ada::k_ada() {

    

}

void k_ada::run() {
int dim= 804;
    string path = string("/home/black/workspace/new Data/worm - OneDrive/");
    
    LinKer *lk;

    high_resolution_clock::time_point t1, t2;
    auto duration = 0;
    
    string indexfile = string("saved_indexes00.bin"), datafile = string("saved_data00.bin");
    
    int num_data = 400,  num_alphas = 400; 
    
    string dest_file, destination = "./roc";
    //Roc *rc;

    int num_folds = 10;
    double bias=0, acum_gen = 0, acum_trn = 0, genres, trres, C=100;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data, dirlist_index;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);
    dirlist_index.resize(num_folds);

    get_file_names(path, dirlist_data, dirlist_index);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w, *alpha;
        
        cout << "Folds KA" << endl;


        edata = new Foldextractor_norm(dim, num_data, num_folds, path,
                dirlist_data, cont_folds);
        num_alphas = edata->num_data;
        edata->run();
                
        lk=new LinKer(num_alphas, edata->dim, false);
        
        lk->fill(edata);

        alpha=new rowvec(num_alphas);
        w=new rowvec(dim);
        w->zeros();
        arma_rng::set_seed(time(NULL));
        
        alpha->ones();
        
        double yi, da, n=0.00000001, z_i, margin=INF;

        t1 = high_resolution_clock::now();
        
        for (int iter = 0; iter < 10 && margin > 1; iter++) {
            double zi_pos_min=INF, zi_neg_max=-INF;
            cout<<"Iter: "<<iter<<endl;
            z_i = 0;
            for (int i = 0; i < num_alphas; i++) {
                for (int j = 0; j < num_alphas; j++) {
                    z_i += alpha->at(j) * lk->y_i->at(j) * lk->xixj->at(i, j);
                }

                yi = lk->y_i->at(i)*(z_i-bias);
                da = n * (1 - yi);
                if (alpha->at(i) + da <= 0)
                    alpha->at(i) = 0;
                else
                    alpha->at(i) = alpha->at(i) + da;

                if (lk->y_i->at(i) == 1 && zi_pos_min > z_i) {
                    zi_pos_min = z_i;
                } else
                    if (lk->y_i->at(i) == -1  && zi_neg_max < z_i) {
                    zi_neg_max = z_i;
                }
            }

            //bias=(zi_pos_min+zi_neg_max)/2;
            //alpha->print("Alpha ");
            margin=abs((zi_pos_min-zi_neg_max)/2);
            cout<<"zi_neg_max "<<zi_neg_max<<endl;
            cout<<"zi_pos_min "<<zi_pos_min<<endl;
            cout<<"Margin: "<<margin<<endl;
        }

        t2 = high_resolution_clock::now();
        duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
        cout.unsetf(std::ios::floatfield);
        cout.precision(6);
        cout << "Time: " << (float) duration / 1000000 << endl;
        time_alg += (float) duration / 1000000;
            
        dest_file = destination + to_string(cont_folds) + ".txt";

        //alpha->print("alpha");
        for (int i = 0; i < num_alphas; i++)
        *w += lk->y_i->at(i) * lk->x_ij->row(i) * alpha->at(i);
        //w->print("w");
        ResultSVM *result=svm.classify(w, bias, edata, false);
        trres = result->percentGood;
        acum_trn += trres;
        wr_class << trres << endl;

        clfdata = new Lineextractor_norm(dim, path, dirlist_data.at(cont_folds));
        result = svm.classify(w, bias, clfdata);
        genres = result->percentGood;
        write_results << genres << endl;
        acum_gen += genres;



        delete edata;
        delete clfdata;
        delete w;
        delete alpha;
        delete lk;
    }

    wr_class << endl;
    write_results << endl;
    wr_class << acum_trn / num_folds << endl;
    write_results << acum_gen / num_folds << endl;

    wr_class << "Time: " << time_alg / num_folds << endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
    
}


void k_ada::form_w() {
    /*int i, dim_alph = swarmdt->num_alphas;

    swarmdt->w_vec->zeros();

    for (i = 0; i < dim_alph; i++)
        *swarmdt->w_vec += swarmdt->xixj->y_i->at(i) * swarmdt->xixj->x_ij->row(i) * swarmdt->P_best[swarmdt->ind_GBest]->at(i);*/
}

void k_ada::get_file_names(string dir_path, vector<string> &dirlist_data, vector<string> &dirlist_index){
    
    DIR *d;
    struct dirent *dir;
    string d_name, subs;

    
    d = opendir(dir_path.c_str());


    if (d) {
        while ((dir = readdir(d)) != NULL) {
            d_name=dir->d_name;
            if(d_name.compare(0,13,"saved_indexes")==0){
                subs=d_name.substr(13,2);
                dirlist_index.at(atoi(subs.c_str()))=dir->d_name;
            }else
            if(d_name.compare(0,10,"saved_data")==0){
                subs=d_name.substr(10,2);
                dirlist_data.at(atoi(subs.c_str()))=dir->d_name;
            }
        }
        
    }
        closedir(d);
}

k_ada::~k_ada() {

}

