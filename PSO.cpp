/*
 * PSO.cpp
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */

#include "PSO.h"
#include <typeinfo>

PSO::PSO(int dim_dset, float c1, float c2, float Vmax, float w_start, float w_end,
        int swarm_size, int final_iter, vec *boundary, string name, 
        string path, string datafile) {

    try {

        swarm = new swarm_data;
        swarm->name=name;
        swarm->path=path;
        swarm->datafile=datafile;
        cout<<path<<endl;
        
        if(strcmp(name.c_str(),"PSO")!=0)
            swarm->dim=5;
        else
            swarm->dim=8;
        
        swarm->dim_dset=dim_dset;
        data = new rowvec(swarm->dim);

        swarm->boundary=boundary;
        swarm->cont_thread = 0;
        swarm->final_iter = final_iter;
        swarm->swarm_size = swarm_size;
        swarm->Vmax = Vmax;
        swarm->c1 = c1;
        swarm->c2 = c2;
        swarm->w_start = w_start;
        swarm->w_end = w_end;
        swarm->fval_Swarm = new vec(swarm_size);
        swarm->fval_PBest = new vec(swarm_size);
        swarm->fval_PBest->fill(100000000);
        swarm->V = new mat(swarm_size, swarm->dim);
        swarm->V->zeros();

        swarm->P_best = new mat(swarm_size, swarm->dim);
        swarm->Swarm_pos = new mat(swarm_size, swarm->dim);

        swarm->ind_GBest = 0;



    } catch (std::bad_alloc& ba) {
        std::cerr << "bad_alloc caught in PSO constructor: " << ba.what() << '\n';
        exit(EXIT_FAILURE);
    }

}

void PSO::run() {

    try {
        double ran_num, perc;
        int numpart, i, iter; 
        uword ind_BestP;
        float r1, r2, dec;
        ff of(swarm->name, 400, swarm->path, swarm->dim_dset);
        
        auto duration = 0;
        high_resolution_clock::time_point t1, t2;

        //t1 = high_resolution_clock::now();
        t1 = high_resolution_clock::now();
        cout << "Run" << endl;
        dec = (swarm->w_start - swarm->w_end) / swarm->final_iter;


        srand(time(NULL));

        swarm->boundary->print("p");
        for (int i = 0; i < swarm->swarm_size; i++) {
            for (int j = 0; j < swarm->dim; j++) {
                ran_num = swarm->boundary->at(j) * ((double) rand() / RAND_MAX);
                
                if(j==0&&ran_num<=3)
                    ran_num=4;
                
                swarm->P_best->at(i, j) = ran_num;
            }
            
        }

        swarm->w = swarm->w_start;
        
        for(numpart=0; numpart<swarm->swarm_size; numpart++){
            swarm->fval_PBest->at(numpart) = of.evaluate(swarm->P_best->row(numpart));
            cout<<"--------------->Part: "<<numpart<<endl;
        }

        for (iter = 0; iter < swarm->final_iter; iter++) {
            cout<<"--------------->Iter: "<<iter<<endl;
            for (numpart = 0; numpart < swarm->swarm_size; numpart++) {
                r1 = ((float) rand() / RAND_MAX);
                r2 = ((float) rand() / RAND_MAX);

                swarm->V->row(numpart) = swarm->V->row(numpart) * swarm->w
                        + swarm->c1 * r1 * (swarm->P_best->row(numpart) - swarm->Swarm_pos->row(numpart))
                        + swarm->c2 * r2 * (swarm->P_best->row(swarm->ind_GBest) - swarm->Swarm_pos->row(numpart));

                for (i = 0; i < swarm->dim; i++)
                    if (swarm->V->at(numpart, i) > swarm->Vmax)
                        swarm->V->at(numpart, i) = swarm->Vmax;
                    else if (swarm->V->at(numpart, i) < -swarm->Vmax)
                        swarm->V->at(numpart, i) = -swarm->Vmax;

                swarm->Swarm_pos->row(numpart) = swarm->Swarm_pos->row(numpart) + swarm->V->row(numpart);

                if (swarm->Swarm_pos->at(numpart, 0) <= 3)
                        swarm->Swarm_pos->at(numpart, 0) = 4;
                else
                    if (swarm->Swarm_pos->at(numpart, 0) >30)
                        swarm->Swarm_pos->at(numpart, 0) = 30;
                
                if (swarm->Swarm_pos->at(numpart, 1) < 10)
                        swarm->Swarm_pos->at(numpart, 1) = 10;

                if (swarm->Swarm_pos->at(numpart, 2) < 0)
                        swarm->Swarm_pos->at(numpart, 2) = 0.0001;
                
                if(strcmp(swarm->name.c_str(),"DE")==0){
                    if (swarm->Swarm_pos->at(numpart, 3) <= 0)
                        swarm->Swarm_pos->at(numpart, 3) = 0.001;
                    else
                        if (swarm->Swarm_pos->at(numpart, 3) >= 1)
                        swarm->Swarm_pos->at(numpart, 3) = 0.999;

                    if (swarm->Swarm_pos->at(numpart, 4) <= 0)
                        swarm->Swarm_pos->at(numpart, 4) = 0.0001;
                }else {
                    if (swarm->Swarm_pos->at(numpart, 3) <= 0)
                        swarm->Swarm_pos->at(numpart, 3) = 0.001;

                    if (swarm->Swarm_pos->at(numpart, 4) < 5)
                        swarm->Swarm_pos->at(numpart, 4) = 5;
                }
                
                swarm->fval_Swarm->at(numpart) = of.evaluate(swarm->Swarm_pos->row(numpart));

                if ((swarm->fval_Swarm->at(numpart) > swarm->fval_PBest->at(numpart))||(swarm->fval_Swarm->at(numpart) == swarm->fval_PBest->at(numpart)&&swarm->Swarm_pos->at(numpart, 0)<swarm->P_best->at(numpart,0))) {
                    swarm->fval_PBest->at(numpart) = swarm->fval_Swarm->at(numpart);
                    swarm->P_best->row(numpart) = swarm->Swarm_pos->row(numpart);
                }
                
                cout<<"--------------->Part: "<<numpart<<endl;
            }
            
            swarm->w = swarm->w_start - dec * iter;
            swarm->fval_PBest->max(ind_BestP);


            if (swarm->fval_PBest->at(swarm->ind_GBest) < swarm->fval_PBest->at(ind_BestP)) {

                swarm->ind_GBest=ind_BestP;
            }
            
        }

       

        t2 = high_resolution_clock::now();
        duration += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        cout << "Time: " << (float) duration / 1000 << endl;

        perc=swarm->fval_PBest->at(swarm->ind_GBest);
        *data = swarm->P_best->row(swarm->ind_GBest);

        cout << perc << endl;
        std::ofstream res;
        
        if(strcmp(swarm->name.c_str(),"DE")==0)
            res.open ("result_PSO-DE.txt", std::ofstream::out);
        else
            res.open("result_PSO-ABC.txt", std::ofstream::out);
        
        res << data->at(swarm->ind_GBest, 0)<<endl;
        res << data->at(swarm->ind_GBest, 1)<<endl;
        res << data->at(swarm->ind_GBest, 2)<<endl;
        res << data->at(swarm->ind_GBest, 3)<<endl;
        res << data->at(swarm->ind_GBest, 4)<<endl;
        res <<"Perc: "<<perc<<endl;
        res.close();

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void PSO::run_pso() {

    try {
        double ran_num, perc, tmp;
        int numpart, i, iter; 
        uword ind_BestP;
        float r1, r2, dec;
        ff of(swarm->name, 400, swarm->path, swarm->dim_dset);
        
        auto duration = 0;
        high_resolution_clock::time_point t1, t2;

        //t1 = high_resolution_clock::now();
        t1 = high_resolution_clock::now();
        cout << "Run" << endl;
        dec = (swarm->w_start - swarm->w_end) / swarm->final_iter;


        srand(time(NULL));

        swarm->boundary->print("p");
        for (unsigned int i = 0; i < swarm->swarm_size; i++) {
            for (unsigned int j = 0; j < swarm->dim; j++) {
                ran_num = swarm->boundary->at(j) * ((float) rand() / RAND_MAX);
                
                if(j==0&&ran_num<=3)
                    ran_num=4;
                
                swarm->P_best->at(i, j) = ran_num;
            }
            
        }

        swarm->w = swarm->w_start;
        
        for(numpart=0; numpart<swarm->swarm_size; numpart++){
            swarm->fval_PBest->at(numpart) = of.evaluate(swarm->P_best->row(numpart));
            cout<<"--------------->Part: "<<numpart<<endl;
        }

        //swarm->P_best->print();
        
        for (iter = 0; iter < swarm->final_iter; iter++) {
            cout<<"--------------->Iter: "<<iter<<endl;
            for (numpart = 0; numpart < swarm->swarm_size; numpart++) {
                r1 = ((float) rand() / RAND_MAX);
                r2 = ((float) rand() / RAND_MAX);

                swarm->V->row(numpart) = swarm->V->row(numpart) * swarm->w
                        + swarm->c1 * r1 * (swarm->P_best->row(numpart) - swarm->Swarm_pos->row(numpart))
                        + swarm->c2 * r2 * (swarm->P_best->row(swarm->ind_GBest) - swarm->Swarm_pos->row(numpart));

                for (i = 0; i < swarm->dim; i++)
                    if (swarm->V->at(numpart, i) > swarm->Vmax)
                        swarm->V->at(numpart, i) = swarm->Vmax;
                    else if (swarm->V->at(numpart, i) < -swarm->Vmax)
                        swarm->V->at(numpart, i) = -swarm->Vmax;

                swarm->Swarm_pos->row(numpart) = swarm->Swarm_pos->row(numpart) + swarm->V->row(numpart);
                //swarm->Swarm_pos->print();
                
                if (swarm->Swarm_pos->at(numpart, 0) <= 3)
                        swarm->Swarm_pos->at(numpart, 0) = 4;
                else
                    if (swarm->Swarm_pos->at(numpart, 0) >30)
                        swarm->Swarm_pos->at(numpart, 0) = 30;

                if (swarm->Swarm_pos->at(numpart, 1) < 10)
                    swarm->Swarm_pos->at(numpart, 1) = 10;

                if (swarm->Swarm_pos->at(numpart, 2) < 0)
                    swarm->Swarm_pos->at(numpart, 2) = 0.0001;

                if (swarm->Swarm_pos->at(numpart, 3) <= 0)
                    swarm->Swarm_pos->at(numpart, 3) = 0.001;

                if (swarm->Swarm_pos->at(numpart, 4) <= 0)
                    swarm->Swarm_pos->at(numpart, 4) = 0.0001;

                if(swarm->Swarm_pos->at(numpart, 5)<swarm->Swarm_pos->at(numpart, 6)){
                    tmp=swarm->Swarm_pos->at(numpart, 6);
                    swarm->Swarm_pos->at(numpart, 6)=swarm->Swarm_pos->at(numpart, 5);
                    swarm->Swarm_pos->at(numpart, 5)=tmp;
                }
                
                if (swarm->Swarm_pos->at(numpart, 5) <= 0)
                    swarm->Swarm_pos->at(numpart, 5) = 0.01;

                if (swarm->Swarm_pos->at(numpart, 6) <= 0)
                    swarm->Swarm_pos->at(numpart, 6) = 0.0001;

                if (swarm->Swarm_pos->at(numpart, 7) <= 0)
                    swarm->Swarm_pos->at(numpart, 7) = 0.1;
                
                //swarm->Swarm_pos->print();
                
                swarm->fval_Swarm->at(numpart) = of.evaluate(swarm->Swarm_pos->row(numpart));

                if ((swarm->fval_Swarm->at(numpart) > swarm->fval_PBest->at(numpart))||(swarm->fval_Swarm->at(numpart) == swarm->fval_PBest->at(numpart)&&swarm->Swarm_pos->at(numpart, 0)<swarm->P_best->at(numpart,0))) {
                    swarm->fval_PBest->at(numpart) = swarm->fval_Swarm->at(numpart);
                    swarm->P_best->row(numpart) = swarm->Swarm_pos->row(numpart);
                }
                
                cout<<"--------------->Part: "<<numpart<<endl;
            }
            
            swarm->w = swarm->w_start - dec * iter;
            swarm->fval_PBest->max(ind_BestP);


            if (swarm->fval_PBest->at(swarm->ind_GBest) < swarm->fval_PBest->at(ind_BestP)) {

                swarm->ind_GBest=ind_BestP;
            }
            
        }

       

        t2 = high_resolution_clock::now();
        duration += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        cout << "Time: " << (float) duration / 1000 << endl;

        perc=swarm->fval_PBest->at(swarm->ind_GBest);
        *data = swarm->P_best->row(swarm->ind_GBest);

        cout << perc << endl;
        std::ofstream res;
        

        res.open("result_PSO-PSO.txt", std::ofstream::out);
        
        res << data->at(swarm->ind_GBest, 0)<<endl;
        res << data->at(swarm->ind_GBest, 1)<<endl;
        res << data->at(swarm->ind_GBest, 2)<<endl;
        res << data->at(swarm->ind_GBest, 3)<<endl;
        res << data->at(swarm->ind_GBest, 4)<<endl;
        res << data->at(swarm->ind_GBest, 5)<<endl;
        res << data->at(swarm->ind_GBest, 6)<<endl;
        res << data->at(swarm->ind_GBest, 7)<<endl;
        res <<"Perc: "<<perc<<endl;
        
        res.close();

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}

void PSO::run_mabc() {

    try {
        double ran_num, perc;
        int numpart, i, iter; 
        uword ind_BestP;
        float r1, r2, dec;
        ff of(swarm->name, 400, swarm->path, swarm->dim_dset);
        
        auto duration = 0;
        high_resolution_clock::time_point t1, t2;

        //t1 = high_resolution_clock::now();
        t1 = high_resolution_clock::now();
        cout << "Run" << endl;
        dec = (swarm->w_start - swarm->w_end) / swarm->final_iter;


        srand(time(NULL));

        swarm->boundary->print("p");
        for (unsigned int i = 0; i < swarm->swarm_size; i++) {
            for (unsigned int j = 0; j < swarm->dim; j++) {
                ran_num = swarm->boundary->at(j) * ((float) rand() / RAND_MAX);
                
                swarm->P_best->at(i, j) = ran_num;
            }
            
        }

        swarm->w = swarm->w_start;
        
        for(numpart=0; numpart<swarm->swarm_size; numpart++){
            swarm->fval_PBest->at(numpart) = of.evaluate(swarm->P_best->row(numpart));
            cout<<"--------------->Part: "<<numpart<<endl;
        }
        
        for (iter = 0; iter < swarm->final_iter; iter++) {
            cout<<"--------------->Iter: "<<iter<<endl;
            for (numpart = 0; numpart < swarm->swarm_size; numpart++) {
                r1 = ((float) rand() / RAND_MAX);
                r2 = ((float) rand() / RAND_MAX);

                swarm->V->row(numpart) = swarm->V->row(numpart) * swarm->w
                        + swarm->c1 * r1 * (swarm->P_best->row(numpart) - swarm->Swarm_pos->row(numpart))
                        + swarm->c2 * r2 * (swarm->P_best->row(swarm->ind_GBest) - swarm->Swarm_pos->row(numpart));

                for (i = 0; i < swarm->dim; i++)
                    if (swarm->V->at(numpart, i) > swarm->Vmax)
                        swarm->V->at(numpart, i) = swarm->Vmax;
                    else if (swarm->V->at(numpart, i) < -swarm->Vmax)
                        swarm->V->at(numpart, i) = -swarm->Vmax;

                swarm->Swarm_pos->row(numpart) = swarm->Swarm_pos->row(numpart) + swarm->V->row(numpart);


                if (swarm->Swarm_pos->at(numpart, 0) < 10)
                    swarm->Swarm_pos->at(numpart, 0) = 10;

                if (swarm->Swarm_pos->at(numpart, 1) < 0)
                    swarm->Swarm_pos->at(numpart, 1) = 0.0001;

                if (swarm->Swarm_pos->at(numpart, 2) < 0)
                    swarm->Swarm_pos->at(numpart, 2) = 0.0001;

                if (swarm->Swarm_pos->at(numpart, 3) <= 5)
                    swarm->Swarm_pos->at(numpart, 3) = 5;

                if (swarm->Swarm_pos->at(numpart, 4) < 0)
                    swarm->Swarm_pos->at(numpart, 4) = 0.0001;
                else
                    if (swarm->Swarm_pos->at(numpart, 4) >= 1)
                    swarm->Swarm_pos->at(numpart, 4) = 0.9999;
                
                swarm->fval_Swarm->at(numpart) = of.evaluate(swarm->Swarm_pos->row(numpart));

                if (swarm->fval_Swarm->at(numpart) > swarm->fval_PBest->at(numpart)) {
                    swarm->fval_PBest->at(numpart) = swarm->fval_Swarm->at(numpart);
                    swarm->P_best->row(numpart) = swarm->Swarm_pos->row(numpart);
                }
                cout<<"--------------->Part: "<<numpart<<endl;
            }
            
            swarm->w = swarm->w_start - dec * iter;
            swarm->fval_PBest->max(ind_BestP);


            if (swarm->fval_PBest->at(swarm->ind_GBest) < swarm->fval_PBest->at(ind_BestP)) {

                swarm->ind_GBest=ind_BestP;
            }
        }

       

        t2 = high_resolution_clock::now();
        duration += std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
        cout << "Time: " << (float) duration / 1000 << endl;

        perc=swarm->fval_PBest->at(swarm->ind_GBest);
        *data = swarm->P_best->row(swarm->ind_GBest);

        cout << perc << endl;
        
        std::ofstream res ("result_PSO-mABC.txt", std::ofstream::out);

        res << data->at(swarm->ind_GBest, 0)<<endl;
        res << data->at(swarm->ind_GBest, 1)<<endl;
        res << data->at(swarm->ind_GBest, 2)<<endl;
        res << data->at(swarm->ind_GBest, 3)<<endl;
        res << data->at(swarm->ind_GBest, 4)<<endl;
        res <<"Perc: "<<perc<<endl;
        
        res.close();

        cout << perc << endl;

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
}
rowvec *PSO::get() {

    return data;
}

PSO::~PSO() {

    delete data;
    delete swarm;
}

