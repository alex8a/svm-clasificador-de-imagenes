/* 
 * File:   DE.h
 * Author: black
 *
 * Created on February 20, 2015, 7:29 PM
 */

#ifndef DE_ADA_H
#define	DE_ADA_H

#include <chrono>
#include <typeinfo>
#include <armadillo>

#include "LinKer.h"
#include "Evolalg.h"
#include "Datacontainer.h"
#include "DE_data.h"
#include "Adatron.h"

using namespace arma;

class DE_ada : public Evol_alg {
public:
    DE_ada(double F, double crsv, int num_vecs, int final_iter, Data_container *dc,
            double C, int num_alphas);
    void run();
    void run(LinKer *lk);
    rowvec *get(double &b);
    virtual ~DE_ada();
private:
    static void *updateVecs(void *threadarg);
    static void *evalVecs(void *threadarg);
    static void write_partval(DE_data *ded);
    
    void form_w();
    
    DE_data *ded;

};

#endif	/* DE_ADA_H */

