#ifndef ENTRENADOR_H
#define ENTRENADOR_H
#include "resultsvm.h"
#include "ui_mainwindow.h"
#include "SVM.h"
#include <QObject>

class Entrenador : public QObject
{
    Q_OBJECT
public:
    Entrenador();
    ResultSVM* bee_col_ada(int dim, string path, string datafile);
    ResultSVM* mbee_col_ada(int dim, string path, string datafile);
    ResultSVM* de_ada(int dim, string path, string datafile);
    ResultSVM* pso_ada(int dim, string path, string datafile);
    int countZerosInW();
    int dim;
    double b;
    rowvec *w;
    Data_container *edata, *clfdata;
    SVM svm;
    ResultSVM *result;
public slots:
    void entrenar(Ui::MainWindow *ui);
signals:
    void progress(int);
};

#endif // ENTRENADOR_H
