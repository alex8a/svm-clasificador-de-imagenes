/* 
 * File:   data_container.h
 * Author: black
 *
 * Created on August 19, 2014, 6:22 PM
 */

#ifndef DATA_CONTAINER_H_
#define	DATA_CONTAINER_H_

#include <armadillo>
#include <string>
#include <fstream>
#include <stdint.h>

using namespace arma;
using namespace std;

typedef uint8_t byte;

class Data_container {

public:
    Data_container(int dim, unsigned int num_data, string path, string datafile);
    virtual ~Data_container();
    virtual mat *extract_xij()=0;
    virtual vec *extract_yi()=0;
    virtual bool end_extraction()=0;
    virtual void run()=0;

    int num_data, dim;

protected:

    rowvec get_line(int &y);
    rowvec get_lineAlex(int &y);
    rowvec get_line(unsigned int numline, int &y);

    mat *x_ij=NULL;
    vec *y_i=NULL;
    unsigned long num_total_data;
    ifstream streamdata, streamindex;
    bool eofflag;

private:
    int size_ind_line;
    unsigned long cont_line;

};

#endif	/* DATA_CONTAINER_H */

