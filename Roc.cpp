/* 
 * File:   Roc.cpp
 * Author: black
 * 
 * Created on March 24, 2015, 7:59 PM
 */

#include "Roc.h"

Roc::Roc(int dim, string path, string datafile, rowvec w, double b, int div_range) {
    this->w=w;
    /*ofstream save_w("./w.txt", ios::out);
    
    save_w<<to_string(b)<<" ";
    save_w.flush();
    for(int i=0; i<w.size(); i++)
        if(w.at(i)!=0){
            save_w<<to_string(i+1)<<":"<<to_string(w.at(i))<<" ";
            save_w.flush();
        }
    
    save_w.close();*/
    
    this->b=b;
    this->le=new Lineextractor(dim, path, datafile);
    this->letmp=new Lineextractor(dim, path, datafile);
    this->div_range=div_range;
    pos_ext=log(0);
    neg_ext=-log(0);
    
    c1=new rowvec(div_range);
    c2=new rowvec(div_range);
    acum_c1=new rowvec(div_range);
    acum_c2=new rowvec(div_range);
    c1->zeros();
    c2->zeros();
}

void Roc::get_extremes() {
    
    unsigned long contlines=0;
    rowvec x_i(letmp->dim);
    double wx;
    
    while (!letmp->end_extraction()) {

        contlines++;
        letmp->run();

        x_i = letmp->extract_xij()->row(0);

        wx = dot(w, x_i)+b;

        if (contlines % 10000 == 0)
            cout << "Line " << contlines << endl;

        if (wx>pos_ext) {
            pos_ext=wx;
        }
        
        if(wx<neg_ext) {
            neg_ext=wx;
        }
    }
}
void Roc::obtain_vals() {
    
    long contlines=0, pos/*, positive=0, negative=0*/;
    rowvec x_i(letmp->dim);
    int error=0;
    double y_i, wx, inc, diff;
    
    inc=(double)(pos_ext-neg_ext)/div_range;
    
    while (!le->end_extraction()) {
        pos=1;
        contlines++;
        le->run();

        x_i = le->extract_xij()->row(0);
        y_i = le->extract_yi()->at(0);

        wx = dot(w, x_i)+b;
        
        if((wx>=0&&y_i==-1)||(wx<=0&&y_i==1))
            error++;
        
        if (contlines % 10000 == 0)
            cout << "Line " << contlines << endl;

        diff = wx - neg_ext;
        while (diff >= (pos * inc)&&pos<=div_range) {

            if (y_i == -1) {
                //negative++;
                c1->at(pos-1)++;

            } else {
                //positive++;
                c2->at(pos-1)++;
            }
            pos++;
        }

    }
    

    *c1 = (*c1)/c1->at(0);
    *c2 = (*c2)/c2->at(0);
    cout<<"Error: "<<error<<endl;
    /*for (pos=div_range-1; pos>=0; pos--){
        if(pos==div_range-1){
            acum_c1->at(pos)=c1->at(pos);
            acum_c2->at(pos)=c2->at(pos);
        }else{
            acum_c1->at(pos)=c1->at(pos)+acum_c1->at(pos+1);
            acum_c2->at(pos)=c2->at(pos)+acum_c2->at(pos+1);
        }
    }
    

    *acum_c1=(*acum_c1)/acum_c1->at(0);
    *acum_c2=(*acum_c2)/acum_c2->at(0);*/
    *acum_c1=*c1;
    *acum_c2=*c2;
}

void Roc::write_curve_vals(string destination, bool append){
  
  ofstream write_file;
 
  get_extremes();
  obtain_vals();
  
  if(append)
    write_file.open(destination.c_str(), ios::out | ios::app);  
  else
    write_file.open(destination.c_str(), ios::out);
  
  write_file<<"c1=[0";
  //write_file<<to_string(acum_c1->at(0));
  
  for(int pos=div_range-1; pos>=0; pos--){
    write_file<<",";
    write_file<<to_string(acum_c1->at(pos));
  }
  
  write_file<<"]+c1;\n";
  
  write_file<<"c2=[0";
  //write_file<<to_string(acum_c2->at(0));
  
  for(int pos=div_range-1; pos>=0; pos--){
    write_file<<",";
    write_file<<to_string(acum_c2->at(pos));
  }
  
  write_file<<"]+c2;\n";
  write_file.close();
}

Roc::~Roc() {

    //delete c1; 
    //delete c2; 
    delete acum_c1;
    delete acum_c2;
    delete letmp;
    delete le;
}

