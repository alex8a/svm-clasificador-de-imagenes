/*
 * Lineextractor_norm.cpp
 *
 *  Created on: Dec 12, 2014
 *      Author: black
 */

#include "Lineextractor_norm.h"

Lineextractor_norm::Lineextractor_norm(int dim,  string path, string datafile, int num_data)
:Data_container(dim, num_data, path, datafile){


	try{
		x_ij=new mat(1,dim);
		x_ij->zeros();
		y_i=new vec(1);
		y_i->zeros();

	}catch(std::bad_alloc& ba){
		std::cerr << "bad_alloc caught Lineextractor: " << ba.what() << '\n';
		exit(EXIT_FAILURE);
	}
}


mat *Lineextractor_norm::extract_xij(){
	return x_ij;
}

vec *Lineextractor_norm::extract_yi(){
	return y_i;
}

bool Lineextractor_norm::end_extraction(){

	return eofflag;
}

void Lineextractor_norm::run(){

	if(!eofflag){
		int y;
                /*delete x_ij;
                delete y_i;
                x_ij=new mat(1,dim);
                y_i=new vec(1);*/
		x_ij->row(0)=normalise(get_line(y),1);
		y_i->at(0)=y;
	}
}

void Lineextractor_norm::run(unsigned int numline){

	if(numline<num_total_data){
		int y;

                /*delete x_ij;
                delete y_i;
                x_ij=new mat(1,dim);
                y_i=new vec(1);*/
		x_ij->row(0)=normalise(get_line(numline, y),1);
		y_i->at(0)=y;
	}else{
		x_ij->zeros();
		y_i->zeros();
	}
}

Lineextractor_norm::~Lineextractor_norm() {
    delete x_ij;
    x_ij=NULL;
    delete y_i;
    y_i=NULL;
}

