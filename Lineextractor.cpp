/*
 * Lineextractor.cpp
 *
 *  Created on: Dec 12, 2014
 *      Author: black
 */

#include "Lineextractor.h"

Lineextractor::Lineextractor(int dim,  string path, string datafile, int num_data)
:Data_container(dim, num_data, path, datafile){


	try{
		x_ij=new mat(1,dim);
		x_ij->zeros();
		y_i=new vec(1);
		y_i->zeros();

	}catch(std::bad_alloc& ba){
		std::cerr << "bad_alloc caught Lineextractor: " << ba.what() << '\n';
		exit(EXIT_FAILURE);
	}
}


mat *Lineextractor::extract_xij(){
	return x_ij;
}

vec *Lineextractor::extract_yi(){
	return y_i;
}

bool Lineextractor::end_extraction(){

	return eofflag;
}

void Lineextractor::run(){

	if(!eofflag){
		int y;
                /*delete x_ij;
                delete y_i;
                x_ij=new mat(1,dim);
                y_i=new vec(1);*/
		x_ij->row(0)=get_line(y);
		y_i->at(0)=y;
	}
}

void Lineextractor::run(unsigned int numline){

	if(numline<num_total_data){
		int y;

                /*delete x_ij;
                delete y_i;
                x_ij=new mat(1,dim);
                y_i=new vec(1);*/
		x_ij->row(0)=get_line(numline, y);
		y_i->at(0)=y;
	}else{
		x_ij->zeros();
		y_i->zeros();
	}
}

Lineextractor::~Lineextractor() {
    delete x_ij;
    x_ij=NULL;
    delete y_i;
    y_i=NULL;
}

