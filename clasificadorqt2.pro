#-------------------------------------------------
#
# Project created by QtCreator 2018-07-17T19:22:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pruebaWindow
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        Adatron.cpp\
        Bee_Col_ada.cpp\
        Cuttingplanes.cpp\
        Datacontainer.cpp\
        DE_ada.cpp\
        ff.cpp\
        Foldextractor_norm.cpp\
        Fullextractor.cpp\
        Fullextractor_norm.cpp\
        ka.cpp\
        Lineextractor.cpp\
        Lineextractor_norm.cpp\
        Evolalg.cpp\
        LinKer.cpp\
        mBee_Col_ada.cpp\
        PSO.cpp\
        PSO_ada.cpp\
        Roc.cpp\
        SVM.cpp \
    preprocessor.cpp \
    entrenador.cpp

HEADERS  += mainwindow.h\
        Evolalg.h\
        Adatron.h\
        Bee_Col_ada.h\
        BeeCol_data.h\
        Cuttingplanes.h\
        Datacontainer.h\
        DE_ada.h\
        DE_data.h\
        evdata.h\
        ff.h\
        Foldextractor_norm.h\
        Fullextractor.h\
        Fullextractor_norm.h\
        ka.h\
        Lineextractor.h\
        Lineextractor_norm.h\
        Evolalg.h\
        LinKer.h\
        LKdata.h\
        mBee_Col_ada.h\
        PSO.h\
        PSO_ada.h\
        Roc.h\
        SVM.h \
    resultsvm.h \
    preprocessor.h \
    entrenador.h

FORMS    += mainwindow.ui

CONFIG += c++11

QMAKE_CXXFLAGS += -std=c++0x -pthread -std=gnu++0x -pthread
LIBS += -pthread -llapack -lblas -larmadillo \
-LC:\Armadillo\lib32 -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui

INCLUDEPATH += /usr/local/include/opencv
