#include "threadupdatevecs.h"
#include "Adatron.h"
/*
threadUpdateVecs::threadUpdateVecs(QThread *parent) : QThread(parent)
{
}

void threadUpdateVecs::run(){
    try {
        double fi, nectar_vi, bias, tmp = 0, v_ij=0, ran_num, prob;
        int r = 0, i, j, phase, cont, num_source;
        bool flag_change;
        Adatron objfunc(bec->num_alphas);
        rowvec *ran_num_vec;

        ran_num_vec = new rowvec(bec->num_alphas);

        bec->count_mutex.lock();
        //cout<<"runUpdateVecs"<<endl;

            num_source = bec->cont_thread;
            bec->cont_thread++;

            //cout<< " cont_thread  "<<bec->cont_thread << "num_sources " << bec->num_sources<<endl;
            if (bec->cont_thread < bec->num_sources)
                bec->condition_var1.wait(&bec->count_mutex);
                //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
            else {
                bec->cont_thread = 0;
                bec->condition_var1.wakeAll();
                //pthread_cond_broadcast(&bec->condition_var1);
            }

        bec->count_mutex.unlock();
        //pthread_mutex_unlock(&bec->count_mutex);

        arma_rng::set_seed(time(NULL)+num_source);//Agregado para arma
        srand(time(NULL)+num_source);
        /*for (j = 0; j < bec->xixj->num_alphas; j++) {
            ran_num = bec->C * ((float) rand() / RAND_MAX);
            bec->Si[num_source]->at(j) = ran_num;
        }/

        ran_num_vec->randu();
        *ran_num_vec=bec->C * (*ran_num_vec);
        *bec->Si[num_source] = normalise(*ran_num_vec,1);
        //*bec->Si[num_source] = normalise(*bec->Si[num_source],1);


        //bec->Si->row(num_source).print("Alpha");
        bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
        bec->bias_Si->at(num_source) = bias;
        //pthread_mutex_unlock(&bec->count_mutex);//<----------

        if(bec->nectar_Si->at(num_source)>=0)
            bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
        else
            bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));

        bec->count_mutex.lock();
        //pthread_mutex_lock(&bec->count_mutex);

            bec->cont_thread++;

            if (bec->cont_thread >= bec->num_sources){
                bec->fval_bpos=bec->nectar_Si->at(num_source);
                bec->Best_Pos=bec->Si[num_source];
                bec->b=bec->bias_Si->at(num_source);
                bec->condition_var2.wakeOne();
                //pthread_cond_signal(&bec->condition_var2);
            }

            bec->condition_var1.wait(&bec->count_mutex);
            //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        bec->count_mutex.unlock();
        //pthread_mutex_unlock(&bec->count_mutex);

            bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;
        bec->count_mutex.lock();
        //pthread_mutex_lock(&bec->count_mutex);
            bec->cont_thread++;
            if (bec->cont_thread >= bec->num_sources){
                bec->cont_thread=0;
                bec->flag_update=false;
                bec->condition_var1.wakeAll();
                //pthread_cond_broadcast(&bec->condition_var1);
            }else
                bec->condition_var1.wait(&bec->count_mutex);
                //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

        bec->count_mutex.unlock();
        //pthread_mutex_unlock(&bec->count_mutex);


        //srand(time(NULL)+num_source);<<<------

        while (bec->block) {

            //pthread_mutex_lock(&bec->count_mutex);//<-----
            for (phase = 0; phase < 2; phase++) {

                flag_change = false;

                if (phase == 0) {//Employed Bees Phase

                    i = num_source;

                } else {//Onlooker Bees Phase

                    ran_num = ((float) rand() / RAND_MAX);

                    bec->count_mutex.lock();
                    //pthread_mutex_lock(&bec->count_mutex);

                    bec->cont_thread++;

                    if (bec->cont_thread >= bec->num_sources)
                        bec->condition_var2.wakeOne();
                        //pthread_cond_signal(&bec->condition_var2);

                    bec->condition_var1.wait(&bec->count_mutex);
                    //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

                    bec->count_mutex.unlock();
                    //pthread_mutex_unlock(&bec->count_mutex);

                    if (bec->flag_update == true) {

                        bec->pm->at(num_source) = bec->fit->at(num_source) / bec->sum_fit;
                    }


                    bec->count_mutex.lock();
                    //pthread_mutex_lock(&bec->count_mutex);

                    bec->cont_thread++;

                    if (bec->cont_thread >= bec->num_sources) {
                        bec->cont_thread = 0;
                        bec->flag_update = false;
                        bec->condition_var1.wakeAll();
                        //pthread_cond_broadcast(&bec->condition_var1);

                    } else
                        bec->condition_var1.wait(&bec->count_mutex);
                        //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

                    bec->count_mutex.unlock();
                    //pthread_mutex_unlock(&bec->count_mutex);

                    for (cont = 0, prob = 0; cont < bec->num_sources && ran_num > prob; cont++) {
                        prob += bec->pm->at(cont);
                        if (ran_num <= prob)
                            i = cont;
                    }
                }

                do {

                    j = round((bec->num_alphas - 1)* ((double) rand() / RAND_MAX));
                    fi = -bec->F + (2 * bec->F * ((double) rand() / RAND_MAX));


                    do {
                        r = round((bec->num_sources - 1) * ((double) rand() / RAND_MAX));
                    } while (r == i);

                    v_ij = bec->Si[i]->at(j) + fi * (bec->Si[i]->at(j) - bec->Si[r]->at(j));



                    if (v_ij < 0)
                        v_ij = 0;
                    else
                        if (v_ij > bec->C)
                            v_ij = bec->C;

                    //cout<<bec->Si->at(i, j)<<endl;
                    if (v_ij != bec->Si[i]->at(j))
                        flag_change = true;



                } while (!flag_change);


//pthread_mutex_lock(&bec->count_mutex);
                nectar_vi = objfunc.eval_cpos(bec->Si[i], j, v_ij, bec->xixj, bias);
                //pthread_mutex_unlock(&bec->count_mutex);


                bec->array_mutex[i].lock();
                //pthread_mutex_lock(&bec->array_mutex[i]);
                    if (nectar_vi < bec->nectar_Si->at(i)) {

                        bec->Si[i]->at(j) = v_ij;
                        bec->nectar_Si->at(i) = nectar_vi;
                        bec->bias_Si->at(i) = bias;

                        if (bec->nectar_Si->at(i) >= 0)
                            bec->fit->at(i) = 1 / (1 + bec->nectar_Si->at(i));
                        else
                            bec->fit->at(i) = (1 + abs(bec->nectar_Si->at(i)));

                        bec->flag_update = true;
                        bec->unsuc_trials->at(i) = 0;

                    } else {

                        bec->unsuc_trials->at(i)++;
                    }
                bec->array_mutex[i].unlock();
                //pthread_mutex_unlock(&bec->array_mutex[i]);

            }


            bec->count_mutex.lock();
            //pthread_mutex_lock(&bec->count_mutex);

                bec->cont_thread++;

                if (bec->cont_thread >= bec->num_sources) {
                    bec->cont_thread = 0;
                    bec->condition_var2.wakeAll();
                    //pthread_cond_broadcast(&bec->condition_var2);
                    bec->condition_var1.wait(&bec->count_mutex);
                    //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
                    bec->flag_update = false;
                } else
                    bec->condition_var1.wait(&bec->count_mutex);
                    //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

            bec->count_mutex.unlock();
            //pthread_mutex_unlock(&bec->count_mutex);


            if (bec->unsuc_trials->at(num_source) >= bec->limit_trials) {//Scout Bee Phase

                /*for (j = 0; j < bec->xixj->num_alphas; j++) {
                    ran_num = bec->C * ((float) rand() / RAND_MAX);
                    bec->Si[num_source]->at(j) = ran_num;
                }

                 *bec->Si[num_source] = normalise(*bec->Si[num_source],1);*

                ran_num_vec->randu();
                *ran_num_vec = bec->C * (*ran_num_vec);
                *bec->Si[num_source] = normalise(*ran_num_vec, 1);

                bec->nectar_Si->at(num_source) = objfunc.eval(bec->Si[num_source], bec->xixj, bias);
                bec->bias_Si->at(num_source) = bias;

                if (bec->nectar_Si->at(num_source) >= 0)
                    bec->fit->at(num_source) = 1 / (1 + bec->nectar_Si->at(num_source));
                else
                    bec->fit->at(num_source) = (1 + abs(bec->nectar_Si->at(num_source)));

                bec->flag_update = true;
                bec->unsuc_trials->at(num_source) = 0;
            }


            bec->count_mutex.lock();
            //pthread_mutex_lock(&bec->count_mutex);

                bec->cont_thread++;

                if (bec->cont_thread >= bec->num_sources) {

                    bec->flag_release=true;
                    bec->condition_var2.wakeOne();
                    //pthread_cond_signal(&bec->condition_var2);
                    bec->condition_var1.wait(&bec->count_mutex);
                    //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);
                    bec->flag_update = false;
                }else
                    bec->condition_var1.wait(&bec->count_mutex);
                    //pthread_cond_wait(&bec->condition_var1, &bec->count_mutex);

            bec->count_mutex.unlock();
            //pthread_mutex_unlock(&bec->count_mutex);

        }

    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }

    pthread_exit(NULL);
}


void threadUpdateVecs::setData(BeeCol_data* &data){
    this->bec=data;
}
*/
