/* 
 * File:   Adatron.h
 * Author: black
 *
 * Created on 12 de julio de 2015, 05:14 PM
 */

#ifndef ADATRON_H
#define	ADATRON_H

#include <armadillo>
#include <cmath>
#include "LinKer.h"

using namespace arma;

class Adatron {
public:
    Adatron(int num_alphas);
    virtual ~Adatron();
    
    double eval(rowvec *alpha,  LinKer *K, double &bias);
    double eval_cpos(rowvec *alpha, int pos_mod, double aj_mod,  LinKer *K, double &bias);
    double classify(rowvec *w, double b, Data_container *dc, bool line_per_line=true);
    
private:
    int num_alphas,cont_elem_tmp, cont_elem;
    double *dist, *dist_tmp, sigma, *no_eval, prom, prom_tmp;
};

#endif	/* ADATRON_H */

