/* 
 * File:   PSOdata.h
 * Author: black
 *
 * Created on September 17, 2014, 5:50 PM
 */

#ifndef PSODATA_ADA_H_
#define	PSODATA_ADA_H_

#include <armadillo>
#include "Cuttingplanes.h"

using namespace arma;

    typedef struct{
        pthread_mutex_t count_mutex;
        pthread_cond_t  condition_var1, condition_var2;
    
        int cont_thread, final_iter, swarm_size, k, ind_GBest;
        double w, C,  c1, c2,  w_start, w_end, num_alphas, Vmax, b;
        bool block;
        vec *fval_PBest, *bias_pb;
        rowvec **P_best, *w_vec;
        Data_container *dc;
        LinKer *xixj;
    }swarm_data_ada; 
    

#endif	/* PSODATA_ADA_H */

