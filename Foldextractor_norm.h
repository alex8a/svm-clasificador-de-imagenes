/* 
 * File:   Foldextractor_norm.h
 * Author: black
 *
 * Created on 23 de octubre de 2015, 05:43 PM
 */

#ifndef FOLDEXTRACTOR_NORM_H
#define	FOLDEXTRACTOR_NORM_H

#include <armadillo>

#include "Datacontainer.h"
#include "Fullextractor_norm.h"
#include "Lineextractor_norm.h"

using namespace arma;

class Foldextractor_norm: public Data_container{
public:
    Foldextractor_norm(int dim, int num_data_pf, int num_folds,
        string path, vector<string> dirlist_data, int omit_fold);
    void run();
    mat *extract_xij();
    vec *extract_yi();
    bool end_extraction();
    virtual ~Foldextractor_norm();
private:
    string path;
    int num_folds, num_data_pf, omit_fold;
    vector<string> dirlist_data;
    Data_container *dc;
};

#endif	/* FOLDEXTRACTOR_NORM_H */

