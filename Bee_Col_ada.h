/* 
 * File:   Bee_Col.h
 * Author: black
 *
 * Created on July 1, 2015, 1:10 PM
 */

#ifndef BEE_COL_ADA_H
#define	BEE_COL_ADA_H

#include <chrono>
#include <typeinfo>
#include <armadillo>

#include "LinKer.h"
#include "Evolalg.h"
#include "Datacontainer.h"
#include "BeeCol_data.h"
#include "Adatron.h"
#include "threadevalvecs.h"
#include "threadupdatevecs.h"
#include <QObject>

class Bee_Col_ada : public Evol_alg {
public:
    Bee_Col_ada(int num_sources, int final_iter, double F, Data_container *dc, 
            double C, int num_alphas, double limit_trials);
    void run();
    void run(LinKer *lk);
    virtual ~Bee_Col_ada();
    rowvec *get(double &b);
    
private:
    static void write_Colval(BeeCol_data *bec);
    
    void form_w();
    BeeCol_data *bec;
public slots:
    static void *updateSources(void *threadarg);
    static void *evalSources(void *threadarg);
};

#endif	/* BEE_COL_ADA_H */
