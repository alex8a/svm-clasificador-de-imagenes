/* 
 * File:   SVM.cpp
 * Author: black
 * 
 * Created on 25 de agosto de 2014, 03:53 PM
 */

#include "SVM.h"
#include "LinKer.h"
#include "resultsvm.h"

SVM::SVM() {
}

void SVM::form_w(rowvec alpha, LinKer *K, float C, rowvec *w, double &b){
    int i, non_zc_alphas=0;
    double wxi[K->num_alphas];
    b=0;
    w->zeros();
    
    for(int i=0; i<alpha.n_elem; i++)
        if(alpha.at(i)>0)
            *w+=alpha.at(i)*K->y_i->at(i)*K->x_ij->row(i);

    //w->print("w");
    
    for(i=0; i<K->num_alphas; i++){
        
        wxi[i]=dot(*w,K->x_ij->row(i));
        
        if(C>alpha.at(i)&&alpha.at(i)>0){
            non_zc_alphas++;
            b+=K->y_i->at(i)-wxi[i];
        }
    }
    
    b=b/non_zc_alphas;
}

double SVM::evaluate_prim(rowvec alpha, LinKer *K, float C, rowvec *w, double &b){
    int i, non_zc_alphas=0, pos=0, neg=0;
    double funcval, ep=0,  wxi[K->num_alphas], tmp;   
    
    b=0;
    w->zeros();
    
    for(int i=0; i<alpha.n_elem; i++)
        if(alpha.at(i)>0)
            *w+=alpha.at(i)*K->y_i->at(i)*K->x_ij->row(i);

    //w->print("w");
    
    for(i=0; i<K->num_alphas; i++){
        
        wxi[i]=dot(*w,K->x_ij->row(i));
        
        if(C>alpha.at(i)&&alpha.at(i)>0){
            non_zc_alphas++;
            b+=K->y_i->at(i)-wxi[i];
        }
    }
    
    b=b/non_zc_alphas;
    
    
    for(i=0; i<K->num_alphas; i++){
        tmp=1-K->y_i->at(i)*(wxi[i]+b);
        ep+=max(0.0,tmp);
        
        if(tmp<0&&K->y_i->at(i)==1)
            pos++;
        else
        if(tmp<0&&K->y_i->at(i)==-1)
            neg++;
    }
    cout<<"Pos: "<<pos<<endl;
    cout<<"Neg: "<<neg<<endl;
    cout<<"%: "<<(double)(neg+pos)/K->num_alphas*100<<endl;
        /*if(alpha.at(i)>0)
            funcval-=alpha.at(i)*(K->y_i->at(i)*(wxi[i]-b)-1);*/

    funcval=/*0.5*dot(*w,*w)+C**/ep;
    
    return funcval;
}

double SVM::check_condition(LinKer *K, rowvec alpha){
    double sum=0;
    
    for(int i=0; i<K->num_alphas;i++)
        sum+=K->y_i->at(i)*alpha.at(i);
    
    sum=abs(sum);
    return sum;
}

double SVM::evaluate_dual(rowvec alpha, LinKer *K, double &cond_val){
    int i, j;
    double funcval, alpha_sum=0, ai, aj, yi, dpw=0;
    cond_val=0;
    //cout<<endl;
    //alpha.print("alpha");
    
    for(i=0; i<K->num_alphas; i++){
        ai=alpha.at(i);
        if(ai>0){
            alpha_sum+=ai;
            yi=K->y_i->at(i);
            cond_val+=ai*yi;
            //K->x_ij->row(i).print("x_i");
            for(j=0; j<K->num_alphas; j++){
                aj=alpha.at(j);
                //K->x_ij->row(j).print("x_j");
                if(aj>0){
                    dpw+=ai*aj*yi*K->y_i->at(j)*K->xixj->at(i,j);
                    //cout<<"Kij "<<K->xixj->at(i,j)<<endl;
                }
            }
        }
    }

    cond_val=abs(cond_val);
    funcval=alpha_sum-0.5*dpw;
    return funcval;
}

double SVM::evaluate_dual(rowvec alpha, int pos_mod, double ai_mod, LinKer *K, double &cond_val){
    int i, j;
    double funcval, alpha_sum=0, ai, aj, yi, dpw=0;
    cond_val=0;
    //cout<<endl;
    //alpha.print("alpha");
    
    for(i=0; i<K->num_alphas; i++){
        
        if(i!=pos_mod)
            ai=alpha.at(i);
        else
            ai=ai_mod;
        
        if(ai>0){
            alpha_sum+=ai;
            yi=K->y_i->at(i);
            cond_val+=ai*yi;
            //K->x_ij->row(i).print("x_i");
            for(j=0; j<K->num_alphas; j++){
                if(j!=pos_mod)
                    aj=alpha.at(j);
                else
                    aj=ai_mod;
                //K->x_ij->row(j).print("x_j");
                if(aj>0){
                    dpw+=ai*aj*yi*K->y_i->at(j)*K->xixj->at(i,j);
                    //cout<<"Kij "<<K->xixj->at(i,j)<<endl;
                }
            }
        }
    }

    cond_val=abs(cond_val);
    funcval=alpha_sum-0.5*dpw;
    return funcval;
}

ResultSVM* SVM::classify(rowvec *w, double b, Data_container *dc, bool line_per_line){

    unsigned long  good = 0, pos = 0, neg = 0, contlines=0;
    rowvec *x_i;
    double y_i, wx, l, perc;
    x_i = new rowvec(dc->dim);
    ResultSVM *result = new ResultSVM();

    cout << "Classifying..."<<line_per_line << endl;

    if (line_per_line) {
        while (!dc->end_extraction()) {
            contlines++;
            dc->run();
            *x_i = dc->extract_xij()->row(0);
            y_i = dc->extract_yi()->at(0);

            wx = dot(*w, *x_i);

            l = y_i * (wx+b);

            if((wx+b)>0) result->resultImageTest=1;
            else         result->resultImageTest=-1;

            if (l > 0) {
                good++;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            }
        }
    } else{
        for(contlines=0; contlines<dc->num_data; contlines++) {

            *x_i = dc->extract_xij()->row(contlines);
            y_i = dc->extract_yi()->at(contlines);

            wx = dot(*w, *x_i);

            l = y_i * (wx+b);

            if (l > 0) {
                good++;
                //cout << " Ok" << endl;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            } /*else {
			cout << endl;

		}*/
        }
    }

    result->totalNumData=contlines;
    result->totalGood=good;
    result->totalGoodPos=pos;
    result->totalGoodNeg=neg;
    result->percentGood=(double) good / contlines * 100;

    cout << "Total num data: " << result->totalNumData << endl;
    cout << "Total good: " << result->totalGood << endl;
    cout << "C1: " << result->totalGoodNeg << endl;
    cout << "C2: " << result->totalGoodPos << endl;
    cout << "%: " <<  result->percentGood<< endl;
    delete x_i;
    
    return result;
}

double SVM::classify(rowvec *w, Data_container *dc, bool line_per_line, bool mult_y){

    unsigned long  good = 0, pos = 0, neg = 0, contlines=0;
    rowvec *x_i;
    double y_i, wx, l, perc;
    x_i = new rowvec(dc->dim);

    cout << "Classifying..." << endl;

    if (line_per_line) {
        while (!dc->end_extraction()) {

            contlines++;
            dc->run();
            *x_i = dc->extract_xij()->row(0);
            y_i = dc->extract_yi()->at(0);

            wx = dot(*w, *x_i);

            if(!mult_y)
                l = y_i * wx;
            else
                l = wx;
            //cout << "line: " << contlines << "* l: " << l << "* wx: " << wx
            //		<< "* y: " << y_i;

            /*if (contlines % 10000 == 0)
                cout << "Line " << contlines << endl;*/

            if (l > 0) {
                good++;
                //cout << " Ok" << endl;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            } /*else {
			cout << endl;

		}*/
        }
    } else{
        for(contlines=0; contlines<dc->num_data; contlines++) {

            *x_i = dc->extract_xij()->row(contlines);
            y_i = dc->extract_yi()->at(contlines);


            wx = dot(*w, *x_i);

            if(!mult_y)
                l = y_i * wx;
            else
                l = wx;
            //cout << "line: " << contlines << "* l: " << l << "* wx: " << wx
            //		<< "* y: " << y_i;

            /*if (contlines % 10000 == 0)
                cout << "Line " << contlines << endl;*/

            if (l > 0) {
                good++;
                //cout << " Ok" << endl;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            } /*else {
			cout << endl;

		}*/
        }
    }
    cout << "Total num data: " << contlines << endl;
    cout << "Total good: " << good << endl;
    cout << "C1: " << neg << endl;
    cout << "C2: " << pos << endl;
    perc=(double) good / contlines * 100;
    cout << "%: " <<  perc<< endl;
    delete x_i;
    
    return perc;
}


SVM::~SVM() {
}

