/*
 * Evolalg.h
 *
 *  Created on: Dec 11, 2014
 *      Author: black
 */



#ifndef EVOLALG_H_
#define EVOLALG_H_

#include <armadillo>
#include <chrono>
#include "Datacontainer.h"
#include "LinKer.h"

using namespace arma;
using namespace std::chrono;

class Evol_alg {
public:
    Evol_alg(){}
    virtual ~Evol_alg(){}
	virtual void run()=0;
        virtual void run(LinKer *lk)=0;
	virtual rowvec *get(double &b)=0;

protected:
        bool erase_lk=true;
};

#endif /* EVOLALG_H_ */
