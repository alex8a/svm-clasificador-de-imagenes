/* 
 * File:   Cutting_planes.cpp
 * Author: black
 * 
 * Created on October 15, 2014, 12:51 PM
 */

#include "Cuttingplanes.h"

Cutting_planes::Cutting_planes(int dim, int num_data){
    this->dim=dim;
    this->num_data=num_data;
    cont=1;
    cp_pos=1;
    np=100;
    a_ij=new mat(np,dim);
    b_i=new rowvec(np);
    a_ij->ones();
    b_i->at(0)=10;
    error=INF;
    replace_row=true;
    length_cut=num_data;
    
    hpd=new double[num_data];
    hpf=new double[num_data];
    new_cut=new rowvec(num_data);
    new_cut_l=new rowvec(num_data);
    
    for(int i=0; i<num_data; i++)
        new_cut->at(i)=i;
   
    output = new rowvec(num_data);
    output->zeros();
    old_output = new rowvec(num_data);
    old_output->zeros();
}

void Cutting_planes::gen_vec(rowvec w, Data_container *dc){
    
    mat *x_ij;
    rowvec a(dc->dim), x(dc->dim);
    vec b(1), *y_i;
    double tmp, y, num_data_err=0, tmperr;
    int pos=0, neg=0;
    
    a.zeros();
    x.zeros();
    b.zeros();
    x_ij=dc->extract_xij();
    y_i=dc->extract_yi();

    for(int i=0; i<num_data; i++) {

        x = x_ij->row(i);
        y = y_i->at(i);
        tmp = y * dot(w, x);
        if (tmp <= 1) {
            a += y*x;
            b += 1 - tmp;
            num_data_err++;
        } else
            if (y == 1)
            pos++;
        else
            neg++;

    }

    tmperr=(double)num_data_err/num_data;
    cout<<"Error: "<<tmperr<<endl;
    cout<<"Pos: "<<pos<<endl;
    cout<<"Neg: "<<neg<<endl;
    a=-a/num_data;
    b=b/num_data-dot(a,w);//*/
    /*a=-a/num_data_err;
    b=b/num_data_err-dot(a,w);//*/

    //a=-a;
    //b=b-dot(a,w);

    /*if(tmperr<error){
        a_ij->row(0)=a;
        b_i->row(0)=b;
    }*/
    
    if(replace_row){
        a_ij->row(0)=a;
        b_i->col(0)=b;
        replace_row=false;
    }else{
        
        a_ij->row(cont)=a;
        b_i->col(cont)=b;
        if(cont<np){
            cont++;
        }else{
        
        a_ij->row(cp_pos)=a;
        b_i->col(cp_pos)=b;
        if(cont<np){
            cont++;
        }
        cp_pos++;
        
        if(cp_pos>=np)
            cp_pos=0;
        }
    }//*/
}


rowvec Cutting_planes::gen_vec_beta(rowvec w, rowvec oldw, Data_container *dc,  double C, bool &change){
    
    mat *x_ij;
    rowvec a(dc->dim), x(dc->dim), new_w(dc->dim);
    vec *y_i;
    double b, tmp, y, Bi, Ci, val, GradVal, A0, B0, tmperr,
            GradVal_new, t=0, t_new, t1, t2,  Q_P, sq_norm_w=0, dp_WoldW=0;
    int num_hp=0, i, pos_cut=0, num_data_err=0;
        
    xi=0;
    training_err=0;
    
    a.zeros();
    x.zeros();
    b=0;
    x_ij=dc->extract_xij();
    y_i=dc->extract_yi();

    for (i = 0; i < dc->dim; i++) {
        sq_norm_w += w.at(i)*w.at(i);

        dp_WoldW += w.at(i)*oldw.at(i);
    }
    A0=sq_norm_w-2*dp_WoldW+old_sqnorm_w;
    B0=dp_WoldW-old_sqnorm_w;
    
    *old_output=*output;
    //output->zeros();
    
    for(i=0; i<num_data; i++) {

        if (((float) rand() / RAND_MAX) < 0.2) {
            x = x_ij->row(i);
            y = y_i->at(i);
            tmp = y * dot(w, x);
            /*if (new_cut[pos_cut]==i) {
                a += y*x;
                b += 1 - tmp;
                if(pos_cut+1<length_cut)
                    pos_cut++;
            }*/

            if (tmp <= 1) {
                a += y*x;
                b += 1 - tmp;
                num_data_err++;
            }

            output->at(i) = tmp;
        }
        Ci = C * (1 - old_output->at(i));
        Bi = C * (old_output->at(i) - output->at(i));

        if (Bi != 0)
            val = -Ci / Bi;
        else
            val = -INF;

        if (val > 0) {
            hpd[num_hp] = Bi;
            hpf[num_hp] = val;
            num_hp++;
        }

        if ((Bi < 0 && val > 0) || (Bi > 0 && val <= 0))
            GradVal += Bi;
        
    }

    
    a=-a/num_data;
    b=b/num_data-dot(a,w);//*/
    /*a=-a/length_cut;
    b=b/length_cut-dot(a,w);*/

    //a=-a;
   // b=b-dot(a,w);
    
    //a_ij->row(0) = a;
    //b_i->row(0) = b;    
    if(replace_row){
        a_ij->row(0)=a;
        b_i->at(0)=b;
        replace_row=false;
    }else{
        
        a_ij->row(cp_pos)=a;
        b_i->at(cp_pos)=b;
        if(cont<np){
            cont++;
        }
        cp_pos++;
        
        if(cp_pos>=np)
            cp_pos=0;
    }//*/
    
    
    write_cp(w);
            
    t=0;
    if(GradVal<0){
        
        sort_data(hpf, hpd, num_hp);
        
        i=0;
        
        while(GradVal<0&&i<num_hp){
            t_new=hpf[i];
            GradVal_new=GradVal+abs(hpd[i])+A0*(t_new-t);
            
            if(GradVal_new>=0){
                t = t + GradVal*(t-t_new)/(GradVal_new - GradVal);
            }else{
                t=t_new;
                i++;
            }
            GradVal = GradVal_new;
        }
    }
    t=max(t,0.0);
    //t=min(t,1.0);
    t1=t;
    //t1=0.65;
    //t2=t+(1-t)*MU;
    
    old_sqnorm_w=sq_norm_w;
    sq_norm_w=0;//<-----agregado
    
    for(i=0; i<dim; i++){
        new_w.at(i)=oldw.at(i)*(1-t)+t*w.at(i);
        sq_norm_w+=new_w.at(i)*new_w.at(i);//<--- cambio w por new_w
    }
    
    //length_cut=0;
    tmperr=0;
    for(i=0; i<num_data; i++){
        
        /*if((old_output->at(i)*(1-t2)+t2*output->at(i))<=1){
            new_cut->at(length_cut)=i;
            length_cut++;
        }*/
        output->at(i)=old_output->at(i)*(1-t1)+t1*output->at(i);
        
        if(output->at(i)<=1){
            xi+=1-output->at(i);
        }
        
        if(output->at(i)<=0)
            training_err++;//}
    }
    
    //tmperr=(double)training_err/num_data;
    tmperr=(xi+training_err)/2;
    //cout<<"Error: "<<tmperr<<endl;
    
    Q_P=0.5*sq_norm_w+C*xi;

    if (error > tmperr) {
        change = true;
        error = tmperr;
        new_cut_l=new_cut;
        xi_l=xi;
        length_cut_l=length_cut;
        training_err_l=training_err;
        return new_w;
    } else {
        change = false;
        return oldw;
    }
    
}

void Cutting_planes::write_cp(rowvec w){
    
    if (remove("/home/black/workspace/ff/cplanes.txt") != 0)
        perror("Error deleting file: cplanes");
    else
        puts("File successfully deleted: cplanes");
    
    ofstream pts;
    pts.open("/home/black/workspace/ff/cplanes.txt",ios::out|ios::app);
    
    for(int j=0; j<dim; j++)
        pts<<w.at(j)<<" ";
    
    pts<<0<<endl;
    
    for(int i=cont-1; i>=0; i--){
        for(int j=0; j<dim; j++)
            pts<<a_ij->at(i,j)<<" ";
        pts<<b_i->at(i);
        
        pts<<endl;
    }
    pts.close();
}

void  Cutting_planes::get_data_beta(rowvec *new_cut, double &xi, unsigned long &length_cut, unsigned long &t_error){
    
    *new_cut=*this->new_cut_l;
    xi=this->xi_l;
    length_cut=this->length_cut_l;
    t_error=this->training_err_l;
}

void Cutting_planes::gen_vec_hyb(rowvec w, Data_container *dc){
    
    mat *x_ij;
    rowvec a(dc->dim), x(dc->dim);
    vec b(1);
    double tmp, tmp2;

    a.zeros();
    x.zeros();
    b.zeros();
    x_ij=dc->extract_xij();

    for(int i=0; i<num_data; i++){

        x=x_ij->row(i);
        tmp=dot(w,x);
        if(tmp<=1)
            a+=x; 

        tmp2=1-tmp;
        if(tmp2>0)
            b+=tmp2;
        
    }

    a=-a/num_data;
    b=b/num_data-dot(a,w);

    if(replace_row){
        a_ij->row(0)=a;
        b_i->row(0)=b;
        replace_row=false;
    }else{
        
        a_ij->insert_rows(cont,a);
        b_i->insert_cols(cont,b);
        if(cont<30){
            cont++;
        }else{
            a_ij->shed_row(0);
            b_i->shed_col(0); 
       }
    }
}

double Cutting_planes::get_max(rowvec w){
    
    double tmp=0, eval;
    
    for(int i=0; i<cont; i++){
        
        eval=dot(a_ij->row(i),w)+b_i->at(i);
        //if(tmp<eval)
        //    tmp=eval;
        
        if(eval>0)
            tmp+=eval;
    }
    
    return tmp;
}

double Cutting_planes::at(int i, int j) {

    if ((i < cont || i >= 0) && j == 0){
        cout<<"b: "<<b_i->at(i)<<endl;
        return b_i->at(i);
    }else if ((i < cont || i >= 0)&&(j <= dim && j >= 1)){
        cout<<"a: "<<a_ij->at(i,j-1)<<endl;
        return a_ij->at(i, j-1);
    }else
        return NULL;
}

double Cutting_planes::replace(int i, int j, double val) {
    
    if ((i < cont || i >= 0) && j == 0)
        return b_i->at(i)=val;
    else if ((i < cont || i >= 0)&&(j <= dim && j >= 1))
        return a_ij->at(i, j-1)=val;
    else
        return NULL;
}

Cutting_planes::~Cutting_planes() {
    delete a_ij;
    delete b_i;
}

int Cutting_planes::sort_data(double* value, double* data, uint32_t size){
    if(size == 1)
      return 0;

	if (size==2)
	{
		if (value[0] > value[1])
		{
			swap(&value[0], &value[1]);
			swap(&data[0], &data[1]);
		}
		return 0;
	}
	double split=value[size/2];

	unsigned int left=0, right=size-1;

	while (left<=right)
	{
		while (value[left] < split)
			left++;
		while (value[right] > split)
			right--;

		if (left<=right)
		{
			swap(&value[left], &value[right]);
			swap(&data[left], &data[right]);
			left++;
			right--;
		}
	}

	if (right+1> 1)
		sort_data(value,data,right+1);

	if (size-left> 1)
		sort_data(&value[left],&data[left], size-left);


    return 0;
}

void Cutting_planes::swap(double* a, double* b){
	double dummy=*b;
	*b=*a;
	*a=dummy;
}

