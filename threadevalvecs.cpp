#include "threadevalvecs.h"
/*
threadEvalVecs::threadEvalVecs(QObject *parent) : QThread(parent)
{
    //cout<<"crear threadEvalVecs"<<endl;
}

void threadEvalVecs::run(){
    //cout<<"arranco threadEvalVecs"<<endl;

    try {
        int cont;
        double fval_CBest;
        uword ind_CBest=0;
        //Adatron objfunc;

        //bec->count_mutex.lock();
        pthread_mutex_lock(&bec->count_mutex);
        //bec->condition_var2.wait(&bec->count_mutex);
        pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);

        for (int iter = 0; iter <= bec->final_iter; iter++) {
            //cout<<"runEvalVecs"+iter<<endl;
            do {

                if (bec->flag_update) {
                    bec->sum_fit = bec->fit->at(0);

                    for (cont = 1; cont < bec->num_sources; cont++)
                        bec->sum_fit += bec->fit->at(cont);

                    fval_CBest=bec->nectar_Si->min(ind_CBest);

                    if(fval_CBest<bec->fval_bpos){
                        //cout<<"Nectar: "<<fval_CBest<<endl;
                        bec->Best_Pos=bec->Si[ind_CBest];
                        bec->fval_bpos=fval_CBest;
                        bec-> b=bec->bias_Si->at(ind_CBest);
                    }
                }

                bec->cont_thread = 0;

                //bec->condition_var1.wakeAll();
                pthread_cond_broadcast(&bec->condition_var1);
                //cout<<"wakeAll"<<endl;
                //bec->condition_var2.wait(&bec->count_mutex);
                pthread_cond_wait(&bec->condition_var2, &bec->count_mutex);
            } while (!bec->flag_release);

            bec->flag_release=false;

            if (bec->fval_bpos <= 0.001)
                break;
        }

        if (bec->flag_update) {

            fval_CBest = bec->nectar_Si->min(ind_CBest);

            if (fval_CBest < bec->fval_bpos) {
                //cout << "Nectar: " << fval_CBest << endl;
                bec->Best_Pos = bec->Si[ind_CBest];
                bec->fval_bpos = fval_CBest;
                bec->b = bec->bias_Si->at(ind_CBest);
            }
        }

        //bec->nectar_Si->max(ind_GBest);

        //cout<<"GBest: "<<bec->fval_bpos<<endl;

        bec->block = false;

        //bec->condition_var1.wakeAll();
        pthread_cond_broadcast(&bec->condition_var1);

        //bec->count_mutex.unlock();
        pthread_mutex_unlock(&bec->count_mutex);
    } catch (...) {
        std::exception_ptr p = std::current_exception();
        std::clog << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
    }
    pthread_exit(NULL);
}


void threadEvalVecs::setData(BeeCol_data* &data){
    this->bec=data;
}
*/
