#include "preprocessor.h"



Preprocessor::Preprocessor(QString path, QString fileNameRead, QString fileNameWrite)
{
    this->path=path;
    this->fileNameRead=fileNameRead;
    this->fileNameWrite=fileNameWrite;
}

const vector<string> splitString(const string& s, const char& c)
{
    string buff{""};
    vector<string> v;
    for(auto n:s){
        if(n != c) buff+=n; else
        if(n == c && buff != "") { v.push_back(buff); buff = ""; }
    }
    if(buff != "") v.push_back(buff);
    return v;
}

void Preprocessor::preprocesar(Ui::MainWindow *ui){
    ifstream myReadFile(path.toStdString()+fileNameRead.toStdString());
    cout<<path.toStdString()+fileNameRead.toStdString()<<endl;
    ofstream preprocessedFile(/*path.toStdString()+*/fileNameWrite.toStdString(),  std::ios::out | std::ios::binary); // saving file
    cout<<fileNameWrite.toStdString()<<endl;
    string line;
    myReadFile.seekg(0,ios_base::end);
    int tamanoArchivo = myReadFile.tellg();
    myReadFile.seekg(0,ios_base::beg);
    int contadorBytesLineas=0;
    cv::Mat foo;
    int imagesNum=0;
    string imageName;
    int positivos=0;
    int negativos=0;
    int counterPixelesAnalizados;
    while (getline(myReadFile, line)){
        contadorBytesLineas+=line.length();
        emit progress((int)(contadorBytesLineas*100/tamanoArchivo));
        vector<string> v{splitString(line, ' ')};
        imageName = path.toStdString()+"/"+v[0]+".pgm";
        foo = cv::imread(imageName);
        uint8_t* pixelPtr = (uint8_t*)foo.data;
        int cn = foo.channels();
        int promedioCanales;
        int initCol=187;
        int finalCol=837;
        float valorPx;
        int zero=-1;
        int one=1;
        if(v[2]=="NORM"){
            preprocessedFile.write( (char*)( &zero ), sizeof( int ) );
            negativos++;
        }else{
            preprocessedFile.write( (char*)( &one ), sizeof( int ) );
            positivos++;
        }
        int counter=0;
        counterPixelesAnalizados=0;
        for(int i = 0; i < foo.rows; i++){
            for(int j = initCol; j < finalCol; j++){
                if(counter%2==0){
                    counterPixelesAnalizados++;
                    promedioCanales=0;
                    for(int k=0;k<cn;k++){
                        promedioCanales+=pixelPtr[i*foo.cols*cn + j*cn + k];
                    }
                    promedioCanales/=cn;
                    valorPx=(((((float)promedioCanales)*10)/255)-5);
                    preprocessedFile.write( (char*)( &valorPx ), sizeof( float ) );
                }
                counter++;
            }
        }
        imagesNum++;
    }

    ui->lblNumImages->setText(QString::number(imagesNum));
    ui->lblOriginalSize->setText(QString::number(foo.rows)+"px "+QString::number(foo.cols)+"px");
    ui->lblCroppedSize->setText(QString::number(foo.rows)+"px "+QString::number(foo.cols-(187*2))+"px");
    ui->lblDim->setText(QString::number(counterPixelesAnalizados));
    ui->txtDim->setText(QString::number(counterPixelesAnalizados));
    ui->lblEjemplosPositivos->setText(QString::number(positivos));
    ui->lblEjemplosNegativos->setText(QString::number(negativos));

    QPixmap pic(QString::fromStdString(imageName));
    ui->imgPreproces->setPixmap(pic);

    myReadFile.close();
    preprocessedFile.close();
    emit progress(100);
    cout<<"Preprocessed"<<endl;
}


