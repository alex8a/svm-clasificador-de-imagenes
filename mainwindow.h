#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "resultsvm.h"
#include "entrenador.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool validarEntradasPreproc();
    bool validarEntradasEntrenamiento();
    bool validarEntradasPrueba();
    Entrenador* entrenador;

private slots:
    void on_examinarEntrenamiento_clicked();

    void on_cmdTrain_clicked();

    void on_examinarPrueba_clicked();

    void on_cmdPreProcess_clicked();

    void on_examinarPreprocessIn_clicked();

    void on_examinarPreprocessOut_clicked();

    void on_cmdPrueba_clicked();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
