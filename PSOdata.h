/* 
 * File:   PSOdata.h
 * Author: black
 *
 * Created on September 17, 2014, 5:50 PM
 */

#ifndef PSODATA_H_
#define	PSODATA_H_

#include <armadillo>
#include <string>
using namespace arma;

typedef struct{

    int cont_thread, final_iter, swarm_size, ind_GBest, dim, dim_dset;
    float w, Vmax, c1, c2,  w_start, w_end;
    bool block;
    vec *fval_Swarm, *fval_PBest, * boundary;
    mat  *V, *P_best, *Swarm_pos;
    std::string name, path, datafile;
}swarm_data;


#endif	/* PSODATA_H */

