/* 
 * File:   Adatron.cpp
 * Author: black
 * 
 * Created on 12 de julio de 2015, 05:14 PM
 */

#include "Adatron.h"
//#define INFINITY 5000000

Adatron::Adatron(int num_alphas) {
    //cout<<"Constructor adatron"<<endl;
    this->num_alphas=num_alphas;
}

double Adatron::eval(rowvec *alpha,  LinKer *K, double &bias){
    //cout<<"eval adatron inicia"<<endl;
    
    int dim_alpha=num_alphas, i=0;
    double z_i, zi_pos_min=INFINITY, zi_neg_max=-INFINITY, ss=40,
            margin, fval/*, err=0, err_pos=0, err_neg=0, good_pos=0, good_neg=0*/;
    ivec rnum = randi<ivec>(ss, distr_param(0, dim_alpha-1));

    arma_rng::set_seed_random();  // set the seed to a random value

    for (i = 0; i < ss; i++) {
            z_i = 0;
            
            /*if(i==0)
                j=i;
            else
                j=i-1;*/

            for (int j=0; j <ss; j++) {//j=0 lo cambié a j=i
                //cout<<"eeval adatron "<<K->xixj->at(rnum.at(j), rnum.at(i))<<endl;
                z_i += alpha->at(rnum.at(j)) * K->y_i->at(rnum.at(j)) * K->xixj->at(rnum.at(j), rnum.at(i));
            } 

            if (K->y_i->at(rnum.at(i)) == 1 && ((zi_pos_min > z_i/*&&z_i>0*/)/*||zi_pos_min==INFINITY*/)) {
                zi_pos_min = z_i;
            } else
                if (K->y_i->at(rnum.at(i)) == -1 && ((zi_neg_max < z_i/*&&z_i<0*/)/*||zi_neg_max==-INFINITY*/)) {
                zi_neg_max = z_i;
            }

            /*if (z_i > 0 && K->y_i->at(i) == -1)//||(z_i>0&&K->y_i->at(i)==1)))    
                err_neg++;
            else
                if (z_i < 0 && K->y_i->at(i) == 1)
                err_pos++;

            if (z_i < 0 && K->y_i->at(i) == -1)//||(z_i>0&&K->y_i->at(i)==1)))    
                good_neg++;
            else
                if (z_i > 0 && K->y_i->at(i) == 1)
                good_pos++;*/
            //err++;

    }
    //err=err_neg+err_pos;
    margin=(zi_pos_min-zi_neg_max)/2;

    bias = 0;
    //bias=(zi_pos_min+zi_neg_max)/2;

    fval = /*err */ abs(1 - margin);
    return fval;
}

double Adatron::eval_cpos(rowvec *alpha, int pos_mod, double aj_mod, LinKer *K, double &bias) {
    //cout<<"eval_cpos adatron"<<endl;
    int dim_alpha = num_alphas, i = 0;
    double z_i, zi_pos_min = INFINITY, zi_neg_max = -INFINITY, margin, alph_j, fval/*,
            err = 0*/;


    for (i = 0; i < dim_alpha; i++) {
            z_i = 0;
            //for (int j = i; j == i; j++) {//j=0 lo cambié a j=i

                    if (i!= pos_mod)
                        alph_j = alpha->at(i);
                    else
                        alph_j = aj_mod;

                    z_i += alph_j * K->y_i->at(i) * K->xixj->at(i, i);

            //}

            if (K->y_i->at(i) == 1 && ((zi_pos_min > z_i /*&& z_i > 0*/) /*|| zi_pos_min == INFINITY*/)) {
                zi_pos_min = z_i;
            } else
                if (K->y_i->at(i) == -1 && ((zi_neg_max < z_i /*&& z_i < 0*/) /*|| zi_neg_max == -INFINITY*/)) {
                zi_neg_max = z_i;
            }

            /*if ((z_i > 0 && K->y_i->at(i) == -1) || (z_i < 0 && K->y_i->at(i) == 1))
                err++;*/

    }

    margin = (zi_pos_min - zi_neg_max) / 2;

    //bias = 0;
    bias=(zi_pos_min+zi_neg_max)/2;
    
    fval = /*err */ abs(1 - margin);
    return fval;
}


double Adatron::classify(rowvec *w, double b, Data_container *dc, bool line_per_line){
    cout<<"classify adatron"<<endl;
    unsigned long  good = 0, pos = 0, neg = 0, contlines=0;
    rowvec *x_i;
    double y_i, wx, l, perc;
    x_i = new rowvec(dc->dim);

    cout << "Classifying..." << endl;

    if (line_per_line) {
        while (!dc->end_extraction()) {

            contlines++;
            dc->run();
            *x_i = dc->extract_xij()->row(0);
            y_i = dc->extract_yi()->at(0);

            wx = dot(*w, *x_i);

            
            l = y_i * (wx+b);

            if (contlines % 10000 == 0)
                cout << "Line " << contlines << endl;

            if (l > 0) {
                good++;
                //cout << " Ok" << endl;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            } /*else {
			cout << endl;

		}*/
        }
    } else{
        for(contlines=0; contlines<dc->num_data; contlines++) {

            *x_i = dc->extract_xij()->row(contlines);
            y_i = dc->extract_yi()->at(contlines);


            wx = dot(*w, *x_i);
            l = y_i * (wx+b);

            if (contlines % 10000 == 0)
                cout << "Line " << contlines << endl;

            if (l > 0) {
                good++;
                //cout << " Ok" << endl;
                if (y_i == 1)
                    pos++;
                else
                    neg++;
            } /*else {
			cout << endl;

		}*/
        }
    }
    cout << "Total num data: " << contlines << endl;
    cout << "Total good: " << good << endl;
    cout << "C1: " << neg << endl;
    cout << "C2: " << pos << endl;
    perc=(double) good / contlines * 100;
    cout << "%: " <<  perc<< endl;
    delete x_i;
    
    return perc;
}

Adatron::~Adatron() {
        /*delete []dist_tmp;
        delete []no_eval;
        delete []dist;*/
}

