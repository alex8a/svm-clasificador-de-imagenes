/* 
 * File:   ff.h
 * Author: black
 *
 * Created on October 19, 2015, 4:20 PM
 */

#ifndef FF_H
#define	FF_H
#include <string>
#include <dirent.h>
#include <armadillo>


#include "Evolalg.h"
#include "Fullextractor.h"
#include "Fullextractor_norm.h"
#include "Lineextractor.h"
#include "Lineextractor_norm.h"
#include "Foldextractor_norm.h"
#include "SVM.h"
#include "Bee_Col_ada.h"
#include "mBee_Col_ada.h"
#include "DE_ada.h"
#include "PSOdata.h"
#include "PSO_ada.h"
#include "evdata.h"

using namespace std;
using namespace arma;

class ff {
public:
    ff(string name_alg, int num_alphas, string path, int dim);
    float evaluate(rowvec eval);
    virtual ~ff();
private:
    void ff_de(rowvec eval);
    void ff_abc(rowvec eval);
    void ff_mabc(rowvec eval);
    void ff_pso(rowvec eval);
    void get_file_names(string dir_path, vector<string> &dirlist_data, vector<string> &dirlist_index);
    static void *fasteval(void *threadarg);
    ev_data *evd;
    
};

#endif	/* FF_H */

