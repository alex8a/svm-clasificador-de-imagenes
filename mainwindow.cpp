#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QFileDialog"
#include "iostream"
#include "resultsvm.h"

#include <dirent.h>
#include <armadillo>
#include "QMessageBox"

#include "Evolalg.h"
#include "Fullextractor.h"
#include "Fullextractor_norm.h"
#include "Lineextractor.h"
#include "Lineextractor_norm.h"
#include "SVM.h"
#include "Bee_Col_ada.h"
#include "mBee_Col_ada.h"
#include "DE_ada.h"
#include "Roc.h"
#include "PSO.h"
#include "PSO_ada.h"
#include "ka.h"
#include "preprocessor.h"
#include "entrenador.h"
#include "QProgressDialog"

#include "QPixmap"
#include <opencv2/opencv.hpp>


using namespace std;
using namespace arma;
double time_alg;


void folds_abca(int dim, string path, string datafile);
void folds_mabca(int dim, string path, string datafile);
void folds_de(int dim, string path, string datafile);
void folds2_mabca(int dim, string path);
void folds2_abca(int dim, string path);
void folds2_de(int dim, string path);
void folds2_pso(int dim, string path);
void get_file_names(string dir_path, vector<string> &dirlist_data);
void define_params(int dim, string path, string datafile);
void roc(int dim, string path, string datafile);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboAlgo->addItem("Bee Colony");
    ui->comboAlgo->addItem("mBee Colony");
    ui->comboAlgo->addItem("Differential Evolution");
    /*ui->comboAlgo->addItem("Particle Swarm Optimization");
    ui->comboAlgo->addItem("Folds mBee Colony");
    ui->comboAlgo->addItem("Folds Bee Colony");
    ui->comboAlgo->addItem("Folds Differential Evolution");
    ui->comboAlgo->addItem("Roc Curves");*/
    ui->txtDim->setText("9600");
    ui->txtDim->setValidator( new QIntValidator(0, 665600, this) );
    ui->tabTest->setEnabled(false);
    ui->imgPreproces->setScaledContents(true);
    ui->imgTest->setScaledContents(true);
    ui->imgTestResult->setScaledContents(true);
    QPixmap pic("/home/adatron/Escritorio/Detector Inteligente/gray.jpeg");
    ui->imgTest->setPixmap(pic);
    ui->imgTestResult->setPixmap(pic);
    ui->imgPreproces->setPixmap(pic);
    ui->radioAdatron->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_examinarPreprocessIn_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this,tr("Seleccionar carpeta de imágenes a preprocesar"),"/home/adatron/Escritorio/Detector Inteligente",QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    ui->txtPathImages->setText(dir);
}

void MainWindow::on_examinarPreprocessOut_clicked()
{
    string fileOutName = (QFileDialog::getSaveFileName(0, "Seleccione ubicación y nombre para el archivo de salida", "/home/adatron/Escritorio/Detector Inteligente", "Binary Files (*.bin)", new QString("Text files (*.txt)"))).toStdString();
    if(fileOutName.find(".bin")== std::string::npos){
        fileOutName+=".bin";
    }
    ui->txtDatasetName->setText(QString::fromStdString(fileOutName));
}

void MainWindow::on_cmdPreProcess_clicked()
{
    if(!validarEntradasPreproc()) return;
    QThread* thread = new QThread();
    Preprocessor* preprocessor = new Preprocessor(ui->txtPathImages->text(),"/Info.txt",ui->txtDatasetName->text());
    preprocessor->moveToThread(thread);

    QProgressDialog progressDialog("Preprocesando Imágenes...", "Abortar proceso", 0, 100, this);
    progressDialog.setWindowTitle("Espere un momento");

    QCoreApplication::processEvents();
    connect(thread, SIGNAL(finished()), preprocessor, SLOT(deleteLater()));
    connect(preprocessor, SIGNAL(progress(int)), &progressDialog, SLOT(setValue(int)));

    progressDialog.setWindowModality(Qt::WindowModal);
    progressDialog.setMinimumDuration(110);
    progressDialog.show();

    preprocessor->preprocesar(ui);
}

bool MainWindow::validarEntradasPreproc(){
    if(ui->txtPathImages->text().length()==0){
            QMessageBox Msgbox;
            Msgbox.setText("Please insert a folder path");
            Msgbox.exec();
            return false;
    }
    if(ui->txtDatasetName->text().length()==0){
            QMessageBox Msgbox;
            Msgbox.setText("Please insert a name for the dataset");
            Msgbox.exec();
            return false;
    }
    return true;
}

void MainWindow::on_examinarEntrenamiento_clicked()
{
    //QString dir = QFileDialog::getExistingDirectory(this,tr("Open Directory"),"/home",QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    QString fileName = QFileDialog::getOpenFileName(this,tr("Abrir archivo de set de datos"), "/home/adatron/Escritorio/Detector Inteligente", tr("Binary File (*.bin)"));
    ui->txtDataSet->setText(fileName);
}

void MainWindow::on_cmdTrain_clicked()
{
    if(!validarEntradasEntrenamiento()) return;
    ui->txtDataSet->text().toStdString();
    QThread* thread = new QThread();

    entrenador = new Entrenador();
    entrenador->moveToThread(thread);

    QProgressDialog progressDialog("Entrenando SVM...", "Abortar proceso", 0, 100, this);
    progressDialog.setWindowTitle("Espere un momento");

    QCoreApplication::processEvents();
    connect(thread, SIGNAL(finished()), entrenador, SLOT(deleteLater()));
    connect(entrenador, SIGNAL(progress(int)), &progressDialog, SLOT(setValue(int)));

    progressDialog.setWindowModality(Qt::WindowModal);
    progressDialog.setMinimumDuration(110);
    progressDialog.show();

    entrenador->entrenar(ui);
}

bool MainWindow::validarEntradasEntrenamiento(){
    ifstream f(ui->txtDataSet->text().toStdString().c_str());
    if(ui->txtDataSet->text().length()==0||!f.good()){
        cout<<"Ingresa un archivo válido"<<endl;
        return false;
    }
    if(ui->txtDim->text().toInt()<=0){
        cout<<"Ingresa una dimensión válida"<<endl;
        return false;
    }
    return true;
}

void MainWindow::on_examinarPrueba_clicked()
{
    QString fileNames = QFileDialog::getOpenFileName(this, tr("Abrir imagen de prueba"),"/home/adatron/Escritorio/Detector Inteligente",tr("Images (*.png *.pgm *.xpm *.jpg)"));
    QPixmap pic(fileNames);
    ui->imgTest->setPixmap(pic);
    ui->imgTestResult->setPixmap(pic);
    ui->txtImageTest->setText(fileNames);
    cout<<fileNames.toUtf8().constData()<<endl;
}

void MainWindow::on_cmdPrueba_clicked(){
    if(!validarEntradasPrueba()) return;
    //Preprocesar imagen
    ofstream preprocessedFile("imageTest.bin",  std::ios::out | std::ios::binary); // saving file
    cv::Mat foo = cv::imread(ui->txtImageTest->text().toUtf8().constData());
    uint8_t* pixelPtr = (uint8_t*)foo.data;
    int cn = foo.channels();
    int promedioCanales;
    int initCol;
    int finalCol;
    //if(entrenador->dim==6900){
        initCol=0;
        finalCol=foo.cols;
    /*}else{
        initCol=187;
        finalCol=837;
    }*/
    float valorPx;
    int posVector=0;
    int one=1;
    preprocessedFile.write( (char*)( &one ), sizeof( int ) );

    int counter=0;
    int counterPixelesAnalizados=0;
    for(int i = 0; i < foo.rows; i++){
        for(int j = initCol; j < finalCol; j++){
            if(ui->checkPreprocesarPrueba->isChecked()&&counter%2!=0){
                counter++;
                continue;
            }
            counterPixelesAnalizados++;
            promedioCanales=0;
            for(int k=0;k<cn;k++){
                promedioCanales+=pixelPtr[i*foo.cols*cn + j*cn + k];
            }
            promedioCanales/=cn;
            valorPx=(((((float)promedioCanales)*10)/255)-5);
            preprocessedFile.write( (char*)( &valorPx ), sizeof( float ) );
            counter++;
        }
    }
    preprocessedFile.close();

    Data_container *clfdata;
    SVM svm;

    clfdata = new Lineextractor_norm(entrenador->dim, "", "imageTest.bin");

    ResultSVM* result = svm.classify(entrenador->w, entrenador->b, clfdata);
    if(result->resultImageTest==1){
        ui->lblResultTest->setText("Positivo");
    }else{
        ui->lblResultTest->setText("Negativo");
    }
}

bool MainWindow::validarEntradasPrueba(){
    if(ui->txtImageTest->text().length()==0){
        QMessageBox Msgbox;
        Msgbox.setText("Selecciona una imagen para buscar posible tumor");
        Msgbox.exec();
        return false;
    }
    return true;
}

void folds_abca(int dim, string path, string datafile){

    /*int num_data = 400, num_sources = 30, final_iter = 1000, num_alphas=400;
    double F=1.25, C=10, limit_trials=50;*/
    int num_data = 400, num_sources = 10, final_iter = 1000, num_alphas=400;
    double F=0.9, C=1, limit_trials=50;//*/
    string dest_file, destination = "./roc";
    Roc *rc;
    bool append;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;


    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);

    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds" << endl;

        datafile=dirlist_data.at(cont_folds);


        edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
        Evol_alg *abca = new Bee_Col_ada(num_sources, final_iter, F, edata, C, num_alphas, limit_trials);

        abca->run();
        w = abca->get(b);

        //w->print("w");

        dest_file=destination+to_string(cont_folds)+".txt";
        append=false;

        for (int pos = 0; pos < num_folds; pos++) {
            datafile = dirlist_data.at(pos);
            ResultSVM *result;
            if (pos != cont_folds) {
                rc = new Roc(dim, path, datafile, *w, b, 100);
                rc->write_curve_vals(dest_file, append);
                clfdata = new Lineextractor_norm(dim, path, datafile);

                result=svm.classify(w, b, clfdata);
                genres=result->percentGood;
                write_results << genres << endl;
                acum_gen += genres;
                delete rc;
                append = true;
            } else {
                clfdata = new Lineextractor_norm(dim, path, datafile);
                result=svm.classify(w, b, clfdata);
                trres=result->percentGood;
                acum_trn += trres;
                wr_class << trres << endl;
            }
        }

        delete abca;
        delete edata;
        delete clfdata;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/(num_folds*(num_folds-1))<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void folds_mabca(int dim, string path, string datafile){

    /*int num_data = 400, final_iter = 2000, num_alphas=400;
    double F=1, C=10, limit_trials=100, FCR=0.1;*/
    int num_data = 400, final_iter = 2000, num_alphas=400;
    double F=0.85, C=10, limit_trials=100, FCR=0.9;//*/
    string dest_file, destination="./roc";
    Roc *rc;
    bool append;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);

    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds" << endl;

        datafile=dirlist_data.at(cont_folds);


        edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
        Evol_alg *mabca = new mBee_Col_ada(final_iter, FCR, F, edata, C, num_alphas, limit_trials);

        mabca->run();
        w = mabca->get(b);

        /*fstream write_w("w.txt",  std::ofstream::out);
        w->print(write_w, "w");
        write_w.close();*/

        dest_file=destination+to_string(cont_folds)+".txt";

        append=false;


        for (int pos = 0; pos < num_folds; pos++) {
            datafile = dirlist_data.at(pos);
            ResultSVM *result;
            if (pos != cont_folds) {
                rc= new Roc(dim, path, datafile, *w, b, 100);
                rc->write_curve_vals(dest_file, append);
                clfdata = new Lineextractor_norm(dim, path, datafile);

                result=svm.classify(w, b, clfdata);
                genres=result->percentGood;
                write_results<<genres<<endl;
                acum_gen+=genres;
                delete rc;
                append=true;
            }else{
                clfdata = new Lineextractor_norm(dim, path, datafile);
                result=svm.classify(w, b, clfdata);
                trres=result->percentGood;
                acum_trn+=trres;
                wr_class<<trres<<endl;
            }
        }

        delete mabca;
        delete edata;
        delete clfdata;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/(num_folds*(num_folds-1))<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void folds_de(int dim, string path, string datafile){

    /*int num_data = 400, num_vecs = 7, final_iter = 1000, num_alphas=400;
    double F=0.8, crsv=0.1, C=10;*/
    /*int num_data = 400, num_vecs = 20, final_iter = 1000, num_alphas=400;
    double F=0.85, crsv=0.9, C=10;//*///<<-----Best

    int num_data = 400, num_vecs = 30, final_iter = 75, num_alphas=400;
    double F=0.8, crsv=0.1, C=10;//*/
    string dest_file, destination = "./roc";
    Roc *rc;
    bool append;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);

    get_file_names(path, dirlist_data);


    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {

        rowvec *w;

        cout << "Folds" << endl;

        datafile=dirlist_data.at(cont_folds);


        edata = new Fullextractor_norm(dim, num_data, path, datafile, true, false);
        Evol_alg *de = new DE_ada(F, crsv, num_vecs, final_iter, edata, C, num_alphas);

        de->run();
        w = de->get(b);
        //break;

        //w->print("w");

        dest_file = destination + to_string(cont_folds) + ".txt";
        append = false;

        for (int pos = 0; pos < num_folds; pos++) {
            datafile = dirlist_data.at(pos);
            ResultSVM *result;
            if (pos != cont_folds) {
                rc= new Roc(dim, path, datafile, *w, b, 100);
                rc->write_curve_vals(dest_file, append);
                clfdata = new Lineextractor_norm(dim, path, datafile);

                result=svm.classify(w, b, clfdata);
                genres=result->percentGood;
                write_results<<genres<<endl;
                acum_gen+=genres;
                delete rc;
                append=true;
            }else{
                clfdata = new Lineextractor_norm(dim, path, datafile);
                result=svm.classify(w, b, clfdata);
                trres=result->percentGood;
                acum_trn+=trres;
                wr_class<<trres<<endl;
            }
        }

        delete de;
        delete edata;
        delete clfdata;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/(num_folds*(num_folds-1))<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void define_params(int dim, string path, string datafile){
    PSO *trainer;
    float c1=2, c2=2, Vmax=10, w_start=5, w_end=0.0001;
    vec *boundary;
    int swarm_size=10, final_iter=20;
    rowvec *results;
    string name="DE";

    if(strcmp(name.c_str(),"DE")==0){
        boundary=new vec(5);
        boundary->at(0)=20;//num_ind
        boundary->at(1)=1000;//final_iter
        boundary->at(2)=20;//F
        boundary->at(3)=1;//crsv
        boundary->at(4)=100;//C
    }else
    if(strcmp(name.c_str(),"ABC")==0){
        boundary=new vec(5);
        boundary->at(0)=20;//num_ind
        boundary->at(1)=1000;//final_iter
        boundary->at(2)=20;//F
        boundary->at(3)=100;//C
        boundary->at(4)=500;//limit_trails
    }else
    if(strcmp(name.c_str(),"mABC")==0){
        boundary=new vec(5);
        boundary->at(0)=500;//final_iter
        boundary->at(1)=2;//F
        boundary->at(2)=20;//C
        boundary->at(3)=50;//limit_trails
        boundary->at(4)=1;//FCR
    }else{

        boundary=new vec(8);
        boundary->at(0)=30;//num_ind
        boundary->at(1)=500;//final_iter
        boundary->at(2)=3;//c1
        boundary->at(3)=3;//c2
        boundary->at(4)=100;//C
        boundary->at(5)=10;//w_start
        boundary->at(6)=0.0001;//w_end
        boundary->at(7)=10;//Vmax
    }


    trainer=new PSO(dim, c1,c2,Vmax,w_start,w_end,swarm_size,final_iter,boundary,name,path,datafile);
    if(strcmp(name.c_str(),"ABC")==0||strcmp(name.c_str(),"DE")==0)
        trainer->run();
    else
    if(strcmp(name.c_str(),"PSO")==0)
        trainer->run_pso();
    else
        trainer->run_mabc();

    results=trainer->get();

    delete results;

}


void folds2_mabca(int dim, string path){

    /*int num_data = 400, final_iter = 2000, num_alphas=400;
    double F=1, C=10, limit_trials=100, FCR=0.1;*/
    int num_data = 380, final_iter = 10, num_alphas=380;
    double F=0.00001, C=0.00001, limit_trials=5, FCR=0.9999;//*/
    string dest_file, destination="./roc";
    Roc *rc;
    bool append=false;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);

    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds mabc" << endl;


        edata = new Foldextractor_norm(dim, num_data, num_folds, path,
                dirlist_data, cont_folds);
        num_alphas=edata->num_data;
        Evol_alg *mabca = new mBee_Col_ada(final_iter, FCR, F, edata, C, num_alphas, limit_trials);

        mabca->run();
        w = mabca->get(b);

        /*fstream write_w("w.txt",  std::ofstream::out);
        w->print(write_w, "w");
        write_w.close();*/

        dest_file=destination+to_string(cont_folds)+".txt";

        rc= new Roc(dim, path, dirlist_data.at(cont_folds), *w, b, 100);
        rc->write_curve_vals(dest_file, append);
        ResultSVM *result;
        result=svm.classify(w, b, clfdata);
        trres=result->percentGood;
        acum_trn += trres;
        wr_class << trres << endl;

        delete rc;

        clfdata = new Lineextractor_norm(dim, path, dirlist_data.at(cont_folds));
        result=svm.classify(w, b, clfdata);
        genres=result->percentGood;
        write_results << genres << endl;
        acum_gen += genres;



        delete mabca;
        delete edata;
        delete clfdata;
        //append=true;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/num_folds<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void folds2_abca(int dim, string path){

    int num_data = 400, final_iter = 10, num_alphas=400;
    double F=1, C=10, limit_trials=100, /*FCR=0.1,*/ num_sources=24;//*/
    /*int num_data = 380, final_iter = 10, num_alphas=380, num_sources=24;
    double F=20, C=20, limit_trials=5;//*/
    string dest_file, destination="./roc";
    Roc *rc;
    bool append=false;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;


    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);


    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds ABC" << endl;


        edata = new Foldextractor_norm(dim, num_data, num_folds, path,
                dirlist_data, cont_folds);
        num_alphas=edata->num_data;
        Evol_alg *abca = new Bee_Col_ada(num_sources, final_iter, F, edata, C, num_alphas, limit_trials);

        abca->run();
        w = abca->get(b);

        /*fstream write_w("w.txt",  std::ofstream::out);
        w->print(write_w, "w");
        write_w.close();*/

        dest_file=destination+to_string(cont_folds)+".txt";

        rc= new Roc(dim, path, dirlist_data.at(cont_folds), *w, b, 100);
        rc->write_curve_vals(dest_file, append);

        ResultSVM *result;
        result=svm.classify(w, b, clfdata);
        trres=result->percentGood;
        acum_trn += trres;
        wr_class << trres << endl;


        clfdata = new Lineextractor_norm(dim, path, dirlist_data.at(cont_folds));
        result=svm.classify(w, b, clfdata);
        genres=result->percentGood;
        write_results << genres << endl;
        acum_gen += genres;


        delete rc;
        delete abca;
        delete edata;
        delete clfdata;
        //append=true;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/num_folds<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void folds2_de(int dim, string path){

    /*int num_data = 360, final_iter = 15, num_alphas=360, num_vecs=30;
    double F=1, C=0.00001, crsv=0.5;*/
    /*int num_data = 380, final_iter = 15, num_alphas=380, num_vecs=30;
    double F=2.67393, C=10.4662, crsv=0.999;//<<-----Best*/
    int num_data = 400, final_iter = 125, num_alphas=400, num_vecs=5;
    double F=0.172346, C=2.25492, crsv=0.67769;//*/

    string dest_file, destination="./roc";
    Roc *rc;
    bool append=false;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);
    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds DE" << endl;


        edata = new Foldextractor_norm(dim, num_data, num_folds, path,
                dirlist_data, cont_folds);
        num_alphas=edata->num_data;
        Evol_alg *de = new DE_ada(F, crsv, num_vecs, final_iter, edata, C, num_alphas);

        de->run();
        w = de->get(b);

        /*fstream write_w("w.txt",  std::ofstream::out);
        w->print(write_w, "w");
        write_w.close();*/

        dest_file=destination+to_string(cont_folds)+".txt";

        rc= new Roc(dim, path, dirlist_data.at(cont_folds), *w, b, 100);
        rc->write_curve_vals(dest_file, append);

        ResultSVM *result;

        result = svm.classify(w, b, edata, false);
        trres = result->percentGood;
        acum_trn += trres;
        wr_class << trres << endl;



        clfdata = new Lineextractor_norm(dim, path, dirlist_data.at(cont_folds));
        result = svm.classify(w, b, edata, false);
        genres = result->percentGood;
        write_results << genres << endl;
        acum_gen += genres;


        delete rc;
        delete de;
        delete edata;
        delete clfdata;
        //append=true;

    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/num_folds<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void folds2_pso(int dim, string path){

    /*double c1=2.11145, c2=2.76978, C=9.65134, w_start=2.30235, w_end=5.08209e-05, Vmax=3.32807;
    int swarm_size=4, num_data = 380, final_iter = 301, num_alphas=380;//*/
    double c1=0.001, c2=0.01, C=0.1, w_start=10, w_end=0.0001, Vmax=0.1;
    int swarm_size=30, num_data = 400, final_iter = 90, num_alphas=400;//*/

    string dest_file, destination="./roc";
    Roc *rc;
    bool append=false;

    int num_folds=10;
    double b, acum_gen=0, acum_trn=0, genres, trres;
    SVM svm;
    Data_container *edata, *clfdata;
    vector<string> dirlist_data;
    ofstream write_results, wr_class;

    if (remove("./results_cross") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    if (remove("./results_cls") != 0)
        perror("Error deleting file");
    else
        puts("File successfully deleted");

    write_results.open("./results_cross", ios::out);
    wr_class.open("./results_cls", ios::out);

    dirlist_data.resize(num_folds);

    get_file_names(path, dirlist_data);

    for (int cont_folds = 0; cont_folds < num_folds; cont_folds++) {


        rowvec *w;

        cout << "Folds PSO" << endl;


        edata = new Foldextractor_norm(dim, num_data, num_folds, path,
                dirlist_data, cont_folds);
        num_alphas=edata->num_data;
        Evol_alg *pso = new PSO_ada(c1, c2, C, w_start, w_end, Vmax, swarm_size, final_iter, edata, num_alphas);

        pso->run();
        w = pso->get(b);

        /*fstream write_w("w.txt",  std::ofstream::out);
        w->print(write_w, "w");
        write_w.close();*/

        dest_file=destination+to_string(cont_folds)+".txt";

        rc= new Roc(dim, path, dirlist_data.at(cont_folds), *w, b, 100);
        rc->write_curve_vals(dest_file, append);

        ResultSVM *result;
        result = svm.classify(w, b, edata, false);
        trres = result->percentGood;
        acum_trn += trres;
        wr_class << trres << endl;



        clfdata = new Lineextractor_norm(dim, path, dirlist_data.at(cont_folds));
        result = svm.classify(w, b, clfdata);
        genres = result->percentGood;
        write_results << genres << endl;
        acum_gen += genres;


        delete rc;
        delete pso;
        delete edata;
        delete clfdata;
        //append=true;
    }

    wr_class<<endl;
    write_results<<endl;
    wr_class<<acum_trn/num_folds<<endl;
    write_results<<acum_gen/num_folds<<endl;

    wr_class<<"Time: "<<time_alg/num_folds<<endl;

    write_results.close();
    wr_class.close();
    cout << "Finish" << endl;
}

void roc(int dim, string path, string datafile){
    Roc *rc;
    string dest_file, destination = "./roc", line;
    bool append;
    int num_folds=10, fold=9, pos=0;
    double b=0;

    ifstream read_w;
    read_w.open("./roc.txt", ios::in);

    rowvec *w;
    w=new rowvec(dim);

    while(pos<dim){
        getline(read_w,line);
        w->at(pos)=atof(line.c_str());
        pos++;
    }
     getline(read_w,line);
     b=atof(line.c_str());

    w->print("w");
    dest_file = destination + to_string(fold) + ".txt";
    append = false;

    for (int pos = 0; pos < num_folds; pos++) {
        if (pos != fold) {
            rc = new Roc(dim, path, datafile, *w, b, 100);
            rc->write_curve_vals(dest_file, append);
            delete rc;
            append = true;
        }
    }

    read_w.close();
    cout << "Finish" << endl;
}

void get_file_names(string dir_path, vector<string> &dirlist_data){

    DIR *d;
    struct dirent *dir;
    string d_name, subs;


    d = opendir(dir_path.c_str());


    if (d) {
        while ((dir = readdir(d)) != NULL) {
            d_name=dir->d_name;
            if(d_name.compare(0,13,"saved_indexes")==0){
                subs=d_name.substr(13,2);
            }else
            if(d_name.compare(0,10,"saved_data")==0){
                subs=d_name.substr(10,2);
                dirlist_data.at(atoi(subs.c_str()))=dir->d_name;
            }
        }
    }
        closedir(d);
}
