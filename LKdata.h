/* 
 * File:   LKdata.h
 * Author: black
 *
 * Created on September 15, 2015, 6:12 PM
 */

#ifndef LKDATA_H
#define	LKDATA_H

typedef struct {
    pthread_mutex_t count_mutex;
    int dim, cont_thread, num_alphas, *pos_elems;
    double inc;
    
    Data_container *dc;
    mat * xixj, *X;
}LK_data;

#endif	/* LKDATA_H */

