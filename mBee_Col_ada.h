/* 
 * File:   Bee_Col.h
 * Author: black
 *
 * Created on July 1, 2015, 1:10 PM
 */

#ifndef MBEE_COL_ADA_H
#define	MBEE_COL_ADA_H

#include <chrono>
#include <typeinfo>
#include <armadillo>

#include "LinKer.h"
#include "Evolalg.h"
#include "Datacontainer.h"
#include "BeeCol_data.h"
#include "Adatron.h"

class mBee_Col_ada : public Evol_alg {
public:
    mBee_Col_ada(int final_iter, double FCR, double F, Data_container *dc, 
            double C, int num_alphas, double limit_trials);
    void run();
    void run(LinKer *lk);
    virtual ~mBee_Col_ada();
    rowvec *get(double &b);
    
private:
    static void *updateSources(void *threadarg);
    static void *evalSources(void *threadarg);
    static void write_Colval(BeeCol_data *bec);
    static void updateSumFit(BeeCol_data *bec);
    static void rank(BeeCol_data *bec);
    
    void form_w();
    
    BeeCol_data *bec;
};

#endif	/* MBEE_COL_ADA_H */

